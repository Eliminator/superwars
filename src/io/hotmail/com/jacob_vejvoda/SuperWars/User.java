package io.hotmail.com.jacob_vejvoda.SuperWars;

import java.util.ArrayList;
import org.bukkit.entity.Player;

public class User {

	private Player player;
	private PreviousStuff previousStuff;
	private String hero;
    private int lives;
    private boolean sneaking = false;
    private boolean jumping = false;
    private boolean cooling = false;
    private String string = "string";
    private ArrayList<String> coolList = new ArrayList<String>();

	public User (Player p, PreviousStuff ps, String h, int l) {
		setPlayer(p);
		setPreviousStuff(ps);
		setHero(h);
		setLives(l);
	}
	
	public void setCooling(String s){
		if(!coolList.contains(s))
			coolList.add(s);
	}
	
	public void stopCooling(String s){
		if(coolList.contains(s))
			coolList.remove(s);
	}
	
	public boolean isCooling(String s){
		if(coolList.contains(s))
			return true;
		return false;
	}

	public boolean isCooling() {
		return cooling;
	}

	public void setCooling(boolean cooling) {
		this.cooling = cooling;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public PreviousStuff getPreviousStuff() {
		return previousStuff;
	}

	public void setPreviousStuff(PreviousStuff previousStuff) {
		this.previousStuff = previousStuff;
	}

	public String getHero() {
		return hero;
	}

	public void setHero(String hero) {
		this.hero = hero;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public boolean isSneaking() {
		return sneaking;
	}

	public void setSneaking(boolean sneaking) {
		this.sneaking = sneaking;
	}

	public boolean isJumping() {
		return jumping;
	}

	public void setJumping(boolean jumping) {
		this.jumping = jumping;
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}
}
package io.hotmail.com.jacob_vejvoda.SuperWars;

import io.hotmail.com.jacob_vejvoda.HeroPack1.Heroes.Galactus;
import io.hotmail.com.jacob_vejvoda.HeroPack1.Heroes.Groot;
import io.hotmail.com.jacob_vejvoda.HeroPack1.Heroes.IceMan;
import io.hotmail.com.jacob_vejvoda.HeroPack1.Heroes.Magneto;
import io.hotmail.com.jacob_vejvoda.HeroPack1.Heroes.ProfessorX;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.AquaMan;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.BatMan;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.CaptainAmerica;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Deadpool;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Flash;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.GreenLantern;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Hawkeye;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Hero;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Hulk;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.HumanTorch;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.IronMan;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Joker;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Loki;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.NightCrawler;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.ScarletWitch;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Shazam;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.SpiderMan;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Storm;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.SuperMan;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Thor;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Venom;
import io.hotmail.com.jacob_vejvoda.SuperWars.heroes.Wolverine;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.PlayerDisguise;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

//import de.janmm14.customskins.bukkit.CustomSkins;

public class SuperWars extends JavaPlugin implements Listener, PluginMessageListener{
	File saveYML = new File(getDataFolder(), "save.yml");
	YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
	private CaptainAmerica CAClass;
	public SpiderMan SMClass;
	private Hulk HKClass;
	private Thor TRClass;
	private IronMan IMClass;
	private BatMan BMClass;
	private Wolverine WEClass;
	public GreenLantern GLClass;
	private AquaMan AMClass;
	private HumanTorch HTClass;
	private NightCrawler NCClass;
	private Hawkeye HEClass;
	private Storm STClass;
	private Joker JKClass;
	private SuperMan CKClass;
	private Flash FLClass;
	private Deadpool DPClass;
	private ScarletWitch SWClass;
	private Venom VClass;
	private Loki LClass;
	private Shazam SHClass;
	public ArrayList<Game> gameList = new ArrayList<Game>();
	HashMap<String, GameMaker> gameMakerMap = new HashMap<String, GameMaker>();
	ArrayList<String> heroList = new ArrayList<String>();
	public ArrayList<Integer> transBlocks = new ArrayList<Integer>();
	public Economy eco;
	File langYML = new File(getDataFolder(), "language.yml");
	public YamlConfiguration langFile = YamlConfiguration.loadConfiguration(langYML);
	public ArrayList<String> climbHeros = new ArrayList<String>();
	public ArrayList<String> restoringMaps = new ArrayList<String>();
	HashMap<String, Integer> SPCount = new HashMap<String, Integer>();
	HashMap<String, Boolean> SGStarted = new HashMap<String, Boolean>();
	public ArrayList<String> servers = null;
	public HashMap<Entity, BossBar> bossBars = new  HashMap<Entity, BossBar>();
	
    @Override
    public void onEnable(){
        this.CAClass = new CaptainAmerica(this);
        getServer().getPluginManager().registerEvents(this.CAClass, this);
        this.SMClass = new SpiderMan(this);
        getServer().getPluginManager().registerEvents(this.SMClass, this);
        this.HKClass = new Hulk(this);
        getServer().getPluginManager().registerEvents(this.HKClass, this);
        this.TRClass = new Thor(this);
        getServer().getPluginManager().registerEvents(this.TRClass, this);
        this.IMClass = new IronMan(this);
        getServer().getPluginManager().registerEvents(this.IMClass, this);
        this.BMClass = new BatMan(this);
        getServer().getPluginManager().registerEvents(this.BMClass, this);
        this.WEClass = new Wolverine(this);
        getServer().getPluginManager().registerEvents(this.WEClass, this);
        this.GLClass = new GreenLantern(this);
        getServer().getPluginManager().registerEvents(this.GLClass, this);
        this.AMClass = new AquaMan(this);
        getServer().getPluginManager().registerEvents(this.AMClass, this);
        this.HTClass = new HumanTorch(this);
        getServer().getPluginManager().registerEvents(this.HTClass, this);
        this.NCClass = new NightCrawler(this);
        getServer().getPluginManager().registerEvents(this.NCClass, this);
        this.HEClass = new Hawkeye(this);
        getServer().getPluginManager().registerEvents(this.HEClass, this);
        this.STClass = new Storm(this);
        getServer().getPluginManager().registerEvents(this.STClass, this);
        this.JKClass = new Joker(this);
        getServer().getPluginManager().registerEvents(this.JKClass, this);
        this.CKClass = new SuperMan(this);
        getServer().getPluginManager().registerEvents(this.CKClass, this);
        this.FLClass = new Flash(this);
        getServer().getPluginManager().registerEvents(this.FLClass, this);
        this.DPClass = new Deadpool(this);
        getServer().getPluginManager().registerEvents(this.DPClass, this);
        this.SWClass = new ScarletWitch(this);
        getServer().getPluginManager().registerEvents(this.SWClass, this);
        this.VClass = new Venom(this);
        getServer().getPluginManager().registerEvents(this.VClass, this);
        this.LClass = new Loki(this);
        getServer().getPluginManager().registerEvents(this.LClass, this);
        this.SHClass = new Shazam(this);
        getServer().getPluginManager().registerEvents(this.SHClass, this);
        //Main Events
    	getServer().getPluginManager().registerEvents(this, this); 
    	//Config
    	if (!new File(getDataFolder(), "config.yml").exists()) {
   	     saveDefaultConfig();
    	}
    	//Metrics
		try {
		    Metrics metrics = new Metrics(this);
		    metrics.start();
		} catch (IOException e) {
		    // Failed to submit the stats :-(
		}
    	//Language
    	if (!new File(getDataFolder(), "language.yml").exists()) {
    		saveDefaultLang();
    	}
    	//Register Saves
        if (!saveYML.exists()) {
            try {
            	saveYML.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    	//Bungee
    	if(hasDedicatedMode()){
	        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", (PluginMessageListener) this);
			getServers();
    	}
        //getCommand("players").setExecutor(this);
    	//Set Plugin Stuff
    	setupEconomy();
		defineTransBlocks();
		updateSigns();
		addHeros();
    }
    
    private boolean setupEconomy(){
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            eco = economyProvider.getProvider();
        }
        return (eco != null);
    }
    
    private void getServers(){
         ByteArrayDataOutput out = ByteStreams.newDataOutput();
         out.writeUTF("GetServers");
         getServer().sendPluginMessage(this, "BungeeCord", out.toByteArray());
    }
    
	@EventHandler(priority=EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent e) {
    	if(servers == null)
    		if(hasDedicatedMode())
    			getServers();
    }
    
    @Override
    public void onDisable(){
		for(Player player : this.getServer().getOnlinePlayers()){
			if(getGame(player) != null)
				leaveGame(player, true);
		}
    }
    
	@SuppressWarnings("deprecation")
	public void reloadLang() {
        if (langYML == null) {
        	langYML = new File(getDataFolder(), "language.yml");
        }
        langFile = YamlConfiguration.loadConfiguration(langYML);
     
        // Look for defaults in the jar
        InputStream defConfigStream = this.getResource("saves.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            langFile.setDefaults(defConfig);
        }
    }
    
    public void saveDefaultLang() {
        if (langYML == null) {
        	langYML = new File(getDataFolder(), "language.yml");
        }
        if (!langYML.exists()) {            
            this.saveResource("language.yml", false);
//        	try {
//				new File(getDataFolder(), "language.yml").createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
         }
    }
    
    private void saveSaveFile(){
    	try{
    		saveFile.save(saveYML);
    	}catch(IOException e){}
    }
    
	@EventHandler(priority=EventPriority.HIGH)
    public void onPlayerQuit(PlayerQuitEvent event) {
		if(getGame(event.getPlayer()) != null)
			leaveGame(event.getPlayer(), true);
    }
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public void openHeroMenu(Player p){
		//Create Shop
		//Inventory inv = this.getServer().createInventory(p, 27, "§0§lPick a super hero/villain");
		Inventory inv = getServer().createInventory(p, 27, this.langFile.getString("classMenuTitle").replace("&", "§"));
		ArrayList<String> sList = (ArrayList<String>) getConfig().getList("enabledHeroes");
		//Heroes
		int index = 0;
		if((sList.contains("CaptainAmerica")) && (p.hasPermission("superwars.CaptainAmerica.hero"))){
			ItemStack s = getItem(Material.LEATHER_HELMET.getId()+"", "§7CaptainAmerica", 1, getHeroInfo("CA")); dye(s, Color.BLUE);
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("SpiderMan")) && (p.hasPermission("superwars.SpiderMan.hero"))){
			ItemStack s = getItem(Material.WEB.getId()+"", "§7SpiderMan", 1, getHeroInfo("SM"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Thor")) && (p.hasPermission("superwars.Thor.hero"))){
			ItemStack s = getItem(Material.IRON_AXE.getId()+"", "§7Thor", 1, getHeroInfo("TH"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Hulk")) && (p.hasPermission("superwars.Hulk.hero"))){
			ItemStack s = getItem(Material.SLIME_BALL.getId()+"", "§7Hulk", 1, getHeroInfo("HK"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("IronMan")) && (p.hasPermission("superwars.IronMan.hero"))){
			ItemStack s = getItem(Material.LEATHER_HELMET.getId()+"", "§7IronMan", 1, getHeroInfo("IM")); dye(s, Color.YELLOW);
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("BatMan")) && (p.hasPermission("superwars.BatMan.hero"))){
			ItemStack s = getItem(Material.INK_SACK.getId()+"", "§7BatMan", 1, getHeroInfo("BM"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Wolverine")) && (p.hasPermission("superwars.Wolverine.hero"))){
			ItemStack s = getItem("101", "§7Wolverine", 1, getHeroInfo("WV"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("GreenLantern")) && (p.hasPermission("superwars.GreenLantern.hero"))){
			ItemStack s = getItem(Material.EMERALD_BLOCK.getId()+"", "§7GreenLantern", 1, getHeroInfo("GL"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("AquaMan")) && (p.hasPermission("superwars.AquaMan.hero"))){
			ItemStack s = getItem("351:4", "§7AquaMan", 1, getHeroInfo("AM"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("HumanTorch")) && (p.hasPermission("superwars.HumanTorch.hero"))){
			ItemStack s = getItem(Material.BLAZE_POWDER.getId()+"", "§7HumanTorch", 1, getHeroInfo("HT"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("NightCrawler")) && (p.hasPermission("superwars.NightCrawler.hero"))){
			ItemStack s = getItem(Material.ENDER_PEARL.getId()+"", "§7NightCrawler", 1, getHeroInfo("NC"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Hawkeye")) && (p.hasPermission("superwars.Hawkeye.hero"))){
			ItemStack s = getItem(Material.BOW.getId()+"", "§7Hawkeye", 1, getHeroInfo("HE"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Storm")) && (p.hasPermission("superwars.Storm.hero"))){
			ItemStack s = getItem("351:15", "§7Storm", 1, getHeroInfo("S"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Joker")) && (p.hasPermission("superwars.Joker.hero"))){
			ItemStack s = getItem(Material.YELLOW_FLOWER.getId()+"", "§7Joker", 1, getHeroInfo("J"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("SuperMan")) && (p.hasPermission("superwars.SuperMan.hero"))){
			ItemStack s = getItem(Material.LEATHER_CHESTPLATE.getId()+"", "§7SuperMan", 1, getHeroInfo("SUM")); dye(s, Color.RED);
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Flash")) && (p.hasPermission("superwars.Flash.hero"))){
			ItemStack s = getItem("175", "§7Flash", 1, getHeroInfo("FL"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Deadpool")) && (p.hasPermission("superwars.Deadpool.hero"))){
			ItemStack s = getItem("383:96", "§7Deadpool", 1, getHeroInfo("DP"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("ScarletWitch")) && (p.hasPermission("superwars.ScarletWitch.hero"))){
			ItemStack s = getItem("351:1", "§7ScarletWitch", 1, getHeroInfo("SW"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Venom")) && (p.hasPermission("superwars.Venom.hero"))){
			ItemStack s = getItem("397:1", "§7Venom", 1, getHeroInfo("V"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if((sList.contains("Loki")) && (p.hasPermission("superwars.Loki.hero"))){
			ItemStack s = getItem("369", "§7Loki", 1, getHeroInfo("L"));
			inv.setItem(index, s);
			index = index + 1;
		}
	    if ((sList.contains("Shazam")) && (p.hasPermission("superwars.Shazam.hero"))){
	      ItemStack s = getItem("384", "§fShazam", 1, getHeroInfo("SH"));
	      inv.setItem(index, s);
	      index++;
	    }
		if(hasPack1() && (sList.contains("Galactus")) && (p.hasPermission("superwars.Galactus.hero"))){
			ItemStack s = getItem("121", "§7Galactus", 1, getHeroInfo("GS"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if(hasPack1() && (sList.contains("Groot")) && (p.hasPermission("superwars.Groot.hero"))){
			ItemStack s = getItem("17", "§7Groot", 1, getHeroInfo("GT"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if(hasPack1() && (sList.contains("Magneto")) && (p.hasPermission("superwars.Magneto.hero"))){
			ItemStack s = getItem("265", "§7Magneto", 1, getHeroInfo("MG"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if(hasPack1() && (sList.contains("IceMan")) && (p.hasPermission("superwars.IceMan.hero"))){
			ItemStack s = getItem("79", "§7IceMan", 1, getHeroInfo("IM"));
			inv.setItem(index, s);
			index = index + 1;
		}
		if(hasPack1() && (sList.contains("ProfessorX")) && (p.hasPermission("superwars.ProfessorX.hero"))){
			ItemStack s = getItem("76", "§7Professor X", 1, getHeroInfo("PX"));
			inv.setItem(index, s);
			index = index + 1;
		}
		p.openInventory(inv);
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<String> getHeroInfo(String type){
		ArrayList<String> list = new ArrayList<String>();
		//System.out.println("CAInfo: " + langFile.getList("CAInfo"));
		//System.out.println(type + "Info: " + langFile.getList(type + "Info"));
		for(String s : (ArrayList<String>)langFile.getList(type + "Info"))
			list.add(s.replace("&", "§"));
		return list;
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerClick(InventoryClickEvent e) {
		String name = e.getInventory().getName();
		Player p = (Player) e.getWhoClicked();
		try{
			String n = e.getCurrentItem().getItemMeta().getDisplayName();
			//if(name.equals("§0§lPick a super hero/villain")){
			if (name.equals(this.langFile.getString("classMenuTitle").replace("&", "§"))){
				e.setCancelled(true);
				if(n.equals("§7CaptainAmerica")){
					makeHero(p, "CaptainAmerica");
				}else if(n.equals("§7SpiderMan")){
					makeHero(p, "SpiderMan");
				}else if(n.equals("§7Thor")){
					makeHero(p, "Thor");
				}else if(n.equals("§7Hulk")){
					makeHero(p, "Hulk");
				}else if(n.equals("§7IronMan")){
					makeHero(p, "IronMan");
				}else if(n.equals("§7BatMan")){
					makeHero(p, "BatMan");
				}else if(n.equals("§7Wolverine")){
					makeHero(p, "Wolverine");
				}else if(n.equals("§7GreenLantern")){
					makeHero(p, "GreenLantern");
				}else if(n.equals("§7AquaMan")){
					makeHero(p, "AquaMan");
				}else if(n.equals("§7HumanTorch")){
					makeHero(p, "HumanTorch");
				}else if(n.equals("§7NightCrawler")){
					makeHero(p, "NightCrawler");
				}else if(n.equals("§7Hawkeye")){
					makeHero(p, "Hawkeye");
				}else if(n.equals("§7Storm")){
					makeHero(p, "Storm");
				}else if(n.equals("§7Joker")){
					makeHero(p, "Joker");
				}else if(n.equals("§7SuperMan")){
					makeHero(p, "SuperMan");
				}else if(n.equals("§7Flash")){
					makeHero(p, "Flash");
				}else if(n.equals("§7Deadpool")){
					makeHero(p, "Deadpool");
				}else if(n.equals("§7ScarletWitch")){
					makeHero(p, "ScarletWitch");
				}else if(n.equals("§7Venom")){
					makeHero(p, "Venom");
				}else if(n.equals("§7Loki")){
					makeHero(p, "Loki");
				}else if (n.equals("§fShazam")) {
			          makeHero(p, "Shazam");
		        }else if(hasPack1() && n.equals("§7Galactus")){
					makeHero(p, "Galactus");
				}else if(hasPack1() && n.equals("§7Groot")){
					makeHero(p, "Groot");
				}else if(hasPack1() && n.equals("§7Magneto")){
					makeHero(p, "Magneto");
				}else if(hasPack1() && n.equals("§7IceMan")){
					makeHero(p, "IceMan");
				}else if(hasPack1() && n.equals("§7Professor X")){
					makeHero(p, "Professor X");
				}
				p.closeInventory();
			}
		}catch(Exception x){/**x.printStackTrace();**/}
	}
	
	@SuppressWarnings("deprecation")
	private void giveLobbyItems(final Player p){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
		     public void run() {
		 		ItemStack h = getItem(Material.SLIME_BALL.getId()+"", "§2Change Hero", 1, null);
				p.getInventory().addItem(h);
				ItemStack s = getItem(Material.MAGMA_CREAM.getId()+"", "§cLeave Game", 1, null);
				p.getInventory().addItem(s);
			 }
		}, (5));
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if((event.getClickedBlock().getType().equals(Material.SIGN_POST)) || (event.getClickedBlock().getType().equals(Material.WALL_SIGN))){
				Sign sign = (Sign) event.getClickedBlock().getState();
				String[] text = sign.getLines();
				if(text[0].equals("§l§nSuperWars")){
					try{
						String gameName = text[2];
						if(hasDedicatedMode()){
							ByteArrayDataOutput out = ByteStreams.newDataOutput();
							out.writeUTF("Connect");
							out.writeUTF(gameName);
							player.sendPluginMessage(this, "BungeeCord", out.toByteArray());
							return;
						}else if(saveFile.getString("gameInfo." + gameName) != null){
							joinGame(player, gameName);
							return;
						}
					}catch(Exception e){}
					if(langFile.getString("gameNotFound") != null)
						player.sendMessage(langFile.getString("gameNotFound").replace("&", "§"));
					return;
				}else if(heroList.contains(text[1])){
					if(player.hasPermission("superwars." + text[1] + ".hero")){
						if(getGame(player) != null)
							makeHero(player, text[1]);
					}else{
						if(langFile.getString("noHeroPerms") != null)
							player.sendMessage(langFile.getString("noHeroPerms").replace("&", "§").replace("<hero>", text[1]));
					}
				}
			}
		}
		if(getGame(player) != null){
			//Lobby Items
			try{
				if(player.getItemInHand().getItemMeta().getDisplayName().equals("§2Change Hero")){
					openHeroMenu(player);
				}else if(player.getItemInHand().getItemMeta().getDisplayName().equals("§cLeave Game")){
					leaveGame(player, true);
				}
			}catch(Exception x){}
			//Stop tools from braking
			if (player.getItemInHand() != null) {
				if ((player.getItemInHand().getType().equals(Material.FISHING_ROD)) || (player.getItemInHand().getType().equals(Material.IRON_AXE)) || (player.getItemInHand().getType().equals(Material.IRON_SWORD))) {
					player.getItemInHand().setDurability((short) 0);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void makeHero(Player player, String hero){
		if(langFile.getString("switchHero") != null)
			player.sendMessage(langFile.getString("switchHero").replace("&", "§").replace("<hero>", hero));
		Game gm = getGame(player);
		for(User usr : (ArrayList<User>)gm.getPlayerList.clone()){
			if(usr.getPlayer().equals(player)){
				//User newUsr = new User(usr.getPlayer, usr.getPreviousStuff, hero, usr.getLives, usr.getCooling, usr.getJumping, usr.getSneaking, usr.getString);
				//gm.getPlayerList.set(getIndex(usr.getPlayer), newUsr);
				User u = gm.getPlayerList.get(getIndex(player));
				u.setHero(hero);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onBlockBreak(BlockBreakEvent event){
		if(getGame(event.getPlayer()) != null){
			Player player = event.getPlayer();
			Game game = getGame(player);
			//User user = game.getPlayerList.get(getIndex(player));
			if(game.getStarted == false){
				event.setCancelled(true);
				if(langFile.getString("noBlockBreak") != null)
					player.sendMessage(langFile.getString("noBlockBreak").replace("&", "§"));
			}else{
				event.getBlock().getDrops().clear();
			}
		}else if((event.getBlock().getType().equals(Material.SIGN_POST)) || (event.getBlock().getType().equals(Material.WALL_SIGN))){
			//System.out.println("signbrake1");//------------------------DEBUG
			Location signLoc = event.getBlock().getLocation();
			removeSign(signLoc);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event){
		try{
			if(getGame(event.getPlayer()) != null){
				Game game = getGame(event.getPlayer());
				if((event.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null) && (game.getStarted == true)){
					return;
				}
				event.setCancelled(true);
				if(langFile.getString("noBlockBuild") != null)
					event.getPlayer().sendMessage(langFile.getString("noBlockBuild").replace("&", "§"));
				event.getPlayer().updateInventory();
			}
		}catch(Exception e){};
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		final Player player = event.getPlayer();
		if(getGame(player) != null){
			try{
				if(event.getItem().getItemStack().getItemMeta().getDisplayName() == null){
					event.setCancelled(true);
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerDie(PlayerDeathEvent event) {
		Player p = (Player) event.getEntity();
		if(getGame(p) != null){
			event.getDrops().clear();
			respawn(p);
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		if(getGame(player) != null){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
					if(langFile.getString("death") != null)
						player.sendMessage(langFile.getString("death").replace("&", "§"));
					checkLives(player);
				}
			},(2));
		}
	}
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if(getGame(player) != null){
				//Stop Armour from braking
				if (player.getInventory().getHelmet() != null) {
					player.getInventory().getHelmet().setDurability((short) 0);
				}
				if (player.getInventory().getChestplate() != null) {
					player.getInventory().getChestplate().setDurability((short) 0);
				}
				if (player.getInventory().getLeggings() != null) {
					player.getInventory().getLeggings().setDurability((short) 0);
				}
				if (player.getInventory().getBoots() != null) {
					player.getInventory().getBoots().setDurability((short) 0);
				}
				if(getGame(player).getStarted == false){
					Damageable d = player;
					if(d.getHealth() < 1){
						event.setCancelled(true);
						if(langFile.getString("death") != null)
							player.sendMessage(langFile.getString("death").replace("&", "§"));
						checkLives(player);
					}
				}
			}
		}
	}
	
	public void checkLives(Player player){
		//Remove From Game
		try{
			Game game = getGame(player);
			User usr = game.getPlayerList.get(getIndex(player));
			int newLives = usr.getLives() - 1;
			if(newLives >= 1){
				//User newUsr = new User(usr.getPlayer, usr.getPreviousStuff, usr.getHero, newLives, usr.getCooling, usr.getJumping, usr.getSneaking, usr.getString);
				//game.getPlayerList.set(getIndex(usr.getPlayer), newUsr);
				usr.setLives(newLives);
				if(langFile.getString("livesLeft") != null)
					player.sendMessage(langFile.getString("livesLeft").replace("&", "§").replace("<lives>", newLives+""));
				Damageable d = player;
				player.setHealth(d.getMaxHealth());
				player.setFoodLevel(20);
				tpToGame(usr.getPlayer(), game.getGameName);
				giveItems(usr.getPlayer(), usr.getHero(), true, 1);
			}else{
				leaveGame(player, false);
				for(User u : game.getPlayerList){
					if(langFile.getString("playerEliminated") != null)
						u.getPlayer().sendMessage(langFile.getString("playerEliminated").replace("&", "§").replace("<player>", player.getDisplayName()));
				}
			}
		}catch(Exception e){}
	}
	
	public void joinGame(Player player, String game){
		game = game.trim();
		if(findGame(game) == null){
			if(!restoringMaps.contains(game)){
				int lives = saveFile.getInt("gameInfo." + game + ".startLives");
				//System.out.println("createGame = " + langFile.getString("createGame"));
				if(langFile.getString("createGame") != null)
					player.sendMessage(langFile.getString("createGame").replace("&", "§"));
				Game gm = new Game(game, lives, true);
				gameList.add(gm);
			}else{
				String msg = langFile.getString("gameRegening").replace("&", "§");
				//System.out.println("GR: " + langFile.getString("gameRegening").replace("&", "§"));
				player.sendMessage(msg);
				return;
			}
		}
		Game checkGame = findGame(game);
		if(checkGame.getGameName.equals(game)){
			if(checkGame.getPlayerList.size() >= 1){
				for(User user : checkGame.getPlayerList){
					if(user.getPlayer().equals(player)){
						if(langFile.getString("alreadyInGame") != null)
							player.sendMessage(langFile.getString("alreadyInGame").replace("&", "§").replace("<game>", game));
						return;
					}
				}
			}
			if(checkGame.getStarted == false){
				if(checkGame.getPlayerList.size() < getConfig().getInt("maxPlayers")){
					Damageable d = player;
					PreviousStuff ps = new PreviousStuff(player.getLocation(), player.getInventory().getContents(), player.getInventory().getArmorContents(), d.getHealth(), player.getFoodLevel(), player.getExp(), player.getLevel(), player.getGameMode());
					User newUser = new User(player, ps, getConfig().getString("defaultHero"), checkGame.getStartLives);
					//checkGame.getPlayerList.add(newUser);
					checkGame.addPlayer(newUser);
					//TP player To Lobby
					//World world = this.getServer().getWorld(saveFile.getString("gameInfo." + checkGame.getGameName + ".world"));
					Location lobbyLoc = getLocation("gameInfo." + checkGame.getGameName + ".lobbyLock", saveFile);
					player.teleport(lobbyLoc);
					for (PotionEffect effect : player.getActivePotionEffects())
				        player.removePotionEffect(effect.getType());
					player.setGameMode(GameMode.SURVIVAL);
					player.getInventory().clear();
				    player.getInventory().setHelmet(null);
				    player.getInventory().setChestplate(null);
				    player.getInventory().setLeggings(null);
				    player.getInventory().setBoots(null);
					player.updateInventory();
					if(getConfig().getBoolean("enableMenu") == true)
						openHeroMenu(player);
					giveLobbyItems(player);
					//Do timer
					if(checkGame.getTimeElapsed == 0){
						startGameTimer(game);
					}
					///Send message
					for(User user : checkGame.getPlayerList){
						if(langFile.getString("playerJoin") != null)
							user.getPlayer().sendMessage(langFile.getString("playerJoin").replace("&", "§").replace("<player>", player.getDisplayName()));
					}
				}else
					player.sendMessage(langFile.getString("gameFull").replace("&", "§").replace("<game>", game));
			}
		}else{
			if(langFile.getString("gameNameNotFound") != null)
				player.sendMessage(langFile.getString("gameNameNotFound").replace("&", "§").replace("<game>", game));
		}
	}
	
//	@SuppressWarnings("deprecation")
//	private HashMap<Location, ItemStack> getMapBlocks(Location loc1, Location loc2, World w){
//		//System.out.println("World = " + w);//------------------------DEBUG
//		HashMap<Location, ItemStack> mapBackUp;
//		mapBackUp = new HashMap<Location, ItemStack>();
//	    int minx = Math.min(loc1.getBlockX(), loc2.getBlockX()),
//	    miny = Math.min(loc1.getBlockY(), loc2.getBlockY()),
//	    minz = Math.min(loc1.getBlockZ(), loc2.getBlockZ()),
//	    maxx = Math.max(loc1.getBlockX(), loc2.getBlockX()),
//	    maxy = Math.max(loc1.getBlockY(), loc2.getBlockY()),
//	    maxz = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
//	    for (int x = minx; x<=maxx;x++) {
//	        for (int y = miny; y<=maxy;y++) {
//	            for (int z = minz; z<=maxz;z++) {
//                    //System.out.println("get block");//------------------------DEBUG
//	                Block b = w.getBlockAt(x, y, z);
//                    //Get ItemStack
//                    ItemStack i = null;
//                    //System.out.println("GMP3");//------------------------DEBUG
//                    if(!b.getType().equals(Material.AIR))
//                    	i = new ItemStack(b.getType(), 1, b.getData());
//                    //System.out.println("GMP4");//------------------------DEBUG
//                    mapBackUp.put(b.getLocation(), i);
//	            }
//	        }
//	    }
//		return mapBackUp;
//	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public void leaveGame(Player player, boolean showMsg){
		//System.out.println("SW: LG");
		try{
			User rUser = null;
			Game g = null;
			for(Game checkGame : (ArrayList<Game>)gameList.clone())
				if(checkGame.getPlayerList.size() >= 1)
					for(User user : checkGame.getPlayerList)
						if(user.getPlayer().equals(player)){
							rUser = user;
							g = checkGame;
							break;
						}
			//Can Leave Check
			if(g.getMapBackUp == null && (!g.getGameName.contains("#NOTGAME#"))){
				if(!g.getWantToLeaveList.contains(player))
					g.setWantsToLeave(player);
				return;
			}
			//MSG
			for(User user2 : g.getPlayerList){
				if(showMsg == true){
					if(langFile.getString("playerLeave") != null)
						user2.getPlayer().sendMessage(langFile.getString("playerLeave").replace("&", "§").replace("<player>", player.getDisplayName()));
				}
			}
			try{
				if (getConfig().getBoolean("enableBossBar") == true){
					clearBossBar(player);
				}
			}catch(Exception e){}
			for (PotionEffect effect : player.getActivePotionEffects())
		        player.removePotionEffect(effect.getType());
			//Mount
			player.eject();
			Entity cart = new Hero(this).getCart(player);
			if(cart != null){
				cart.eject();
				cart.remove();
			}
			try{
				PreviousStuff pe = rUser.getPreviousStuff();
				Location preLock = pe.getLocation;
				int preFood = pe.getFood;
				double preHealth = pe.getHealth;
				float preXP = pe.getXP;
				int preLvl = pe.getLevel;
				ItemStack[] preInv = pe.getInventory;
				ItemStack[] preAmour = pe.getArmour;
				player.setFallDistance(0);
				player.teleport(preLock);
				player.setVelocity(new Vector(0, 0, 0));
				player.setFoodLevel(preFood);
				player.setFireTicks(0);
				player.setLevel(preLvl);
				player.setExp(preXP);
				player.setHealth(preHealth);
				player.getInventory().clear();
				player.getInventory().setContents(preInv);
				player.getInventory().setArmorContents(preAmour);
				player.updateInventory();
				player.setGameMode(pe.getGameMode);
				//DisguiseAPI.undisguiseToAll(player);
				resetSkin(player);
			}catch(Exception e){}
			g.getPlayerList.remove(rUser);
			player.setAllowFlight(false);
			return;
		}catch(Exception e){e.printStackTrace();}
		if(langFile.getString("notInGame") != null)
			player.sendMessage(langFile.getString("notInGame").replace("&", "§"));
	}
	
	public void clearBossBar(Player player){
		for (Map.Entry<Entity, BossBar> hm : bossBars.entrySet())
			if(hm.getValue().getPlayers().contains(player))
				hm.getValue().removePlayer(player);
		if(bossBars.containsKey(player))
			bossBars.remove(player);
	}
	
	@SuppressWarnings("unchecked")
	public Game findGame(String gameSearchName) {
	    for (Game game : (ArrayList<Game>)gameList.clone()){
	        if((game.getGameName != null) && (game.getGameName.equals(gameSearchName))){
	        	return game;
	        }
	    }
	    return null;
	}

	@SuppressWarnings("unchecked")
	public void startGameTimer(final String gameName){
		//System.out.println("GTT");//------------------------DEBUG
		if(gameList != null){
			for(Game checkGame : (ArrayList<Game>)gameList.clone()){
				if(checkGame.getGameName.equals(gameName)){
					if(checkGame.getPlayerList.size() <= 0){
						stopGame(gameName);
					}else if((checkGame.getPlayerList.size() <= 1) && (checkGame.getStarted == true)){
						stopGame(gameName);
					}
					int realGameTime = saveFile.getInt("gameInfo." + checkGame.getGameName + ".gameTime");
					int realLobbyTime = saveFile.getInt("gameInfo." + checkGame.getGameName + ".lobbyTime");
					int timeElapsed = checkGame.getTimeElapsed;
					if((timeElapsed == realLobbyTime) && (checkGame.getStarted == false)){
						startGame(gameName);
					}else if((timeElapsed >= realGameTime) && (checkGame.getStarted == true)){
						stopGame(gameName);
					}
					if(checkGame.getStarted == false){
						for(User user : checkGame.getPlayerList){
							//Get Player
							Player player = user.getPlayer();
							//Fix Health and Hunger
							Damageable d = player;
							player.setHealth(d.getMaxHealth());
							player.setFoodLevel(20);
							//Print Time
							if(langFile.getString("timeStart") != null){
								if(timeElapsed == realLobbyTime-10){
										player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "10"));
								}else if(timeElapsed == realLobbyTime-9){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "9"));
								}else if(timeElapsed == realLobbyTime-8){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "8"));
								}else if(timeElapsed == realLobbyTime-7){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "7"));
								}else if(timeElapsed == realLobbyTime-6){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "6"));
								}else if(timeElapsed == realLobbyTime-5){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "5"));
								}else if(timeElapsed == realLobbyTime-4){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "4"));
								}else if(timeElapsed == realLobbyTime-3){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "3"));
								}else if(timeElapsed == realLobbyTime-2){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "2"));
								}else if(timeElapsed == realLobbyTime-1){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", "1"));
								}else if(timeElapsed == realLobbyTime/2){
									player.sendMessage(langFile.getString("timeStart").replace("&", "§").replace("<time>", (realLobbyTime/2)+""));
								}
							}
							//Set XP Bar Number
							int timeLeft = (realLobbyTime-timeElapsed);
							player.setLevel(timeLeft);
							//Set XP Bar Level
	    	            	float time = (float) realLobbyTime-timeElapsed;
	    	            	float maxTime = (float) realLobbyTime;
	    	            	float setTime = (time * 100.0f) / maxTime;
	    	            	setTime = setTime/100.0f;
	    	            	player.setExp(setTime);
						}
					}else if(checkGame.getStarted == true){
						for(User user : checkGame.getPlayerList){
							Player player = user.getPlayer();
							if(langFile.getString("timeEnd") != null){
								if(timeElapsed == realGameTime-60){
										player.sendMessage(langFile.getString("timeEnd").replace("&", "§").replace("<time>", "60"));
								}else if(timeElapsed == realGameTime-30){
									player.sendMessage(langFile.getString("timeEnd").replace("&", "§").replace("<time>", "30"));
								}else if(timeElapsed == realGameTime-10){
									player.sendMessage(langFile.getString("timeEnd").replace("&", "§").replace("<time>", "10"));
								}
							}
							//Set XP Bar Number
							int timeLeft = (realGameTime-timeElapsed);
							player.setLevel(timeLeft);
							//Set XP Bar Level
	    	            	float time = (float) realGameTime-timeElapsed;
	    	            	float maxTime = (float) realGameTime;
	    	            	float setTime = (time * 100.0f) / maxTime;
	    	            	setTime = setTime/100.0f;
	    	            	player.setExp(setTime);
						}
					}
					//System.out.println("Game time at " + timeElapsed);//------------------------DEBUG
					timeElapsed = timeElapsed + 1;
					try{
						checkGame.setTime(timeElapsed);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
						     public void run() {
								startGameTimer(gameName);
							 }
						}, (1 * 20));
					}catch(Exception e){System.out.println("startGameTimer: " + e);}
					//System.out.println("Game time now at " + timeElapsed);//------------------------DEBUG
				}
			}
		}
	}
	
	public void startGame(String gameName){
		//Start Game
		//System.out.println("STG1");//------------------------DEBUG
		for(Game checkGame : gameList)
			if(checkGame.getGameName.equals(gameName))
				if(checkGame.getMapBackUp != null){
					if(checkGame.getPlayerList.size() > 1){
						checkGame.setStarted(true);
						for(User user : checkGame.getPlayerList){
							tpToGame(user.getPlayer(), gameName);
							giveItems(user.getPlayer(), user.getHero(), true, checkGame.getPlayerList.indexOf(user)*10);
							if(langFile.getString("gameStart") != null)
								user.getPlayer().sendMessage(langFile.getString("gameStart").replace("&", "§").replace("<game>", gameName));
						}
						return;
					}
				}else
					for(User u : checkGame.getPlayerList)
						u.getPlayer().sendMessage("§cGame can't start because the map is not backed up!");
		stopGame(gameName);
	}
	
	public void disguise(LivingEntity player, String hero){
		//Disguise
		PlayerDisguise pDisguise;
		if(getConfig().getBoolean("useHeroNames")){
			pDisguise = new PlayerDisguise(hero);
			pDisguise.setSkin(getConfig().getString("heroDisguises." + hero));
		}else
			pDisguise = new PlayerDisguise(getConfig().getString("heroDisguises." + hero));
		DisguiseAPI.disguiseToAll(player, pDisguise);
	}
	
//	public void changeSkin(Player p, String s){
//		JavaPlugin.getPlugin(CustomSkins.class).getData().setUsedSkin(p.getUniqueId(), s);
//		JavaPlugin.getPlugin(CustomSkins.class).getSkinHandler().updatePlayerSkin(p.getUniqueId());
//	}

	public void resetSkin(Player p){
		 if(getConfig().getBoolean("useHeroDisguises")){
			 DisguiseAPI.undisguiseToAll(p);
		 }//else if(getConfig().getBoolean("useHeroSkins")){
		//	 JavaPlugin.getPlugin(CustomSkins.class).getData().resetSkin(p.getUniqueId());
		//	 JavaPlugin.getPlugin(CustomSkins.class).getSkinHandler().updatePlayerSkin(p.getUniqueId());
		// }
	}
	
//	public void skinChange(Player p, String hero){
//		//String skinId = args[1].toLowerCase();
//		if (getPlugin().getData().getCachedSkin(skinId) == null) {
//			sender.sendMessage("§cSkin §6" + skinId + "§cnot found!");
//		}
//		getPlugin().getData().setUsedSkin(target.getUuid(), skinId);
//		getPlugin().getSkinHandler().updatePlayerSkin(target.getUuid());
//		sender.sendMessage("§aSkin of §6" + target.getName() + " §achanged!");
//
//	}
	
	public void giveItems(final Player player, final String hero, boolean replace, int time){
		if(time >= 0)
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
			    	 try{
			    		 if(getConfig().getBoolean("useHeroDisguises")){
			    			 disguise(player, hero);
			    		 }//else if(getConfig().getBoolean("useHeroSkins"))
			    		//	 changeSkin(player, hero);
			    	 }catch(Exception x){}
				 }
			}, (time));
		if(replace == true){
			//Clear Players Stuff
			player.getInventory().clear();
			player.getInventory().setHelmet(null);
			player.getInventory().setChestplate(null);
			player.getInventory().setLeggings(null);
			player.getInventory().setBoots(null);
		}
		//Create Compass
		ItemStack compass = new ItemStack(Material.COMPASS);
		ItemMeta meta = compass.getItemMeta();
		meta.setDisplayName("Player Target");
		compass.setItemMeta(meta);
		//Create Armour
		ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
		ItemStack ches = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemStack pant = new ItemStack(Material.LEATHER_LEGGINGS);
		ItemStack boot = new ItemStack(Material.LEATHER_BOOTS);
		//System.out.println("Hero = " + hero);
		//Give Players Stuff
		if(hero.equals("CaptainAmerica")){
			//Give Equipment
			player.getInventory().addItem(CAClass.getGun());
			player.getInventory().addItem(CAClass.getShield());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color capBlue = Color.fromRGB(0,100,255);
			dye(helm, capBlue);
			dye(ches, capBlue);
			dye(pant, capBlue);
			dye(boot, capBlue);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(6 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(6 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(6 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(6 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("SpiderMan")){
			//Give Equipment
			player.getInventory().addItem(SMClass.getWeb());
			player.getInventory().addItem(SMClass.getString());
			player.getInventory().addItem(compass);
			climbHeros.add("SpiderMan");
			//Dye Armour
			Color spideyRed = Color.fromRGB(255,10,0);
			Color spideyBlue = Color.fromRGB(0,50,255);
			dye(helm, spideyRed);
			dye(ches, spideyRed);
			dye(pant, spideyBlue);
			dye(boot, spideyRed);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Hulk")){
			//Give Equipment
			player.setAllowFlight(true);
			player.getInventory().addItem(compass);
			//Dye Armour
			Color hulkGreen = Color.fromRGB(0,255,50);
			Color pantsPurp = Color.fromRGB(200,0,255);
			dye(ches, hulkGreen);
			dye(pant, pantsPurp);
			dye(boot, hulkGreen);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			//Give Armour
			ItemStack hat = new ItemStack(Material.SKULL, 1, (short) 2);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(hat);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Thor")){
			//Give Equipment
			player.getInventory().addItem(TRClass.getHammer());
			player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 1));
			player.getInventory().addItem(compass);
			//Dye Armour
			Color thorGrey = Color.fromRGB(150,150,150);
			dye(helm, thorGrey);
			dye(ches, thorGrey);
			dye(pant, thorGrey);
			dye(boot, thorGrey);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("IronMan")){
			//Give Equipment
			player.getInventory().addItem(IMClass.getUnibeam());
			player.getInventory().addItem(IMClass.getPulsars());
			player.getInventory().addItem(IMClass.getFlight());
			player.getInventory().addItem(IMClass.getMissiles());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color ironYellow = Color.fromRGB(255,255,0);
			Color ironRed = Color.fromRGB(255,10,0);
			dye(helm, ironYellow);
			dye(ches, ironRed);
			dye(pant, ironYellow);
			dye(boot, ironRed);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(7 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(7 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(7 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(7 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("BatMan")){
			//Give Equipment
			player.getInventory().addItem(BMClass.getBatarang());
			player.getInventory().addItem(BMClass.getGG());
			player.getInventory().addItem(BMClass.getTransmiter());
			player.getInventory().addItem(BMClass.getGlider());
			player.getInventory().addItem(BMClass.getGrenade(7));
			player.getInventory().addItem(BMClass.getFlashBang(10));
			player.getInventory().addItem(compass);
			//Dye Armour
			Color batBlack = Color.fromRGB(0,0,0);
			dye(helm, batBlack);
			dye(ches, batBlack);
			dye(pant, batBlack);
			dye(boot, batBlack);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Wolverine")){
			//Give Equipment
			player.getInventory().addItem(WEClass.getClaws());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color wolvYellow = Color.fromRGB(255,255,0);
			Color wolvBlack = Color.fromRGB(0,0,25);
			dye(helm, wolvYellow);
			dye(ches, wolvYellow);
			dye(pant, wolvBlack);
			dye(boot, wolvBlack);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(8 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("GreenLantern")){
			//Give Equipment
			player.getInventory().addItem(GLClass.getBuild());
			player.getInventory().addItem(GLClass.getFly());
			player.getInventory().addItem(GLClass.getBeam());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color greenGreen = Color.fromRGB(0,255,00);
			Color greenBlack = Color.fromRGB(0,0,0);
			dye(helm, greenGreen);
			dye(ches, greenGreen);
			dye(pant, greenBlack);
			dye(boot, greenBlack);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("AquaMan")){
			//Give Equipment
			player.getInventory().addItem(AquaMan.getTrident());
			player.getInventory().addItem(AquaMan.getWater());
			player.getInventory().addItem(AquaMan.getFish());
			player.getInventory().addItem(new ItemStack(Material.WATER, 10));
			player.getInventory().addItem(compass);
			//Dye Armour
			Color aquaYellow = Color.fromRGB(200,180,0);
			Color aquaGreen = Color.fromRGB(0,30,80);
			dye(ches, aquaYellow);
			dye(pant, aquaGreen);
			dye(boot, aquaGreen);
			//Add Enchantments
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("HumanTorch")){
			//Give Equipment
			player.getInventory().addItem(HTClass.getFire());
			player.getInventory().addItem(HTClass.getCharge());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color fireORange = Color.fromRGB(200,75,10);
			dye(helm, fireORange);
			dye(ches, fireORange);
			dye(pant, fireORange);
			dye(boot, fireORange);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("NightCrawler")){
			//Give Equipment
			player.getInventory().addItem(NCClass.getTail());
			player.getInventory().addItem(NCClass.getTP());
			player.getInventory().addItem(NCClass.getTP2());
			player.getInventory().addItem(compass);
			climbHeros.add("NightCrawler");
			//Dye Armour
			Color nightBlue = Color.fromRGB(0,20,60);
			dye(helm, nightBlue);
			dye(ches, nightBlue);
			dye(pant, nightBlue);
			dye(boot, nightBlue);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(3 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Hawkeye")){
			//Give Equipment
			player.getInventory().addItem(HEClass.getBow());
			player.getInventory().addItem(HEClass.getArrow1());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color hawkBlack = Color.fromRGB(50,50,50);
			Color hawkPurp = Color.fromRGB(30,0,30);
			dye(ches, hawkPurp);
			dye(pant, hawkBlack);
			dye(boot, hawkBlack);
			//Add Enchantments
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Storm")){
			//System.out.println("Giving storm stuff to " + player.getName());
			//Give Equipment
			player.getInventory().addItem(STClass.getWind());
			player.getInventory().addItem(STClass.getWeather());
			player.getInventory().addItem(STClass.getRain("off"));
			player.getInventory().addItem(STClass.getFly("off"));
			player.getInventory().addItem(compass);
			//Dye Armour
			Color stormWhite = Color.fromRGB(255,255,255);
			dye(ches, stormWhite);
			dye(pant, stormWhite);
			dye(boot, stormWhite);
			//Add Enchantments
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(2 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(2 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(2 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Joker")){
			//System.out.println("Giving storm stuff to " + player.getName());
			//Give Equipment
			Game g = getGame(player);
			int amount = g.getPlayerList.size() * 2;
			if(amount > 10){amount = 10;}
			player.getInventory().addItem(JKClass.getCards(amount));
			player.getInventory().addItem(JKClass.getFlower());
			player.getInventory().addItem(JKClass.getGrenade());
			player.getInventory().addItem(JKClass.getExplosive());
			player.getInventory().addItem(JKClass.getGun());
			player.getInventory().addItem(compass);
			//Dye Armour
			//Color jokerWhite = Color.fromRGB(255,255,255);
			Color jokerPurple = Color.fromRGB(150,10,150);
			dye(ches, jokerPurple);
			dye(pant, jokerPurple);
			dye(boot, jokerPurple);
			//Add Enchantments
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("SuperMan")){
			//System.out.println("Giving storm stuff to " + player.getName());
			//Give Equipment
			player.getInventory().addItem(CKClass.getVision());
			player.getInventory().addItem(CKClass.getBreath());
			player.getInventory().addItem(CKClass.getSpeed());
			player.getInventory().addItem(CKClass.getFlight());
			player.getInventory().addItem(compass);
			//Dye Armour
			//Color jokerWhite = Color.fromRGB(255,255,255);
			Color superBlue = Color.fromRGB(0,10,200);
			Color superRed = Color.fromRGB(200,10,0);
			dye(ches, superBlue);
			dye(pant, superRed);
			dye(boot, superBlue);
			//Add Enchantments
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Flash")){
			//System.out.println("Giving storm stuff to " + player.getName());
			//Give Equipment
			player.getInventory().addItem(FLClass.getSpeed());
			player.getInventory().addItem(FLClass.getFist());
			player.getInventory().addItem(FLClass.getTime());
			player.getInventory().addItem(compass);
			//Dye Armour
			//Color jokerWhite = Color.fromRGB(255,255,255);
			Color red = Color.fromRGB(250,10,0);
			Color yellow = Color.fromRGB(200,190,0);
			dye(helm, red);
			dye(ches, red);
			dye(pant, red);
			dye(boot, yellow);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Deadpool")){
			player.setAllowFlight(true);
			//Give Equipment
			player.getInventory().addItem(DPClass.getSword());
			player.getInventory().addItem(DPClass.getPistol());
			player.getInventory().addItem(DPClass.getGrenades(5));
			player.getInventory().addItem(DPClass.getTPDevice());
			player.getInventory().addItem(compass);
			//Dye Armour
			//Color jokerWhite = Color.fromRGB(255,255,255);
			Color red = Color.fromRGB(250,10,0);
			Color black = Color.fromRGB(0,0,0);
			dye(helm, red);
			dye(ches, black);
			dye(pant, red);
			dye(boot, black);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("ScarletWitch")){
			//Give Equipment
			player.getInventory().addItem(SWClass.getFly("off"));
			player.getInventory().addItem(SWClass.getHexBolt());
			player.getInventory().addItem(SWClass.getRealityBend());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color red = Color.fromRGB(250,10,0);
			dye(helm, red);
			dye(ches, red);
			dye(boot, red);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Venom")){
			//Give Equipment
			player.getInventory().addItem(VClass.getClaws());
			player.getInventory().addItem(VClass.getWeb());
			player.getInventory().addItem(compass);
			climbHeros.add("Venom");
			//Dye Armour
			Color black = Color.fromRGB(0,0,0);
			dye(helm, black);
			dye(ches, black);
			dye(pant, black);
			dye(boot, black);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(4 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hero.equals("Loki")){
			//Give Equipment
			player.getInventory().addItem(LClass.getStaff());
			player.getInventory().addItem(LClass.getShift());
			player.getInventory().addItem(compass);
			//Dye Armour
			Color gold = Color.fromRGB(232,188,14);
			Color green = Color.fromRGB(10,122,0);
			Color black = Color.fromRGB(0,0,0);
			dye(helm, gold);
			dye(ches, green);
			dye(pant, black);
			dye(boot, black);
			//Add Enchantments
			helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
			//Give Armour
			if(getConfig().getBoolean("useHeroDisguises") == false){
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if (hero.equals("Shazam")){
		      player.getInventory().addItem(new ItemStack[] { this.SHClass.getLightning() });
		      player.getInventory().addItem(new ItemStack[] { this.SHClass.getSpeed() });
		      player.getInventory().addItem(new ItemStack[] { this.SHClass.getFlight() });
		      player.getInventory().addItem(new ItemStack[] { compass });
		      
		      Color red = Color.fromRGB(255, 0, 0);
		      dye(ches, red);
		      dye(pant, red);
		      dye(boot, red);
		      
		      ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1.0D * getArmour()));
		      pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1.0D * getArmour()));
		      boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1.0D * getArmour()));
		      if (!getConfig().getBoolean("useHeroDisguises"))
		      {
		        player.getInventory().setChestplate(ches);
		        player.getInventory().setLeggings(pant);
		        player.getInventory().setBoots(boot);
		      }
		}else if(hasPack1() && hero.equals("Galactus")){
			//Give Equipment
			Galactus GAClass = new Galactus(this);
			player.getInventory().addItem(GAClass.getEnergy());
			player.getInventory().addItem(GAClass.getFly("off"));
			player.getInventory().addItem(GAClass.getHealer());
			player.getInventory().addItem(GAClass.getTP());
			player.getInventory().addItem(GAClass.getPower());
			player.getInventory().addItem(compass);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				//Dye Armour
				Color purple = Color.fromRGB(123,81,214);
				Color pink = Color.fromRGB(214,81,210);
				dye(helm, pink);
				dye(ches, purple);
				dye(pant, purple);
				dye(boot, pink);
				//Add Enchantments
				helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				//Give Armour
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hasPack1() && hero.equals("Groot")){
			//Give Equipment
			Groot GTClass = new Groot(this);
			player.getInventory().addItem(GTClass.getRoot());
			player.getInventory().addItem(GTClass.getGrow());
			player.getInventory().addItem(GTClass.getAxe());
			player.getInventory().addItem(GTClass.getWood());
			player.getInventory().addItem(compass);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				//Dye Armour
				Color brown = Color.fromRGB(125,87,25);
				dye(helm, brown);
				dye(ches, brown);
				dye(pant, brown);
				dye(boot, brown);
				//Add Enchantments
				helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				//Give Armour
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hasPack1() && hero.equals("Magneto")){
			//Give Equipment
			Magneto MGClass = new Magneto(this);
			player.getInventory().addItem(MGClass.getMagnet());
			player.getInventory().addItem(MGClass.getFly("off"));
			player.getInventory().addItem(MGClass.getPower());
			player.getInventory().addItem(compass);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				//Dye Armour
				Color red = Color.fromRGB(224,4,45);
				Color purple = Color.fromRGB(133,54,179);
				dye(helm, red);
				dye(ches, red);
				dye(pant, purple);
				dye(boot, purple);
				//Add Enchantments
				helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				//Give Armour
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hasPack1() && hero.equals("IceMan")){
			//Give Equipment
			IceMan IMClass = new IceMan(this);
			player.getInventory().addItem(IMClass.getIce());
			player.getInventory().addItem(IMClass.getIceBlock());
			player.getInventory().addItem(compass);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				//Dye Armour
				Color blue = Color.fromRGB(34,216,230);
				dye(helm, blue);
				dye(ches, blue);
				dye(pant, blue);
				dye(boot, blue);
				//Add Enchantments
				helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				//Give Armour
				player.getInventory().setHelmet(helm);
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}else if(hasPack1() && hero.equals("Professor X")){
			//Give Equipment
			ProfessorX PXClass = new ProfessorX(this);
			player.getInventory().addItem(PXClass.getTelepathy());
			player.getInventory().addItem(PXClass.getSuperTelepathy());
			player.getInventory().addItem(PXClass.getChairControl());
			player.getInventory().addItem(PXClass.getDetector(false));
			player.getInventory().addItem(compass);
			if(getConfig().getBoolean("useHeroDisguises") == false){
				//Dye Armour
				Color black = Color.fromRGB(0,0,0);
				dye(ches, black);
				dye(pant, black);
				dye(boot, black);
				//Add Enchantments
				ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, (int)(1 * getArmour()));
				//Give Armour
				player.getInventory().setChestplate(ches);
				player.getInventory().setLeggings(pant);
				player.getInventory().setBoots(boot);
			}
		}
		potions(player, hero);
	}
	
	@SuppressWarnings("unchecked")
	public void tpToGame(Player player, String gameName){
		for(Game checkGame : (ArrayList<Game>)gameList.clone()){
			if(checkGame.getGameName.equals(gameName)){
				if(checkGame.getPlayerList.size() > 1){
					ArrayList<Location> spawnLocList = new ArrayList<Location>();
					for(String locName : saveFile.getConfigurationSection("gameInfo." + checkGame.getGameName + ".spawnLocks").getKeys(false)){
						Location spawnPoint = getLocation("gameInfo." + checkGame.getGameName + ".spawnLocks." + locName, saveFile);
						spawnLocList.add(spawnPoint);
					}
				    int index = new Random().nextInt(spawnLocList.size());
				    Location tpToLocOne = spawnLocList.get(index);
				    player.teleport(tpToLocOne);
				    if(playerIsNear(player) == true){
			    		for(Location tpToLocTwo : spawnLocList){
			    			player.teleport(tpToLocTwo);
			    			if(playerIsNear(player) == false){
			    				return;
			    			}
			    		}
			    		if(playerIsNear(player) == false){
			    			player.teleport(tpToLocOne);
			    		}
				    }
			        return;
				}
			}
		}
	}
	
	public boolean playerIsNear(Player player){
	    for(Entity ent : player.getNearbyEntities(5, 5, 5)){
	    	if(ent instanceof Player){
	    		return true;
	    	}
	    }
	    return false;
	}
	
	@SuppressWarnings("unchecked")
	public void stopGame(String gameName){
		Game rGame = null;
		for(Game checkGame : (ArrayList<Game>)gameList.clone()){
			if(checkGame.getGameName.equals(gameName)){
				if(checkGame.getTimeElapsed <= saveFile.getInt("gameInfo." + gameName + ".lobbyTime")){
					if(langFile.getString("gameConclude") != null)
						for(User user : checkGame.getPlayerList)
							user.getPlayer().sendMessage(langFile.getString("gameConclude").replace("&", "§").replace("<game>", gameName));
				}else if(checkGame.getPlayerList.size() == 1){
					Player p = checkGame.getPlayerList.get(0).getPlayer();
					if(langFile.getString("gameConclude") != null)
						for(User user : checkGame.getPlayerList)
							user.getPlayer().sendMessage(langFile.getString("gameConclude").replace("&", "§").replace("<game>", gameName).replace("<winner>",  p.getDisplayName()));
					//Give Reward
					giveReward(p);
				}else{
					double highestHealth = 0;
					Player winner = null;
					for(User u : checkGame.getPlayerList){
						Damageable d = u.getPlayer();
						if(d.getHealth() > highestHealth){
							highestHealth = d.getHealth();
							winner = u.getPlayer();
						}
					}
					int pht = 0;
					for(User u : checkGame.getPlayerList){
						Damageable d = winner;
						Damageable d2 = u.getPlayer();
						if(d.getHealth() > d2.getHealth()){
							pht = pht + 1;
						}
					}
					if(pht >= checkGame.getPlayerList.size()){
						if(langFile.getString("gameConclude") != null)
							for(User user : checkGame.getPlayerList)
								user.getPlayer().sendMessage(langFile.getString("gameConclude").replace("&", "§").replace("<game>", gameName).replace("<winner>",  winner.getDisplayName()));
						//Give Reward
						giveReward(winner);
					}/*else{
						this.getServer().broadcastMessage("§2SuperWars: game " + gameName + " §2has ended with no victor!");
					}*/
				}
				rGame = gameList.get(gameList.indexOf(checkGame));
			}
		}
		try{
			for(User eUser : (ArrayList<User>)rGame.getPlayerList.clone()){
				Player player = eUser.getPlayer();
				leaveGame(player, false);
			}
			rGame.saveMapBlocks(false);
			gameList.remove(rGame);
		}catch(Exception e){System.out.println("stopGame: " + e);}
	}
	
	@SuppressWarnings("deprecation")
	public void giveReward(Player p){
		try{
			if(getConfig().getBoolean("enableMoneyRewards") == true){
				int amount = getConfig().getInt("rewardAmount");
				eco.depositPlayer(p.getName(), amount);
				if(langFile.getString("reward") != null)
					p.sendMessage(langFile.getString("reward").replace("&", "§").replace("<amount>", amount+""));
			}
		}catch(Exception x){System.out.println("SuperWars: Reward could not be given!");}
	}
	
//	@SuppressWarnings("deprecation")
//	private void restoreBlocks_old(Game game){
//		HashMap<Location, ItemStack> hashmap = game.getMapBackUp;
//		//System.out.println("SuperWars: Restoring " + game.getGameName + " Game's Blocks");
//		//String worldName = saveFile.getString("gameInfo." + game.getGameName + ".world");
//		//World world = this.getServer().getWorld(worldName);
//		for (Entry<Location, ItemStack> hm : hashmap.entrySet()) {
//		    Location loc = hm.getKey();
//		    ItemStack iStack = null;
//		    if(hm.getValue() != null)
//		    	iStack = hm.getValue();
//		    Block block = loc.getBlock();
//		    if(iStack != null){
//		    	if((block.getTypeId() != (iStack.getTypeId())) || (block.getData() != (iStack.getDurability()))){
//		    		block.setTypeIdAndData(iStack.getTypeId(), (byte) iStack.getDurability(), false);
//		    	}
//		    }else if(!block.getType().equals(Material.AIR)){
//		    	//System.out.println("Setting " + block.getType() + " to air!");//------------------------DEBUG
//		    	block.setType(Material.AIR);
//		    }
//		}
//	}
	
    public void dye(ItemStack item, Color color){
    	try{
			LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
			meta.setColor(color);
			item.setItemMeta(meta);
    	}catch(Exception e){}
    }
    
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
       
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
        //System.out.println("subchannel" + subchannel);
        if (subchannel.equals("PlayerCount")) {
        	try{
	            String server = in.readUTF();
	            int playerCount = in.readInt();
	            SPCount.put(server, playerCount);
        	}catch(Exception x){}
        }else if(subchannel.equals("GetServers")){
        	String[] serverList = in.readUTF().split(", ");
        	servers = new ArrayList<String>(Arrays.asList(serverList));
        	//System.out.println("servers" + servers.toString());
        }else if(subchannel.equals("GameStartedInfo")){
            String server = in.readUTF();
        	boolean started = in.readBoolean();
        	SGStarted.put(server, started);
        }
       
    }
    
	@EventHandler(priority=EventPriority.HIGH)
	public void onSignChange(SignChangeEvent event){
		Player player = event.getPlayer();
		String[] text = event.getLines();
		if(text[0].equals("[SuperWars]")){
			String game;
			try{
				game = text[2];
				if((hasDedicatedMode() && servers.contains(game)) || (saveFile.getString("gameInfo." + game) != null)){
					event.setLine(0, "§l§nSuperWars");
					event.setLine(1, "§2Join");
					Location loc = event.getBlock().getLocation();
					String signName = (loc.getX() + "." + loc.getY() + "." + loc.getZ());
					signName = signName.replace(".", "");
//					saveFile.set("signList." + signName + ".x", loc.getX());
//					saveFile.set("signList." + signName + ".y", loc.getY());
//					saveFile.set("signList." + signName + ".z", loc.getZ());
//					saveFile.set("signList." + signName + ".world", loc.getWorld().getName());
					setLocation("signList." + signName, loc, saveFile);
					saveSaveFile();
					return;
				}
			}catch(Exception e){}
			if(langFile.getString("gameNotFound") != null)
				player.sendMessage(langFile.getString("gameNotFound").replace("&", "§"));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateSigns(){
		try{
			if(saveFile.getConfigurationSection("signList").getKeys(false) != null){
				for(String signName : saveFile.getConfigurationSection("signList").getKeys(false)){
					//String worldName = saveFile.getString("signList." + signName + ".world");
					//World world = this.getServer().getWorld(worldName);
					Location loc = getLocation("signList." + signName, saveFile);
					if((loc.getBlock().getType().equals(Material.SIGN_POST)) || (loc.getBlock().getType().equals(Material.WALL_SIGN))){
						Sign sign = (Sign) loc.getBlock().getState();
						String[] text = sign.getLines();
						//System.out.println("US Text: " + text[0]);//------------------------DEBUG
						if(text[0].equals("§l§nSuperWars")){
							//System.out.println("US1");//------------------------DEBUG
							String gameName = text[2];
							if(hasDedicatedMode()){
								//Dedicated Mode Signs
								int count = 0;
								if(SPCount.get(gameName) != null)
									count = SPCount.get(gameName);
								boolean started = false;
								if(SGStarted.get(gameName) != null)
									started = SGStarted.get(gameName);
								if(!started){
									sign.setLine(1, "§2Join[§2" + count + "]");
							        sign.update();
								}else if(started){
									sign.setLine(1, "§4Started[§4" + count + "]");
							        sign.update();
								}
								//Send Player Count Request
						        ByteArrayDataOutput out = ByteStreams.newDataOutput();
						        out.writeUTF("PlayerCount");
						        out.writeUTF(gameName);
						        getServer().sendPluginMessage(this, "BungeeCord", out.toByteArray());
						        //Send Game Started Request
						        ByteArrayDataOutput out2 = ByteStreams.newDataOutput();
						        out2.writeUTF("GameStarted");
						        out2.writeUTF(gameName);
						        getServer().sendPluginMessage(this, "BungeeCord", out2.toByteArray());
							}else{
								//Save Server Game Signs
								boolean onGame = false;
								for(Game game : (ArrayList<Game>)gameList.clone()){
									if((game.getGameName.equals(gameName)) && (game.getStarted == false)){
										sign.setLine(1, "§2Join[§2" + game.getPlayerList.size() + "]");
								        sign.update();
								        onGame = true;
									}else if((game.getGameName.equals(gameName)) && (game.getStarted == true)){
										sign.setLine(1, "§4Started[§4" + game.getPlayerList.size() + "]");
								        sign.update();
								        onGame = true;
									}
								}
								if(!(saveFile.getString("gameInfo." + gameName) != null)){
									sign.setLine(1, "§cError!");
							        sign.update();
								}else if(onGame == false){
									sign.setLine(1, "§2Join");
							        sign.update();
								}
							}
						}
					}else{
						//System.out.println("Sign location at X:" + loc.getX() + " Y:" + loc.getY() + " Z:" + loc.getZ() + " invalid, removing sign!");
						//loc.getBlock().setType(Material.EMERALD_BLOCK);
						removeSign(loc);
					}
				}
			}
		}catch(Exception e){/*System.out.println("Error updating signs: ");e.printStackTrace();*/}
		applyPotionEffects();
		pointCompasses();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
		     public void run() {
		    	 updateSigns();
			 }
		}, (10 * 20));
	}
	
	public void pointCompasses(){
		for(Game game : gameList){
			for(User user : game.getPlayerList){
				Player player = user.getPlayer();
				Player target = null;
				try{
					for(Entity e : player.getNearbyEntities(50, 50, 50)){
						if(e instanceof Player){
							target = (Player) e;
						}
					}
					player.setCompassTarget(target.getLocation());
				}catch(Exception e){}
			}
		}
	}
	
	public void applyPotionEffects(){
		try{
			for(Game game : gameList){
				if(game.getStarted == true){
					for(User user : game.getPlayerList){
						String h = user.getHero();
						Player player = user.getPlayer();
						if (locIsInGame(player.getLocation(), game) == true) {
							potions(player, h);
						}else{
							if(getConfig().getBoolean("killBoundaryLeavers")){
								if(langFile.getString("boundariesKill") != null)
									player.sendMessage(langFile.getString("boundariesKill").replace("&", "§"));
								checkLives(player);
							}else if(!game.getGameName.contains("#NOTGAME#")){
								if(getConfig().getBoolean("tpBoundaryLeavers")){
									if(langFile.getString("boundariesTP") != null)
										player.sendMessage(langFile.getString("boundariesTP").replace("&", "§"));
									tpToGame(player, game.getGameName);
								}
							}
						}
					}
				}
			}
		}catch(Exception e){/*System.out.println("Potions: "); e.printStackTrace();*/}
	}
	
	@SuppressWarnings("static-access")
	private void potions(Player player, String h){
		if(h.equals("CaptainAmerica")){
			CAClass.addPotionEffects(player);
		}else if(h.equals("SpiderMan")){
			SMClass.addPotionEffects(player);
		}else if(h.equals("Thor")){
			TRClass.addPotionEffects(player);
		}else if(h.equals("Hulk")){
			HKClass.addPotionEffects(player);
		}else if(h.equals("IronMan")){
			IMClass.addPotionEffects(player);
		}else if(h.equals("BatMan")){
			BMClass.addPotionEffects(player);
		}else if(h.equals("Wolverine")){
			WEClass.addPotionEffects(player);
		}else if(h.equals("GreenLantern")){
			GLClass.addPotionEffects(player);
		}else if(h.equals("AquaMan")){
			AquaMan.addPotionEffects(player);
		}else if(h.equals("HumanTorch")){
			HTClass.addPotionEffects(player);
		}else if(h.equals("NightCrawler")){
			NCClass.addPotionEffects(player);
		}else if(h.equals("Hawkeye")){
			HEClass.addPotionEffects(player);
		}else if(h.equals("Storm")){
			STClass.addPotionEffects(player);
		}else if(h.equals("Joker")){
			JKClass.addPotionEffects(player);
		}else if(h.equals("SuperMan")){
			CKClass.addPotionEffects(player);
		}else if(h.equals("Flash")){
			FLClass.addPotionEffects(player);
		}else if(h.equals("Deadpool")){
			DPClass.addPotionEffects(player);
		}else if(h.equals("ScarletWitch")){
			SWClass.addPotionEffects(player);
		}else if(h.equals("Venom")){
			VClass.addPotionEffects(player);
		}else if(h.equals("Loki")){
			LClass.addPotionEffects(player);
		}else if(h.equals("Shazam")){
			SHClass.addPotionEffects(player);
		}else if(h.equals("Galactus")){
			new Galactus(this).addPotionEffects(player);
		}else if(h.equals("Groot")){
			new Groot(this).addPotionEffects(player);
		}else if(h.equals("Magneto")){
			new Magneto(this).addPotionEffects(player);
		}else if(h.equals("IceMan")){
			new IceMan(this).addPotionEffects(player);
		}else if(h.equals("Professor X")){
			new ProfessorX(this).addPotionEffects(player);
		}
		if(player.getFoodLevel() < 5){
			player.setFoodLevel(5);
		}
	}
	
	public boolean locIsInGame(Location l, Game g){
		try{
			if(g.getGameName.contains("#NOTGAME#"))
				return true;
			//World world = this.getServer().getWorld(saveFile.getString("gameInfo." + g.getGameName + ".world"));
			Location point1 = getLocation("gameInfo." + g.getGameName + ".gameLoc1", saveFile);
			Location point2 = getLocation("gameInfo." + g.getGameName + ".gameLoc2", saveFile);
			if (isInside(l, point1, point2))
				return true;
		}catch(Exception x){}
		return false;
	}
	
	/*
	  * Check if Location loc is within the cuboid with corners l1 and l2.
	  * l1 and l2 can be any two (opposite) corners of the cuboid.
	  */
	public boolean isInside(Location loc, Location l1, Location l2) {
	        int x = loc.getBlockX();
	        int y = loc.getBlockY();
	        int z = loc.getBlockZ();
	        int x1 = Math.min(l1.getBlockX(), l2.getBlockX());
	        int y1 = Math.min(l1.getBlockY(), l2.getBlockY());
	        int z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
	        int x2 = Math.max(l1.getBlockX(), l2.getBlockX());
	        int y2 = Math.max(l1.getBlockY(), l2.getBlockY());
	        int z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());
	 
	        return x >= x1 && x <= x2 && y >= y1 && y <= y2 && z >= z1 && z <= z2;
	}
	
	@SuppressWarnings("unchecked")
	public void addHeros(){
		heroList = (ArrayList<String>) getConfig().getList("enabledHeroes");
	}
	
	public void removeSign(Location signLoc){
		String signName = (signLoc.getX() + "." + signLoc.getY() + "." + signLoc.getZ());
		signName = signName.replace(".", "");
		if((saveFile.getConfigurationSection("signList").getKeys(false) != null) && (saveFile.getConfigurationSection("signList").getKeys(false).contains(signName))){
			//System.out.println("Removed Sign");//------------------------DEBUG
		    saveFile.set("signList." + signName, null);
		    saveSaveFile();
		}
	}
	
	public int getIndex(Player player){
		Game game = getGame(player);
		for(User user : game.getPlayerList){
			if(user.getPlayer().equals(player)){
				return game.getPlayerList.indexOf(user);
			}
		}
		return -1;
	}
	
	@SuppressWarnings("unchecked")
	public Game getGame(Player player){
		for(Game checkGame : (ArrayList<Game>)gameList.clone()){
			if(checkGame.getPlayerList.size() >= 1){
				for(User user : checkGame.getPlayerList){
					if(user.getPlayer().equals(player)){
						return checkGame;
					}
				}
			}
		}
		return null;
	}
	
	public Game getGame(String gn){
		for(Game checkGame : gameList)
			if(checkGame.getGameName.equals(gn))
				return checkGame;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String getHero(Player player){
		for(Game checkGame : (ArrayList<Game>)gameList.clone()){
			if(checkGame.getPlayerList.size() >= 1){
				for(User user : checkGame.getPlayerList){
					if(user.getPlayer().equals(player)){
						return user.getHero();
					}
				}
			}
		}
		return getConfig().getString("defaultHero");
	}
	
	public double getArmour(){
		double powerLevel = getConfig().getDouble("armourLevel");
		return powerLevel;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		if(getConfig().getBoolean("enableChat")){
			Player p = e.getPlayer();
			if(getGame(p) != null){
				Game g = getGame(p);
				String hero = "Groot";
				for(User u : g.getPlayerList)
					if(u.getPlayer().equals(p)){
						//Check If "Became" Hero
						if(g.getGameName.contains("#NOTGAME#"))
							return;
						//Get Hero
						hero = u.getHero();
						break;
					}
				String message = e.getMessage();
				e.setCancelled(true);
				//Get Message
				String fullMessage = getConfig().getString("chatFormat");
				fullMessage = fullMessage.replace("<player>", p.getDisplayName());
				fullMessage = fullMessage.replace("<message>", message);
				fullMessage = fullMessage.replace("<hero>", hero);
				//Fix Colour
				fullMessage = fullMessage.replace("&", "§");
				for(User u : g.getPlayerList){
					u.getPlayer().sendMessage(fullMessage);
				}
			}
		}
	}
	
	public void info(Game g, CommandSender s){
		//System.out.println("str " + langFile.getString("statusName"));
		//System.out.println("name " + g.getGameName);
		s.sendMessage(langFile.getString("statusName").replace("&", "§").replace("<game>", g.getGameName));
		if(g.getStarted){
			s.sendMessage(langFile.getString("statusStarted").replace("&", "§").replace("<game>", g.getGameName));
		}else{
			int max = 25;
			if(saveFile.getString("gameInfo." + g.getGameName + ".maxPlayers") != null)
				max = saveFile.getInt("gameInfo." + g.getGameName + ".maxPlayers");
			s.sendMessage(langFile.getString("statusLobby").replace("&", "§").replace("<game>", g.getGameName).replace("<players>", g.getPlayerList.size()+"").replace("<maxPlayer>", max+""));
		}
	}
	
	public boolean hasPack1(){
		if(Bukkit.getServer().getPluginManager().getPlugin("HeroPack1") != null)
			return true;
		return false;
	}
	
	public boolean hasDedicatedMode(){
		//if(Bukkit.getServer().getPluginManager().getPlugin("SWDedicatedMode") != null)
		//	return true;
		return getConfig().getBoolean("useDedicatedMode");
		//return false;
	}
	
	private void respawn(final Player p){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			public void run() {
				//if(getServer().getVersion().contains("1.8.3")){
					net.minecraft.server.v1_9_R1.PacketPlayInClientCommand in = new net.minecraft.server.v1_9_R1.PacketPlayInClientCommand(net.minecraft.server.v1_9_R1.PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
					org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer cp = (org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer) p;
					cp.getHandle().playerConnection.a(in);
				//}else if(getServer().getVersion().contains("1.8.6") || getServer().getVersion().contains("1.8.7")){
				//	net.minecraft.server.v1_8_R3.PacketPlayInClientCommand in = new net.minecraft.server.v1_8_R3.PacketPlayInClientCommand(net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN);
				//	org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer cp = (org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) p;
				//	cp.getHandle().playerConnection.a(in);
				//}else if(getServer().getVersion().contains("1.8")){
				//	net.minecraft.server.v1_8_R1.PacketPlayInClientCommand in = new net.minecraft.server.v1_8_R1.PacketPlayInClientCommand(net.minecraft.server.v1_8_R1.EnumClientCommand.PERFORM_RESPAWN);
				//	org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer cp = (org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer) p;
				//	cp.getHandle().playerConnection.a(in);
				//}
			}
		},(2));
	}
	
	public void displayParticles(String effect, Location l, double radius, int amount){
		displayParticles(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, 0, amount);
	}
	
	public void displayParticles(String effect, Location l, double radius, int speed, int amount){
		displayParticles(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, speed, amount);
	}
		  
	private void displayParticles(String effect, World w, double x, double y, double z, double radius, int speed, int amount){
		Location l = new Location(w, x, y, z);
		//System.out.println("V: " + getServer().getVersion());
		if(radius == 0){
			ParticleEffects_1_9.sendToLocation(ParticleEffects_1_9.valueOf(effect), l, 0, 0, 0, speed, amount);
		}else{
			ArrayList<Location> ll = getArea(l, radius, 0.2);
			for(int i = 0; i < amount; i++){
				int index = new Random().nextInt(ll.size());
				ParticleEffects_1_9.sendToLocation(ParticleEffects_1_9.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
				ll.remove(index);
			}
		}
	}
	  
	  private ArrayList<Location> getArea(Location l, double r, double t){
	    ArrayList<Location> ll = new ArrayList<Location>();
	    for (double x = l.getX() - r; x < l.getX() + r; x += t) {
	      for (double y = l.getY() - r; y < l.getY() + r; y += t) {
	        for (double z = l.getZ() - r; z < l.getZ() + r; z += t) {
	          ll.add(new Location(l.getWorld(), x, y, z));
	        }
	      }
	    }
	    return ll;
	  }
	
    private String getLocationName(Location l){
    	return (l.getX() + "." + l.getY() + "." + l.getZ() + l.getWorld().getName()).replace(".", "");
    }
    
    private void setLocation(String path, Location l, FileConfiguration fc){
    	fc.set(path + ".world", l.getWorld().getName());
    	fc.set(path + ".x", l.getX());
    	fc.set(path + ".y", l.getY());
    	fc.set(path + ".z", l.getZ());
    	saveConfig();
    	try{saveFile.save(saveYML);}catch(IOException e){}
    }
    
    private Location getLocation(String path, FileConfiguration fc){
		World world = getServer().getWorld(fc.getString(path + ".world"));
		double x = fc.getDouble(path + ".x");
		double y = fc.getDouble(path + ".y");
		double z = fc.getDouble(path + ".z");
		Location loc = new Location(world, x, y, z);
		return loc;
	}
	
//	public Location getPointsLocation(String path, World world){
//		double x = saveFile.getDouble(path + ".x");
//		double y = saveFile.getDouble(path + ".y");
//		double z = saveFile.getDouble(path + ".z");
//		Location loc = new Location(world, x, y, z);
//		return loc;
//	}
    
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	if ((cmd.getName().equals("superwars")) || (cmd.getName().equals("sw"))) {
    		try{
	    		Player player = null;
	    		//Ensure Sender Is A Player 
	    		if (!(sender instanceof Player)) {
	    			if ((!args[0].equals("reload")) && (!args[0].equals("status"))){
						if(langFile.getString("onlyPlayerCommand") != null)
							sender.sendMessage(langFile.getString("onlyPlayerCommand").replace("&", "§"));
	    				return true;
	    			}
	    		}else{
	    			//Check For Perms
					player = (Player)sender;
		    		if (args.length == 0) {
						throwError(sender);
						return true;
		    		}else if((args[0].equals("join")) || (args[0].equals("leave")) || (args[0].equals("stuff"))){
						if(!player.hasPermission("superwars.play.commands")){
							if(langFile.getString("noPlayerPerms") != null)
								sender.sendMessage(langFile.getString("noPlayerPerms").replace("&", "§"));
							return true;
						}
					}else {
						if(!player.hasPermission("superwars.op.commands")){
							if(langFile.getString("noOpPerms") != null)
								sender.sendMessage(langFile.getString("noOpPerms").replace("&", "§"));
							return true;
						}
					}
				}
	    		if ((args.length == 1) && (args[0].equals("reload"))) {
	    			//Reload Config
	    			reloadConfig();
	    			reloadLang();
	    			addHeros();
					if(langFile.getString("configReload") != null)
						sender.sendMessage(langFile.getString("configReload").replace("&", "§"));
	    		}else if ((args.length == 1) && (args[0].equals("help"))) {
	    			//Help
	    			throwError(sender);
	    		}else if ((args.length == 1) && (args[0].equals("start"))) {
	    			//Start Game
	    			Game checkGame = getGame(player);
	    			int setTime = saveFile.getInt("gameInfo." + checkGame.getGameName + ".lobbyTime");
	    			checkGame.setTime(setTime);
	    		}/*else if ((args.length == 2) && (args[0].equals("status"))) {
	    			//Game Info CMD: /sw status <game>
	    			try{
		    			//System.out.println("Satus " + gameList.toString());
		    			Game game = null;
		    			for(Game g : gameList)
		    				if(g.getGameName.equals(args[1]))
		    					game = g;
		    			if(game == null)
		    				game = new Game(args[1], 1, true);
	    				info(game, sender);
	    			}catch(Exception x){}
	    		}*/else if ((args.length == 2) && (args[0].equals("join"))) {
	    			//Join Game
	    			joinGame(player, args[1]);
	    		}else if ((args.length == 2) && (args[0].equals("become"))) {
		    		try{
		    			//Become Hero
		    			//CMD: /sw become <hero>
		    			if(getGame(player) == null){
		    				if(heroList.contains(args[1])){
				    			Damageable d = player;
				    			PreviousStuff ps = new PreviousStuff(player.getLocation(), player.getInventory().getContents(), player.getInventory().getArmorContents(), d.getHealth(), player.getFoodLevel(), player.getExp(), player.getLevel(), player.getGameMode());
				    			//playerList.add(new User(player, ps, args[1], 1));
				    			Game gm = new Game(player.getName()+"#NOTGAME#", 1, false);
				    			gm.addPlayer(new User(player, ps, args[1], 1));
				    			gm.setStarted(true);
				    			gameList.add(gm);
				    			giveItems(player, args[1], false, 1);
		    					if(langFile.getString("becomeHero") != null)
		    						sender.sendMessage(langFile.getString("becomeHero").replace("&", "§").replace("<hero>", args[1]));
		    				}else
		    					if(langFile.getString("heroNotFound") != null)
		    						sender.sendMessage(langFile.getString("heroNotFound").replace("&", "§").replace("<hero>", args[1]));
		    			}else
	    					if(langFile.getString("alreadyInGame") != null)
	    						sender.sendMessage(langFile.getString("alreadyInGame").replace("&", "§").replace("<game>", getGame(player).getGameName));
		    		}catch(Exception e){e.printStackTrace();}
	    		}else if ((args.length == 3) && (args[0].equals("make"))) {
	    			//Make Player(args[1]) Become Hero(args[2])
	    			Player p = Bukkit.getPlayer(args[1]);
	    			String hero = args[2];
	    			if(heroList.contains(hero)){
						if(player.hasPermission("superwars." + hero + ".hero")){
							if(getGame(p) != null){
								//Language
								if(langFile.getString("switchHero") != null)
		    						p.sendMessage(langFile.getString("switchHero").replace("&", "§").replace("<hero>", hero));
								if(langFile.getString("otherChangeHero") != null)
		    						p.sendMessage(langFile.getString("otherChangeHero").replace("&", "§").replace("<hero>", hero).replace("<player>", p.getDisplayName()));
			    				Game gm = getGame(p);
			    				User usr = gm.getPlayerList.get(getIndex(p));
								//User newUsr = new User(usr.getPlayer, usr.getPreviousStuff, hero, usr.getLives, usr.getCooling, usr.getJumping, usr.getSneaking, usr.getString);
								//gm.getPlayerList.set(getIndex(p), newUsr);
			    				usr.setHero(hero);
							}
						}else{
							if(langFile.getString("noMakeHero") != null)
	    						p.sendMessage(langFile.getString("noMakeHero").replace("&", "§").replace("<hero>", hero));
						}
					}else{
						if(langFile.getString("heroNotFound") != null)
    						p.sendMessage(langFile.getString("heroNotFound").replace("&", "§").replace("<hero>", hero));
					}
	    		}else if ((args.length == 3) && (args[0].equals("stuff"))) {
	    			//Give Player(args[1]) The Hero(args[2]) Items
	    			giveItems(Bukkit.getPlayer(args[1]), args[2], false, 1);
	    		}else if ((args.length == 1) && (args[0].equals("stuff"))) {
	    			//Give In-Game Player Their Stuff Again
					if(getGame(player) != null){
	    				Game gm = getGame(player);
	    				User usr = gm.getPlayerList.get(getIndex(player));
	    				if(!usr.getHero().equals("BatMan")){
	    					giveItems(player, usr.getHero(), true, 1);
	    				}
					}
	    		}else if ((args.length == 2) && (args[0].equals("hero"))) {
	    			if(heroList.contains(args[1])){
						if(player.hasPermission("superwars." + args[1] + ".hero")){
							if(getGame(player) != null){
								if(langFile.getString("switchHero") != null)
		    						player.sendMessage(langFile.getString("switchHero").replace("&", "§").replace("<hero>", args[1]));
			    				Game gm = getGame(player);
			    				User usr = gm.getPlayerList.get(getIndex(player));
								//User newUsr = new User(usr.getPlayer, usr.getPreviousStuff, args[1], usr.getLives, usr.getCooling, usr.getJumping, usr.getSneaking, usr.getString);
								//gm.getPlayerList.set(getIndex(usr.getPlayer), newUsr);
			    				usr.setHero(args[1]);
							}
						}else{
							if(langFile.getString("noHeroPerms") != null)
	    						player.sendMessage(langFile.getString("noHeroPerms").replace("&", "§").replace("<hero>", args[1]));
						}
					}else{
						if(langFile.getString("heroNotFound") != null)
    						player.sendMessage(langFile.getString("heroNotFound").replace("&", "§").replace("<hero>", args[1]));
					}
	    		}else if (args[0].equals("leave")) {
	    			//System.out.println("l1");
	    			leaveGame(player, true);
	    		}else if (args[0].equals("info")) {
	    			throwError(sender);
	    		}else if ((args.length == 2) && (args[0].equals("delete"))) {
	    			try{
						if(saveFile.getString("gameInfo." + args[1]) != null){
							sender.sendMessage("§2SuperWars: The game " + args[1] + " has been sucsessufully removed!");
							//Remove Game from Config
							//Game Info
							saveFile.set("gameInfo." + args[1], null);
				    		saveSaveFile();
							return true;
						}
		    			sender.sendMessage("§2SuperWars: §cThe game " + args[1] + " §cdoes not exist!");
	    			}catch(Exception e){}
	    		}else if ((args.length == 2) && (args[0].equals("create"))) {
    				if(gameMakerMap.containsKey(player.getName())){
    					sender.sendMessage("§2SuperWars: §cYou are already creating a game!");
    					return true;
    				}
    				if(saveFile.getString("gameInfo." + args[1]) != null){
    					sender.sendMessage("§2SuperWars: §cThe game " + args[1] + " §calready exists!");
    					return true;
    				}
	    			GameMaker gm = new GameMaker(args[1], player.getLocation());
	    			gameMakerMap.put(player.getName(), gm);
	    			sender.sendMessage("§2SuperWars: Creating " + args[1] + "!");
	    			sender.sendMessage("§2SuperWars: §6Commands:");
	    			showCreationCMDs(sender);
	    		}else if ((args.length >= 2) && (args[0].equals("set"))) {
	    			//sw set <gametime/lobbytime> <time>
	    			//sw set <point1/point2/lobby>
	    			try{
		    			int time = 0;
		    			if(args.length == 3)
		    				time = Integer.parseInt(args[2]);
		    			GameMaker gm = gameMakerMap.get(player.getName());
		    			if (args[1].equals("gametime")){
		    				gm.setGameTime(time);
		        			sender.sendMessage("§2SuperWars: Game time set to " + time + "!");
		    			}else if (args[1].equals("lobbytime")){
		    				gm.setLobbyTime(time);
		        			sender.sendMessage("§2SuperWars: Lobby time set to " + time + "!");
		    			}else if (args[1].equals("lives")){
		    				gm.setStartLives(time);
		        			sender.sendMessage("§2SuperWars: Start lives set to " + time + "!");
		    			}else if (args[1].equals("point1")){
		    				gm.setPoint1(player.getLocation());
			        		sender.sendMessage("§2SuperWars: Point 1 set!");
		    			}else if (args[1].equals("point2")){
		    				gm.setPoint2(player.getLocation());
			        		sender.sendMessage("§2SuperWars: Point 2 set!");
		    			}else if (args[1].equals("lobby")){
		    				gm.setLobby(player.getLocation());
			        		sender.sendMessage("§2SuperWars: Lobby set!");
		    			}
	    			}catch(Exception e){sender.sendMessage("§2SuperWars: §cWrong command!");throwError(sender);}
	    		}else if ((args.length == 2) && (args[0].equals("add"))) {
	    			//sw add spawnpoint
    				GameMaker gm = gameMakerMap.get(player.getName());
	    			if(args[1].equals("spawnpoint")){
	    				gm.addSpawnLoc(player.getLocation());
		        		sender.sendMessage("§2SuperWars: Spawn point added!");
	    			}
	    		}else if (args[0].equals("finish")) {
	    			//sw finish
	    			GameMaker gam = gameMakerMap.get(player.getName());
					//Make Sure All Fields Full
					if(gam.getSpawnLocksList.size() < 1){
						sender.sendMessage("§2SuperWars: §cYou haven't added any spawn points!");
						return true;
					}else if((gam.getPoint1 == null) || (gam.getPoint2 == null)){
						sender.sendMessage("§2SuperWars: §cYou haven't set the game points!");
						return true;
					}
					//Add Game to Config
					saveFile.set("gameInfo." + gam.getGameName + ".world", gam.getWorld.getName());
					//Lobby
					setLocation("gameInfo." + gam.getGameName + ".lobbyLock", gam.getLobbyLock, saveFile);
					//saveFile.set("gameInfo." + gam.getGameName + ".world", gam.getWorld.getName());
					//saveFile.set("gameInfo." + gam.getGameName + ".lobbyLock.x", gam.getLobbyLock.getX());
					//saveFile.set("gameInfo." + gam.getGameName + ".lobbyLock.y", gam.getLobbyLock.getY());
					//saveFile.set("gameInfo." + gam.getGameName + ".lobbyLock.z", gam.getLobbyLock.getZ());
					//Point1
					setLocation("gameInfo." + gam.getGameName + ".gameLoc1", gam.getPoint1, saveFile);
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc1.x", gam.getPoint1.getX());
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc1.y", gam.getPoint1.getY());
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc1.z", gam.getPoint1.getZ());
					//Point2
					setLocation("gameInfo." + gam.getGameName + ".gameLoc2", gam.getPoint2, saveFile);
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc2.x", gam.getPoint2.getX());
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc2.y", gam.getPoint2.getY());
					//saveFile.set("gameInfo." + gam.getGameName + ".gameLoc2.z", gam.getPoint2.getZ());
					//Spawn Locs
					for(Location loc : gam.getSpawnLocksList){
						//String name = (loc.getX() + "." + loc.getY() + "." + loc.getZ());
					    //name = name.replace(".", "");
						//saveFile.set("gameInfo." + gam.getGameName + ".spawnLocks." + name + ".x", loc.getX());
						//saveFile.set("gameInfo." + gam.getGameName + ".spawnLocks." + name + ".y", loc.getY());
						//saveFile.set("gameInfo." + gam.getGameName + ".spawnLocks." + name + ".z", loc.getZ());
						setLocation("gameInfo." + gam.getGameName + ".spawnLocks." + getLocationName(loc), loc, saveFile);
					}

					//Game/Lobby Time
					saveFile.set("gameInfo." + gam.getGameName + ".gameTime", gam.getGameTime);
					saveFile.set("gameInfo." + gam.getGameName + ".lobbyTime", gam.getLobbyTime);
					//Game Lives
					saveFile.set("gameInfo." + gam.getGameName + ".startLives", gam.getStartLives);
		    		saveSaveFile();
					sender.sendMessage("§2SuperWars: The game " + gam.getGameName + " §2has been saved!");
					gameMakerMap.remove(player.getName());
					return true;
	    		}else{
	    			sender.sendMessage("§cWrong arguments!");
	    			throwError(sender);
	    		}
    		}catch(Exception x){
    			sender.sendMessage("§cWrong arguments!");
    			throwError(sender);
    		}
    	}
    	return true; 
	}
	
    @SuppressWarnings("deprecation")
	public void defineTransBlocks() {
    	transBlocks.add(Integer.valueOf(Material.AIR.getId()));
    	transBlocks.add(Integer.valueOf(Material.RAILS.getId()));
    	transBlocks.add(Integer.valueOf(Material.ANVIL.getId()));
    	transBlocks.add(Integer.valueOf(Material.BREWING_STAND.getId()));
    	transBlocks.add(Integer.valueOf(Material.DAYLIGHT_DETECTOR.getId()));
    	transBlocks.add(Integer.valueOf(Material.DETECTOR_RAIL.getId()));
    	transBlocks.add(Integer.valueOf(Material.DEAD_BUSH.getId()));
    	transBlocks.add(Integer.valueOf(Material.RED_MUSHROOM.getId()));
    	transBlocks.add(Integer.valueOf(Material.LADDER.getId()));
    	transBlocks.add(Integer.valueOf(Material.RED_ROSE.getId()));
    	transBlocks.add(Integer.valueOf(Material.YELLOW_FLOWER.getId()));
    	transBlocks.add(Integer.valueOf(Material.BROWN_MUSHROOM.getId()));
    	transBlocks.add(Integer.valueOf(6));
    	transBlocks.add(Integer.valueOf(31));
    	transBlocks.add(Integer.valueOf(75));
    	transBlocks.add(Integer.valueOf(76));
    	transBlocks.add(Integer.valueOf(104));
    	transBlocks.add(Integer.valueOf(105));
    	transBlocks.add(Integer.valueOf(111));
    	transBlocks.add(Integer.valueOf(127));
    	transBlocks.add(Integer.valueOf(132));
    	transBlocks.add(Integer.valueOf(140));
    	transBlocks.add(Integer.valueOf(141));
    	transBlocks.add(Integer.valueOf(142));
    	transBlocks.add(Integer.valueOf(149));
    	transBlocks.add(Integer.valueOf(150));
    	transBlocks.add(Integer.valueOf(171));
    	transBlocks.add(Integer.valueOf(Material.PORTAL.getId()));
    	transBlocks.add(Integer.valueOf(Material.POWERED_RAIL.getId()));
    	transBlocks.add(Integer.valueOf(Material.WEB.getId()));
    	transBlocks.add(Integer.valueOf(Material.TORCH.getId()));
    	transBlocks.add(Integer.valueOf(Material.SIGN.getId()));
    	transBlocks.add(Integer.valueOf(Material.STONE_BUTTON.getId()));
    	transBlocks.add(Integer.valueOf(70));
    	transBlocks.add(Integer.valueOf(72));
    	transBlocks.add(Integer.valueOf(Material.WOOD_BUTTON.getId()));
    	transBlocks.add(Integer.valueOf(Material.SUGAR_CANE_BLOCK.getId()));
    	transBlocks.add(Integer.valueOf(Material.GOLD_PLATE.getId()));
    	transBlocks.add(Integer.valueOf(Material.IRON_PLATE.getId()));
    	transBlocks.add(Integer.valueOf(Material.HOPPER.getId()));
    	transBlocks.add(Integer.valueOf(Material.LADDER.getId()));
    	transBlocks.add(Integer.valueOf(Material.VINE.getId()));
    	transBlocks.add(Integer.valueOf(Material.BED.getId()));
    	transBlocks.add(Integer.valueOf(Material.BED_BLOCK.getId()));
    	transBlocks.add(Integer.valueOf(Material.SNOW.getId()));
    	transBlocks.add(Integer.valueOf(Material.RAILS.getId()));
    	transBlocks.add(Integer.valueOf(Material.LEVER.getId()));
    	transBlocks.add(Integer.valueOf(Material.TRAP_DOOR.getId()));
    	transBlocks.add(Integer.valueOf(Material.PISTON_EXTENSION.getId()));
    	transBlocks.add(Integer.valueOf(Material.PISTON_MOVING_PIECE.getId()));
    	transBlocks.add(Integer.valueOf(Material.TRIPWIRE_HOOK.getId()));
    	transBlocks.add(Integer.valueOf(93));
    	transBlocks.add(Integer.valueOf(94));
    	transBlocks.add(Integer.valueOf(Material.BOAT.getId()));
    	transBlocks.add(Integer.valueOf(Material.MINECART.getId()));
    	transBlocks.add(Integer.valueOf(Material.CAKE.getId()));
    	transBlocks.add(Integer.valueOf(Material.CAKE_BLOCK.getId()));
    	transBlocks.add(Integer.valueOf(Material.WATER.getId()));
    	transBlocks.add(Integer.valueOf(Material.STATIONARY_WATER.getId()));
    	transBlocks.add(Integer.valueOf(Material.LAVA.getId()));
    	transBlocks.add(Integer.valueOf(Material.STATIONARY_LAVA.getId()));
    	transBlocks.add(Integer.valueOf(175));
    	transBlocks.add(Integer.valueOf(64));
    	transBlocks.add(Integer.valueOf(71));
      }
    
    public void throwError(CommandSender sender) {
    	sender.sendMessage("§2--- SuperWars v" + Bukkit.getServer().getPluginManager().getPlugin("SuperWars").getDescription().getVersion() +" ---");
		sender.sendMessage("/sw reload");
		sender.sendMessage("/sw join <game>");
		sender.sendMessage("/sw leave");
		sender.sendMessage("/sw create <game>");
		sender.sendMessage("/sw delete <game>");
		sender.sendMessage("/sw become <hero>");
		sender.sendMessage("/sw make <player> <hero>");
		sender.sendMessage("/sw stuff <player> <hero>");
		sender.sendMessage("/sw stuff");
		sender.sendMessage("/sw hero <hero>");
		showCreationCMDs(sender);
    }
    
    private void showCreationCMDs(CommandSender sender){
		sender.sendMessage("/sw set <gametime/lobbytime> <time>");
		sender.sendMessage("/sw set <point1/point2/lobby>");
		sender.sendMessage("/sw add spawnpoint");
		sender.sendMessage("/sw finish");
    }
    
	@SuppressWarnings("deprecation")
	public ItemStack getItem(String setItemString, String name, int amount, List<String> loreList){
		//Get Item
		ItemStack item = null;
		if(setItemString.contains(":")){
			String[] split = setItemString.split(":");
	    	int setItem = Integer.parseInt(split[0]);
	    	int subtype = Integer.parseInt(split[1]);
	    	item = (new ItemStack(setItem, amount, (short) subtype));
		}else{
			int setItem = Integer.parseInt(setItemString);
			item = new ItemStack(setItem, amount);
		}
		ItemMeta m = item.getItemMeta();
		m.setDisplayName(name);
		m.setLore(loreList);
		item.setItemMeta(m);
		return item;
	}
	
	public class Game {

		public ArrayList<User> getPlayerList;
	    public String getGameName;
	    public int getTimeElapsed;
	    public int getStartLives;
	    public boolean getStarted;
	    public HashMap<Location, ItemStack> getMapBackUp = null;
	    public ArrayList<Player> getWantToLeaveList = new ArrayList<Player>();
	 
	    Game (String name, int startLives, boolean useMap) {
	    	getPlayerList = new ArrayList<User>();
	    	getGameName = name;
	    	getTimeElapsed = 0;
	    	getStartLives = startLives;
	    	getStarted = false;
	    	if(useMap)
	    		saveMapBlocks(true);
		}
	    
	    private void saveMapBlocks(boolean save){
	    	if(getMapBackUp != null || save == true){
				//World world = getServer().getWorld(saveFile.getString("gameInfo." + getGameName + ".world"));
				Location point1 = getLocation("gameInfo." + getGameName + ".gameLoc1", saveFile);
				Location point2 = getLocation("gameInfo." + getGameName + ".gameLoc2", saveFile);
				new BlockRunner(point1, point2, this, Bukkit.getServer().getPluginManager().getPlugin("SuperWars"), save);
	    	}
	    }
	    
	    public void setStarted(boolean b){
	    	getStarted = b;
	    }
	    
	    public void setTime(int i){
	    	getTimeElapsed = i;
	    }
	    
	    public void addPlayer(User u){
	    	getPlayerList.add(u);
	    }
	    
	    public void setWantsToLeave(Player p){
	    	getWantToLeaveList.add(p);
	    }
	    
	    public void setMapBackUp(HashMap<Location, ItemStack> m){
	    	getMapBackUp = m;
	    	for(Player p : getWantToLeaveList)
	    		leaveGame(p, true);
	    	getWantToLeaveList.clear();
	    }
	}

	class GameMaker {

	    public String getGameName;
	    public World getWorld;
		public Location getLobbyLock;
		public ArrayList<Location> getSpawnLocksList = new ArrayList<Location>();
	    public int getGameTime;
	    public int getLobbyTime;
	    public int getStartLives;
		public Location getPoint1;
		public Location getPoint2;
	 
	    GameMaker (String name, Location loobyLoc) {
	    	getGameName = name;
	    	getLobbyLock = loobyLoc;
	    	getWorld = loobyLoc.getWorld();
//	    	getSpawnLocksList = spawnLocksList;
	    	getGameTime = getConfig().getInt("defaultGameTime");
	    	getLobbyTime = getConfig().getInt("defaultLobbyTime");
	    	getStartLives = getConfig().getInt("defaultLives");
//	    	getPoint1 = point1;
//	    	getPoint2 = point2;
		}
	    
	    public void addSpawnLoc(Location l){
	    	getSpawnLocksList.add(l);
	    	//System.out.println(getSpawnLocksList.size() + " SLL: " + getSpawnLocksList.toString());
	    }
	    
	    public void setLobby(Location l){
	    	getLobbyLock = l;
	    }
	    
	    public void setPoint1(Location l){
	    	getPoint1 = l;
	    }
	    
	    public void setPoint2(Location l){
	    	getPoint2 = l;
	    }
	    
	    public void setGameTime(int i){
	    	getGameTime = i;
	    }
	    
	    public void setLobbyTime(int i){
	    	getLobbyTime = i;
	    }
	    
	    public void setStartLives(int i){
	    	getStartLives = i;
	    }
	}
	
	public class BlockRunner extends BukkitRunnable {
   	 
        private int minX;
        private int minY;
        private int minZ;
        private int maxX, maxY, maxZ;
        private int x, y, z;
        Location loc1, loc2;
        
        Game game;
        public HashMap<Location, ItemStack> blocks = new HashMap<Location, ItemStack>();;
        boolean saving;
        String name;
        
        public BlockRunner(Location loc1, Location loc2, Game g, Plugin plugin, boolean s) {
 		    game = g;
 		    saving = s;
 		    if(!saving){
 		    	restoringMaps.add(game.getGameName);
 		    	name = game.getGameName;
 		    	blocks = game.getMapBackUp;
 		    }
        	//System.out.println("Constructer");
            this.loc1 = loc1;
            this.loc2 = loc2;
 		    minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
 		    minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
 		    minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
 		    
 		    x = minX;
 		    y = minY;
 		    z = minZ;
 		    
 		    maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
 		    maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
 		    maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
            //run();
            runTaskTimer(plugin, 1L, 1L);
        }
        
        @SuppressWarnings("deprecation")
		@Override
        public void run () {
        	long start = System.currentTimeMillis();
        	int count = 0;
        	outer:
                while (x <= maxX) {
                    while (y <= maxY) {
                        while (z <= maxZ) {
	                    	  Block b = loc1.getWorld().getBlockAt(x, y, z);
	                    	  if(saving){
		                          //Save Block
		                          if(!b.getType().equals(Material.AIR)){
		                        	  //System.out.println("1: " + b);
		                        	  //System.out.println("2: " + b.getLocation());
		                        	  //System.out.println("3: " + b.getType());
		                        	  //System.out.println("4: " + b.getData());
		                        	  ItemStack i = new ItemStack(b.getType(), 1, b.getData());
		                        	  //System.out.println("5: " + i);
		                        	  //System.out.println("6: " + blocks);
		                        	  blocks.put(b.getLocation(), i);
		                          }
	                    	  }else{
	                  		    ItemStack iStack = null;
	                		    if(blocks.get(b.getLocation()) != null)
	                		    	iStack = blocks.get(b.getLocation());
	                		    if(iStack != null){
	                		    	if((b.getTypeId() != (iStack.getTypeId())) || (b.getData() != (iStack.getDurability()))){
	                		    		b.setTypeIdAndData(iStack.getTypeId(), (byte) iStack.getDurability(), false);
	                		    	}
	                		    }else if(!b.getType().equals(Material.AIR)){
	                		    	b.setType(Material.AIR);
	                		    }
	                    	  }
	                    	  count++;
	                    	  if (count >= 100) {
	                    		  if (System.currentTimeMillis() - start > 10) {
	                    			  //System.out.println("break");
	                    			  break outer;
	                    		  }else{
	                    			  count = 0;
	                    		  }
	                    	  }
	                    	  z++;
                        }
                        z = minZ;
                        y++;
                    }
                    y = minY;
                    x++;
                }
        	if(x >= maxX){
//	  	      	for(Player p : getServer().getOnlinePlayers())
//	  	      		if(p.isOp())
//	  	      			p.sendMessage("§eLighting area done!");
        		if(saving){
        			game.setMapBackUp(blocks);
        		}else
        			restoringMaps.remove(name);
	        	cancel();
	  	      	return;
        	}
        }
   }
}

class PreviousStuff {

    public Location getLocation;
    public ItemStack[] getInventory;
    public ItemStack[] getArmour;
    public double getHealth;
    public int getFood;
    public float getXP;
    public int getLevel;
    public GameMode getGameMode;
 
    PreviousStuff (Location previousLocation, ItemStack[] previousInventory, ItemStack[] previousArmour, double previousHealth, int previousFood, float previousXP, int previousLevel, GameMode previousGM) {
		getLocation = previousLocation;
		getInventory = previousInventory;
		getArmour = previousArmour;
		getHealth = previousHealth;
		getFood = previousFood;
		getXP = previousXP;
		getGameMode = previousGM;
		getLevel = previousLevel;
	}
}
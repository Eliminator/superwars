package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Shazam
  extends Hero
{
  public Shazam(SuperWars instance)
  {
    super(instance);
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onManMove(PlayerMoveEvent event)
  {
    Player player = event.getPlayer();
    if (plugin.getGame(player) != null)
    {
      SuperWars.Game game = plugin.getGame(player);
      User user = (User)game.getPlayerList.get(plugin.getIndex(player));
      if ((user.getHero().equals("Shazam")) && (game.getStarted) && 
        (user.isJumping())) {
        player.setVelocity(player.getLocation().getDirection().multiply(1));
      }
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.NORMAL)
  public void onManInteract(PlayerInteractEvent event)
  {
    if (event.getAction().equals(Action.PHYSICAL)) {
      return;
    }
    final Player player = event.getPlayer();
    if (plugin.getGame(player) != null)
    {
      SuperWars.Game game = plugin.getGame(player);
      User user = (User)game.getPlayerList.get(plugin.getIndex(player));
      if ((user.getHero().equals("Shazam")) && (game.getStarted)) {
        try
        {
          ItemStack it = player.getItemInHand();
          if ((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying")))
          {
            ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
            ItemMeta meta = flyTog.getItemMeta();
            meta.setDisplayName("§4Use to start flying");
            flyTog.setItemMeta(meta);
            player.setItemInHand(flyTog);
            user.setJumping(false);
            player.setAllowFlight(false);
          }
          else
          {
            ItemMeta meta;
            if ((it.getType().equals(Material.ANVIL)) && (it.getItemMeta().getDisplayName().equals("§4Use to start flying")))
            {
              event.setCancelled(true);
              ItemStack flyTog = new ItemStack(Material.FEATHER, 1);
              meta = flyTog.getItemMeta();
              meta.setDisplayName("§4Use to stop flying");
              flyTog.setItemMeta(meta);
              player.setItemInHand(flyTog);
              user.setJumping(true);
              player.setAllowFlight(true);
            }
            else if ((it.getType().equals(Material.NETHER_STAR)) && (it.getItemMeta().getDisplayName().equals("§fLightning Control")))
            {
              event.setCancelled(true);
              if ((!user.isCooling()) && ((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR))))
              {
                player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 160, 10), true);
                player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 200, 10), true);
                player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 240, 10), true);
                
                player.getWorld().strikeLightning(player.getLocation());
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 1.0F);
                
                plugin.displayParticles("BIG_EXPLODE", player.getLocation(), 1.0D, 1, 3);
                plugin.displayParticles("SMOKE", player.getLocation(), 0.0D, 1, 40);
                for (Entity ent : player.getNearbyEntities(5.0D, 5.0D, 5.0D)) {
                  if ((ent instanceof LivingEntity))
                  {
                    LivingEntity le = (LivingEntity)ent;
                    le.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100, 10), true);
                  }
                }
                user.setCooling(true);
                startCoolTimer(player, "SHAZAM", 25, 0);
              }
              else if ((!user.isCooling("lightning")) && ((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))))
              {
                Location l = player.getTargetBlock((HashSet<Byte>)null, 50).getLocation();
                l.getWorld().strikeLightning(l);
                
                quickCool(player, "lightning", 8);
              }
            }
            else if ((it.getType().equals(Material.SUGAR)) && (it.getItemMeta().getDisplayName().equals("§4Super Speed")))
            {
              event.setCancelled(true);
              if ((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR)))
              {
                Location l = player.getTargetBlock((HashSet<Byte>)null, 50).getLocation();
                if (!l.getBlock().getType().equals(Material.AIR))
                {
                  l.setY(l.getY() + 2.0D);
                  plugin.displayParticles("SMOKE", player.getLocation(), 0.7D, 0, 12);
                  plugin.displayParticles("SMOKE", player.getEyeLocation(), 0.7D, 0, 12);
                  player.teleport(l);
                }
                player.setItemInHand(null);
                
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
                {
                  public void run()
                  {
                    try
                    {
                      if (Shazam.plugin.getGame(player) != null) {
                        player.getInventory().addItem(new ItemStack[] { Shazam.this.getSpeed() });
                      }
                    }
                    catch (Exception localException) {}
                  }
                }, 200L);
              }
              else if ((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR)))
              {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 10), true);
                player.setItemInHand(null);
                
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
                {
                  public void run()
                  {
                    try
                    {
                      if (Shazam.plugin.getGame(player) != null) {
                        player.getInventory().addItem(new ItemStack[] { Shazam.this.getSpeed() });
                      }
                    }
                    catch (Exception localException) {}
                  }
                }, 200L);
              }
            }
          }
        }
        catch (Exception localException) {}
      }
    }
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onEntityAttack(EntityDamageByEntityEvent event)
  {
    try
    {
      Entity eAttacker = event.getDamager();
      Entity eVictim = event.getEntity();
      if ((eAttacker instanceof Player))
      {
        Player attacker = (Player)eAttacker;
        if (plugin.getGame(attacker) != null)
        {
          SuperWars.Game game = plugin.getGame(attacker);
          String aHero = plugin.getHero(attacker);
          if ((aHero.equals("Shazam")) && (game.getStarted) && 
            (attacker.getItemInHand().getType().equals(Material.AIR)))
          {
            event.setDamage(2.0D);
            eVictim.setVelocity(attacker.getLocation().getDirection().multiply(0.9D));
          }
        }
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onEntityDamage(EntityDamageEvent event)
  {
    if ((event.getEntity() instanceof Player)) {
      try
      {
        Player player = (Player)event.getEntity();
        if (plugin.getGame(player) != null)
        {
          SuperWars.Game game = plugin.getGame(player);
          User user = null;
          for (User u : game.getPlayerList) {
            if (u.getPlayer().equals(player)) {
              user = u;
            }
          }
          if (user.getHero().equals("Shazam"))
          {
            if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
              event.setDamage(event.getDamage() / 2.0D);
            }
            if ((event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) || (event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION)) {
              event.setDamage(event.getDamage() / 2.0D);
            }
            if (event.getCause() == EntityDamageEvent.DamageCause.LIGHTNING) {
              event.setDamage(0.0D);
            }
          }
        }
      }
      catch (Exception localException) {}
    }
  }
  
  public ItemStack getLightning()
  {
    ItemStack star = new ItemStack(Material.NETHER_STAR);
    ItemMeta meta = star.getItemMeta();
    meta.setDisplayName("§fLightning Control");
    ArrayList<String> loreList = new ArrayList<String>();
    loreList.add("§4Right-click to strike lightning");
    loreList.add("§4Left-click SHAZAM");
    meta.setLore(loreList);
    star.setItemMeta(meta);
    return star;
  }
  
  public ItemStack getSpeed()
  {
    ItemStack pulsars = new ItemStack(Material.SUGAR);
    ItemMeta meta = pulsars.getItemMeta();
    meta.setDisplayName("§4Super Speed");
    ArrayList<String> loreList = new ArrayList<String>();
    loreList.add("§4Right-click to activate speed");
    loreList.add("§4Left-click to teleport");
    meta.setLore(loreList);
    pulsars.setItemMeta(meta);
    return pulsars;
  }
  
  public ItemStack getFlight()
  {
    ItemStack fly = new ItemStack(Material.ANVIL);
    ItemMeta meta = fly.getItemMeta();
    meta.setDisplayName("§4Use to start flying");
    fly.setItemMeta(meta);
    return fly;
  }
  
  public void addPotionEffects(Player player)
  {
    try
    {
      SuperWars.Game game = plugin.getGame(player);
      if (game.getStarted)
      {
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(3.0D * plugin.getArmour())), true);
        
        Damageable d = player;
        if (d.getHealth() < 20.0D) {
          try
          {
            player.setHealth(d.getHealth() + 1.0D);
          }
          catch (Exception localException) {}
        }
        Location l = player.getLocation();
        l.setY(l.getY() - 1.0D);
        if (!l.getBlock().getType().equals(Material.AIR))
        {
          User user = (User)game.getPlayerList.get(plugin.getIndex(player));
          user.setJumping(false);
          player.setAllowFlight(false);
          for (ItemStack it : player.getInventory()) {
            if ((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying")))
            {
              ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
              ItemMeta meta = flyTog.getItemMeta();
              meta.setDisplayName("§4Use to start flying");
              flyTog.setItemMeta(meta);
              player.getInventory().remove(it);
              player.getInventory().addItem(new ItemStack[] { flyTog });
            }
          }
        }
      }
    }
    catch (Exception localException1) {}
  }
}
package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.Random;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class IronMan extends Hero{
	
	public IronMan(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("IronMan")) && (game.getStarted == true)){
				if(user.isJumping() == true){
					player.setVelocity(player.getLocation().getDirection().multiply(1));
					//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, player.getLocation(), 0, 0, 0, 0, 2);
					plugin.displayParticles("CLOUD", player.getLocation(), 0, 2);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("IronMan")) && (game.getStarted == true)){
				try{
					ItemStack it = player.getItemInHand();
		    		if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying"))) {
	    				ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Use to start flying");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(false);
	    				player.setAllowFlight(false);
		    		}else if((it.getType().equals(Material.ANVIL)) && (it.getItemMeta().getDisplayName().equals("§4Use to start flying"))) {
		    			event.setCancelled(true);
		    			ItemStack flyTog = new ItemStack(Material.FEATHER, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Use to stop flying");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(true);
	    				player.setAllowFlight(true);
		    		}else if((it.getType().equals(Material.BLAZE_ROD)) && (it.getItemMeta().getDisplayName().equals("§4Unibeam")) && (user.isCooling() == false)) {
		    			//Set Cooling
		    			user.setCooling(true);
		    			//Shoot Beam
		    			Location eyeLoc = player.getEyeLocation();
		    			double px = eyeLoc.getX();
		    			double py = eyeLoc.getY();
		    			double pz = eyeLoc.getZ();
		    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
		    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
		    			double x = Math.sin(pitch) * Math.cos(yaw);
		    			double y = Math.sin(pitch) * Math.sin(yaw);
		    			double z = Math.cos(pitch);
		    			for (int j = 1 ; j <= 10 ; j++) {
							if (getTarget(player) != null) {
								Entity attacked = getTarget(player);
								if ((attacked instanceof LivingEntity)) {
									((LivingEntity)attacked).damage((int)Math.round(2.0D * 10));
								}
							}
		    			}
		    			for (int i = 1 ; i <= 50 ; i++) {
			    			Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
			    			//player.playEffect(loc, Effect.STEP_SOUND, 57);
			    			//ParticleEffects.sendToLocation(ParticleEffects.ENCHANT_CRITS, loc, 0, 0, 0, 0, 10);
			    			plugin.displayParticles("ENCHANT_CRITS", loc, 0, 10);
			    			if(loc.getBlock().getType() != Material.AIR)
			    				i = 50;
		    			}
		    			//Create Boom
			    		Location boom = player.getTargetBlock((Set<Material>)null, 200).getLocation();
			    		boom.getWorld().createExplosion(boom, 3);
			    		startCoolTimer(player, "Unibeam", 20, 0);
		    		}else if((it.getType().equals(Material.DIAMOND)) && (it.getItemMeta().getDisplayName().equals("§4Hand Pulsars"))) {
						if (!user.isCooling("pulser")) {
							Location loc = player.getEyeLocation();
							//Set Cool
							quickCool(player, "pulser", 3);
							//Make Gun Sound
							player.playSound(loc, Sound.ENTITY_BAT_TAKEOFF, 2F, 1F);
							player.playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 2F, 50F);
							player.playSound(loc, Sound.ENTITY_ITEM_BREAK, 1F, 0F);
							//Fire Gun Bullet
							Snowball ball = player.getWorld().spawn(loc, Snowball.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(8));
						}
		    		}else if((it.getType().equals(Material.ARROW)) && (it.getItemMeta().getDisplayName().equals("§4Missile"))) {
						//Remove Arrow
						int amount = player.getItemInHand().getAmount() - 1;
						if(amount <= 0){
							player.setItemInHand(null);
						}else{
							player.getItemInHand().setAmount(amount);
						}
						//Launch Arrow
						player.launchProjectile(Arrow.class);
		    		}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if((event.getEntity().getShooter() instanceof Player) && (event.getEntity() instanceof Arrow)){
			final Player player = (Player) event.getEntity().getShooter();
			Arrow a = (Arrow) event.getEntity();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("IronMan")) && (game.getStarted == true)){
					//Change Arrow To Missile
	    		      double minAngle = 6.283185307179586D;
	    		      Entity minEntity = null;
	    		      for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
	    		        if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity)) && (!entity.isDead())) {
	    		          Vector toTarget = entity.getLocation().toVector().clone().subtract(player.getLocation().toVector());
	    		          double angle = a.getVelocity().angle(toTarget);
	    		          if (angle < minAngle) {
	    		            minAngle = angle;
	    		            minEntity = entity;
	    		          }
	    		        }
	    		      }
	    		      if (minEntity != null)
	    		        new ArrowHomingTask((Arrow)a, (LivingEntity)minEntity, plugin);
					//Cool
	    		    user.setString("Explosive");
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("IronMan")) && (game.getStarted == true))
    					event.setDamage(4);
    					//System.out.println("Iron Damage!");//------------------------DEBUG
    			}
    		}
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("IronMan")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
							eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.25)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("IronMan")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/4);
						}
						if((event.getCause() == DamageCause.BLOCK_EXPLOSION) || (event.getCause() == DamageCause.ENTITY_EXPLOSION)){
					    	event.setDamage(event.getDamage()/2);
						}
						if(event.getCause() == DamageCause.FIRE_TICK){
					    	event.setDamage(event.getDamage()/5);
						}
						int r = new Random().nextInt(10 - 1 + 1) + 1;
						//Stop Flying
						if(r == 5)
							stopFlying(game, player);
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getUnibeam(){
		ItemStack unibeam = new ItemStack(Material.BLAZE_ROD);
		ItemMeta meta = unibeam.getItemMeta();
		meta.setDisplayName("§4Unibeam");
		unibeam.setItemMeta(meta);
		return unibeam;
	}
	
	public ItemStack getPulsars(){
		ItemStack pulsars = new ItemStack(Material.DIAMOND);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§4Hand Pulsars");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getMissiles(){
		ItemStack pulsars = new ItemStack(Material.ARROW, 2);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§4Missile");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getFlight(){
		ItemStack fly = new ItemStack(Material.ANVIL);
		ItemMeta meta = fly.getItemMeta();
		meta.setDisplayName("§4Use to start flying");
		fly.setItemMeta(meta);
		return fly;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					//Stop Flying
					stopFlying(game, player);
				}
			}
		}catch(Exception e){}
	}
	
	public void stopFlying(Game game, Player player){
		//Stop Flying
		User user = game.getPlayerList.get(plugin.getIndex(player));
		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, false, user.getCooling, user.getString);
		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
		user.setJumping(false);
		player.setAllowFlight(false);
		for(ItemStack it : player.getInventory()){
			if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying"))) {
				ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
				ItemMeta meta = flyTog.getItemMeta();
				meta.setDisplayName("§4Use to start flying");
				flyTog.setItemMeta(meta);
				player.getInventory().remove(it);
				player.getInventory().addItem(flyTog);
    		}
		}
	}
    
}
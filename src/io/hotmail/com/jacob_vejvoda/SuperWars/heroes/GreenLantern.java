package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

@SuppressWarnings("deprecation")
public class GreenLantern extends Hero{
	Map<Player, ArrayList<Block>> constructList = new HashMap<Player, ArrayList<Block>>();
	
	public GreenLantern(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("GreenLantern")) && (game.getStarted == true)){
				Location l = player.getLocation();
				l.setY(l.getY()-2);
				Block b = l.getBlock();
				if(b.getType().equals(Material.AIR)){
					player.setAllowFlight(true);
				}else{
					player.setAllowFlight(false);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("GreenLantern")) && (game.getStarted == true)){
				try{
					ItemStack h = player.getItemInHand();
					if((h.getType().equals(Material.EMERALD_BLOCK)) && (h.getItemMeta().getDisplayName().equals("§aHardLight Blocks"))){
						if((user.isCooling() == false) && (event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (player.isSneaking())){
							//System.out.println("1");//------------------------DEBUG
							if (getTarget(player) != null) {
								Entity attacked = getTarget(player);
								if(attacked instanceof Player){
									Player p = (Player) attacked;
									Block head = (p.getEyeLocation()).getBlock();
									Block feet = (p.getLocation()).getBlock();
									//Feet
									if(feet.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)){
										feet.getRelative(BlockFace.DOWN).setType(Material.EMERALD_BLOCK);
										setAir(feet.getRelative(BlockFace.DOWN), 6);
									}
									if(feet.getRelative(BlockFace.NORTH).getType().equals(Material.AIR)){
										feet.getRelative(BlockFace.NORTH).setType(Material.EMERALD_BLOCK);
										setAir(feet.getRelative(BlockFace.NORTH), 6);
									}
									if(feet.getRelative(BlockFace.EAST).getType().equals(Material.AIR)){
										feet.getRelative(BlockFace.EAST).setType(Material.EMERALD_BLOCK);
										setAir(feet.getRelative(BlockFace.EAST), 6);
									}
									if(feet.getRelative(BlockFace.SOUTH).getType().equals(Material.AIR)){
										feet.getRelative(BlockFace.SOUTH).setType(Material.EMERALD_BLOCK);
										setAir(feet.getRelative(BlockFace.SOUTH), 6);
									}
									if(feet.getRelative(BlockFace.WEST).getType().equals(Material.AIR)){
										feet.getRelative(BlockFace.WEST).setType(Material.EMERALD_BLOCK);
										setAir(feet.getRelative(BlockFace.WEST), 6);
									}
									//Head
									if(head.getRelative(BlockFace.UP).getType().equals(Material.AIR)){
										head.getRelative(BlockFace.UP).setType(Material.EMERALD_BLOCK);
										setAir(head.getRelative(BlockFace.UP), 6);
									}
									if(head.getRelative(BlockFace.NORTH).getType().equals(Material.AIR)){
										head.getRelative(BlockFace.NORTH).setType(Material.EMERALD_BLOCK);
										setAir(head.getRelative(BlockFace.NORTH), 6);
									}
									if(head.getRelative(BlockFace.EAST).getType().equals(Material.AIR)){
										head.getRelative(BlockFace.EAST).setType(Material.EMERALD_BLOCK);
										setAir(head.getRelative(BlockFace.EAST), 6);
									}
									if(head.getRelative(BlockFace.SOUTH).getType().equals(Material.AIR)){
										head.getRelative(BlockFace.SOUTH).setType(Material.EMERALD_BLOCK);
										setAir(head.getRelative(BlockFace.SOUTH), 6);
									}
									if(head.getRelative(BlockFace.WEST).getType().equals(Material.AIR)){
										head.getRelative(BlockFace.WEST).setType(Material.EMERALD_BLOCK);
										setAir(head.getRelative(BlockFace.WEST), 6);
									}
									//Start Cool
									user.setCooling(true);
									startCoolTimer(player, "HardLight Encase", 20, 0);
								}
							}
						}else if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
							//System.out.println("2");//------------------------DEBUG
							//Get Clicked Block Directions
				    		Block clickedlBlock = player.getTargetBlock((Set<Material>)null, 200);
				    		if(clickedlBlock.getType().equals(Material.AIR))
				    			return;
				    		Block b1 = clickedlBlock.getRelative(BlockFace.DOWN);
				    		Block b2 = clickedlBlock.getRelative(BlockFace.EAST);
				    		Block b3 = clickedlBlock.getRelative(BlockFace.NORTH);
				    		Block b4 = clickedlBlock.getRelative(BlockFace.NORTH_EAST);
				    		Block b5 = clickedlBlock.getRelative(BlockFace.NORTH_WEST);
				    		Block b6 = clickedlBlock.getRelative(BlockFace.SOUTH);
				    		Block b7 = clickedlBlock.getRelative(BlockFace.SOUTH_EAST);
				    		Block b8 = clickedlBlock.getRelative(BlockFace.UP);
				    		Block b9 = clickedlBlock.getRelative(BlockFace.WEST);
				    		Block b10 = clickedlBlock.getRelative(BlockFace.SOUTH_WEST);
				    		//Add Them To List 
				    		ArrayList<distanceBlock> theDistance = new ArrayList<distanceBlock>();
				    		theDistance.add(new distanceBlock(b1.getLocation().distanceSquared(player.getLocation()), b1.getLocation()));
				    		theDistance.add(new distanceBlock(b2.getLocation().distanceSquared(player.getLocation()), b2.getLocation()));
				    		theDistance.add(new distanceBlock(b3.getLocation().distanceSquared(player.getLocation()), b3.getLocation()));
				    		theDistance.add(new distanceBlock(b4.getLocation().distanceSquared(player.getLocation()), b4.getLocation()));
				    		theDistance.add(new distanceBlock(b5.getLocation().distanceSquared(player.getLocation()), b5.getLocation()));
				    		theDistance.add(new distanceBlock(b6.getLocation().distanceSquared(player.getLocation()), b6.getLocation()));
				    		theDistance.add(new distanceBlock(b7.getLocation().distanceSquared(player.getLocation()), b7.getLocation()));
				    		theDistance.add(new distanceBlock(b8.getLocation().distanceSquared(player.getLocation()), b8.getLocation()));
				    		theDistance.add(new distanceBlock(b9.getLocation().distanceSquared(player.getLocation()), b9.getLocation()));
				    		theDistance.add(new distanceBlock(b10.getLocation().distanceSquared(player.getLocation()), b10.getLocation()));
				    		//Get Closest Block
				    		distanceBlock smallest = new distanceBlock(clickedlBlock.getLocation().distanceSquared(player.getLocation()), clickedlBlock.getLocation());
				    		for (distanceBlock newNum : theDistance){
				    			if ((newNum.distance < smallest.distance) && (newNum.location.getBlock().getType().equals(Material.AIR))){
				    				smallest = newNum;
				    			}
				    		}
				    		//Set Block
				    		final Block EmBlock = smallest.location.getBlock();
				    		EmBlock.setType(Material.EMERALD_BLOCK);
				    		//Add Block To List
				    		setAir(EmBlock, 30);
				    		//Remove Block Timer
				    		if (EmBlock != null){
								Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								     public void run() {
								    	 EmBlock.setType(Material.AIR);
									 }
								}, (30 * 20));
			    			}
						}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) && (player.isSneaking())){
							//System.out.println("3");//------------------------DEBUG
			    			//Change to Sword
							player.setItemInHand(getSword());
							//player.getInventory().addItem(getSword());
							player.updateInventory();
						}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) && (!user.isCooling("throwBlock"))){
							//System.out.println("4");//------------------------DEBUG
			    			//Set Cooling
							quickCool(player, "throwBlock", 5);
							//Throw Block
							FallingBlock block = event.getPlayer().getWorld().spawnFallingBlock(event.getPlayer().getEyeLocation(), player.getItemInHand().getType(), (byte) player.getItemInHand().getDurability());
							block.setVelocity((player.getLocation().getDirection().multiply(1.5)));
							entityCauseDamage(player, block, 4);
						}
		    		}else if((h.getType().equals(Material.DIAMOND_SWORD)) && (player.isSneaking()) && (h.getItemMeta().getDisplayName().equals("§aHardLight sword"))){
		    			//Change to Block
						player.setItemInHand(getBuild());
						player.updateInventory();
					}else if ((player.getItemInHand().getType().equals(Material.SLIME_BALL)) && (h.getItemMeta().getDisplayName().equals("§aGreen Light Beam"))){
		    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (!user.isCooling("shoot"))){
			    			//Set Cooling
		    				quickCool(player, "shoot", 5);
			    			//Shoot Beam
			    			Location eyeLoc = player.getEyeLocation();
			    			double px = eyeLoc.getX();
			    			double py = eyeLoc.getY();
			    			double pz = eyeLoc.getZ();
			    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
			    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
			    			double x = Math.sin(pitch) * Math.cos(yaw);
			    			double y = Math.sin(pitch) * Math.sin(yaw);
			    			double z = Math.cos(pitch);
			    			for (int j = 1 ; j <= 10 ; j++) {
								if (getTarget(player) != null) {
									Entity attacked = getTarget(player);
									if ((attacked instanceof LivingEntity)) {
									((LivingEntity)attacked).damage((int)Math.round(2.0D * 4));
									((LivingEntity)attacked).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1), true);
									}
								}
			    			}
			    			for (int i = 1 ; i <= 50 ; i++) {
				    			Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
				    			//player.playEffect(loc, Effect.STEP_SOUND, 133);
				    			//ParticleEffects.sendToLocation(ParticleEffects.SLIME, loc, 0, 0, 0, 1, 3);
				    			plugin.displayParticles("SLIME", loc, 1, 3);
			    				if(loc.getBlock().getType() != Material.AIR)
			    					i = 50;
				    		}
		    			}
		    		}else if((player.getItemInHand().getType().equals(Material.EMERALD)) && (h.getItemMeta().getDisplayName().equals("§aLevitate"))){
		    			//System.out.println("In Emrald " + event.getAction());//------------------------DEBUG
		    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
		    				Vector v = player.getVelocity();
		    				//Levitate
		    				v = v.setY(1);
		    				player.setVelocity(v);
		    			}else if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)){
		    				//System.out.println("Phase");//------------------------DEBUG
		    				Block feet = event.getClickedBlock().getRelative(event.getBlockFace().getOppositeFace());
		    				Location hl = feet.getLocation();
		    				hl.setY(hl.getY()+1);
		    				Block head = hl.getBlock();
		    				if((plugin.transBlocks.contains(head.getTypeId())) && (plugin.transBlocks.contains(feet.getTypeId()))){
		    					player.teleport(feet.getLocation());
		    				}
		    			}
		    		}
				}catch(Exception e){}
		    }
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent event){
		Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("GreenLantern")) && (game.getStarted == true)){
				try{
					//System.out.println("Placed " + player.getItemInHand().getType());//--------------DEBUG
					ItemStack h = player.getItemInHand();
					if ((h.getType().equals(Material.EMERALD_BLOCK)) && (h.getItemMeta().getDisplayName().equals("§aHardLight Blocks"))){
						player.getInventory().addItem(h);
						player.updateInventory();
						setAir(event.getBlock(), 6);
					}
				}catch(Exception e){};
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onFallingBlockLand(final EntityChangeBlockEvent event){
	  if(event.getEntity() instanceof FallingBlock){
	   // FallingBlock fallingBlock = (FallingBlock) event.getEntity();
	    if(event.getBlock().getType() == Material.AIR){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
			    	 setAir(event.getBlock(), 30);
				 }
			}, (1));
	    }
	  }
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("GreenLantern")){
						if(event.getCause() == DamageCause.FALL){
							Location l = player.getLocation();
							l.setY(l.getY()-1);
							if(l.getBlock().getType().equals(Material.EMERALD_BLOCK))
								event.setDamage(0);
							else
								event.setDamage(event.getDamage()/4);
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("GreenLantern")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
    					}else if((attacker.getItemInHand().getType().equals(Material.DIAMOND_SWORD)) && (attacker.getItemInHand().getItemMeta().getDisplayName().equals("§aHardLight sword"))){
    						event.setDamage(3.5);
    					}
					}
    			}
			}
        }catch(Exception e){}
	}
	
	public ItemStack getBuild(){
		ItemStack unibeam = new ItemStack(Material.EMERALD_BLOCK);
		ItemMeta meta = unibeam.getItemMeta();
		meta.setDisplayName("§aHardLight Blocks");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§2Right click to build HardLight blocks");
		loreList.add("§2You can build at any distance");
		loreList.add("§2Left click to throw HardLight blocks");
		loreList.add("§2Sneak-Left click to make a HardLight sword");
		loreList.add("§2Sneak-Right click to encase");
		loreList.add("§2your target in HardLight blocks");
		meta.setLore(loreList);
		unibeam.setItemMeta(meta);
		return unibeam;
	}
	
	public ItemStack getFly(){
		ItemStack pulsars = new ItemStack(Material.SLIME_BALL);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§aGreen Light Beam");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§2Right click to shoot green light beam");
		meta.setLore(loreList);
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getBeam(){
		ItemStack fly = new ItemStack(Material.EMERALD);
		ItemMeta meta = fly.getItemMeta();
		meta.setDisplayName("§aLevitate");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§2Right click to to levitate");
		loreList.add("§2Left click to phase through walls");
		meta.setLore(loreList);
		fly.setItemMeta(meta);
		return fly;
	}
	
	public ItemStack getSword(){
		ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta meta = sword.getItemMeta();
		meta.setDisplayName("§aHardLight sword");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§2Sneak-Left click to go back to HardLight blocks");
		meta.setLore(loreList);
		sword.setItemMeta(meta);
		sword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
		return sword;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
		}
	}
    
}


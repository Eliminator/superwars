package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class SpiderMan extends Hero{
	Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
	public static ArrayList<String> climbingPlayers = new ArrayList<String>();
	
	public SpiderMan(SuperWars instance) {
		super(instance);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		try{
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				String hero = plugin.getHero(player);
				Block fix = player.getLocation().getBlock();
				if(plugin.climbHeros.contains(hero) && (game.getStarted == true) && (!fix.getType().equals(Material.STONE_PLATE)) && (!fix.getType().equals(Material.WOOD_PLATE))){
			        BlockFace bf = yawToFace(player.getLocation().getYaw());
			        Block block = player.getLocation().getBlock().getRelative(bf);
			        if (block.getType() != Material.AIR) {
			          for (int i = 0; i < 300; i++) {
			            Block temp = block.getLocation().add(0.0D, i, 0.0D).getBlock();
			            Block opp = player.getLocation().add(0.0D, i, 0.0D).getBlock();
			            Block aboveOpp = opp.getLocation().add(0.0D, 1.0D, 0.0D).getBlock();
			            int counter = 0;
			            for (int k = 0; k < plugin.transBlocks.size(); k++) {
			              if ((temp.getType() != Material.AIR) && (temp.getTypeId() != plugin.transBlocks.get(k).intValue()))
			                counter++;
			            }
			            try {
			            if ((counter != plugin.transBlocks.size()) || ((opp.getType() != Material.AIR))) break;
			            }finally{}
			            if (aboveOpp.getType() == Material.AIR) {
			              player.sendBlockChange(opp.getLocation(), Material.VINE, (byte)0);
			              addVines(player, opp);
			            }
			            player.setFallDistance(0.0F);
			          }
	
			        }else{
			          for (int i = 0; i < getVines(player).size(); i++) {
			            player.sendBlockChange(((Block)getVines(player).get(i)).getLocation(), Material.AIR, (byte)0);
			          }
			          getVines(player).clear();
			        }
			        Location tophead = player.getEyeLocation();
			        tophead.setY(tophead.getY() +1);
			        Location bottomFeet = player.getLocation();
			        bottomFeet.setY(bottomFeet.getY() -1);
			        if ((!tophead.getBlock().getType().equals(Material.AIR)) && (bottomFeet.getBlock().getType().equals(Material.AIR))) {
			        	player.setAllowFlight(true);
			        	player.setFlying(true);
			        	//System.out.println("Flying!");
			        }else if (tophead.getBlock().getType().equals(Material.AIR) && (player.isFlying())) {
			        	player.setFlying(false);
			        	player.setAllowFlight(false);
			        	//System.out.println("Falling!");
			        }
		        }
		    }
		}catch(Exception e){/**System.out.println("Spidey Climb: " + e);**/}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onSpideyInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if((plugin.getGame(player) != null) && (player.getItemInHand() != null) && (!event.getAction().equals(Action.LEFT_CLICK_BLOCK)) && (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
			final Game game = plugin.getGame(player);
			String hero = plugin.getHero(player);
			if((hero.equals("SpiderMan")) && (game.getStarted == true)){
				try{
					User user = game.getPlayerList.get(plugin.getIndex(player));
					if(!user.isCooling("web")) {
						boolean cool = false;
						boolean sence = false;
						if((player.getItemInHand().getType().equals(Material.WEB)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7Web Shot"))){
							//Set Right or Left Click
				    		//User userCool = null;
				    		if((player.isSneaking()) && (user.isCooling() == false)){
				    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				    				//Nest
				    				//Cool
				    				user.setString("nest");
				    				user.setJumping(true);
				    				user.setCooling(true);
									startCoolTimer(player, "Super Web", 25, 0);
									cool = true;
				    			}
				    		}else if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
				    			//Make Web
			    				user.setString("make");
			    				user.setJumping(true);
			    				cool = true;
				    		}else if(event.getAction().equals(Action.LEFT_CLICK_AIR)) {
				    			//Remove Web
			    				user.setString("remove");
			    				user.setJumping(true);
			    				cool = true;
				    		}
						}else if((player.getItemInHand().getType().equals(Material.STRING)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7String Shot"))){
							//Set Right or Left Click
				    		if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
				    			//Grapple String
				    			user.setJumping(true);
				    			user.setString("grapple");
				    			cool = true;
				    		}else if(event.getAction().equals(Action.LEFT_CLICK_AIR)) {
				    			//Bullet String
				    			user.setJumping(true);
				    			user.setString("bullet");
				    			cool = true;
				    			sence = true;
				    		}
						}
						if(cool == true){
							//Make Gun Sound
							player.playSound(player.getLocation(), Sound.BLOCK_CLOTH_BREAK, 2F, 1F);
							//Fire Gun Bullet
							Snowball ball = player.getWorld().spawn(player.getEyeLocation(), Snowball.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(3));
			    			//Start Homing
							if(sence == true){
				    		      double minAngle = 6.283185307179586D;
				    		      Entity minEntity = null;

				    		      for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
				    		        if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity)) && (!entity.isDead())) {
				    		          Vector toTarget = entity.getLocation().toVector().clone().subtract(player.getLocation().toVector());
				    		          double angle = ball.getVelocity().angle(toTarget);
				    		          if (angle < minAngle) {
				    		            minAngle = angle;
				    		            minEntity = entity;
				    		          }
				    		        }
				    		      }

				    		      if (minEntity != null)
				    		        new HomingTask((Snowball)ball, (LivingEntity)minEntity, plugin);
							}
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		if(event.getDamager() instanceof Snowball){
    			Snowball sb = (Snowball) event.getDamager();
    			if(sb.getShooter() instanceof Player){
    				final Player shooter = (Player) sb.getShooter();
    				Game game = plugin.getGame(shooter);
    				User user = game.getPlayerList.get(plugin.getIndex(shooter));
					if(user.getString().equals("bullet")){
						//Bullet String
    					event.setDamage(2);
    					//System.out.println("Bullet String");//------------------------DEBUG
	    				user.setJumping(false);
					}
    			}
    		}
    		if(event.getDamager() instanceof Player){
    			Player attacker = (Player) event.getDamager();
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("SpiderMan")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR))
							event.setDamage(2);
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onProjectileHitEvent(ProjectileHitEvent event){
		try{
			if(event.getEntity() instanceof Snowball){
				Snowball sb = (Snowball) event.getEntity();
				if(sb.getShooter() instanceof Player){
					final Player shooter = (Player) sb.getShooter();
					Game game = plugin.getGame(shooter);
					User user = game.getPlayerList.get(plugin.getIndex(shooter));
					if((user.getHero().equals("SpiderMan")) && (game.getStarted == true)){
						Location l = event.getEntity().getLocation();
						final Block b = l.getBlock();
						if(user.getString().equals("make")){
							//Make Web
							if(b.getType().equals(Material.AIR)){
								b.setType(Material.WEB);
								setAir(b, 30);
							}
	    					//System.out.println("Made Web on " + b.getType());//------------------------DEBUG
						}else if(user.getString().equals("remove")){
							//Remove Web
							//setAir(l.getBlock(), 3);
							setNearToAir(b, Material.WEB);
	    					//System.out.println("Remove Web");//------------------------DEBUG
						}else if(user.getString().equals("grapple")){
							//Grapple String
							try{
								hookPull((Entity)shooter, sb.getLocation());
							}catch(Exception e){}
						}else if(user.getString().equals("nest")){
							//Web Nest
							List<Block> blocks = getSphere(b, 4);
							for(Block bl : blocks){
								if(bl.getType().equals(Material.AIR)){
									bl.setType(Material.WEB);
									setAir(bl, 30);
								}
							}
						}
	    				user.setJumping(false);
					}
				}
			}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("SpiderMan"))){
				    	event.setDamage(event.getDamage()/5);
					}
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event){
		try{
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("SpiderMan")) && (game.getStarted == true)){
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.WEB)) && (name.contains("§7Web Shot"))){
						event.setCancelled(true);
						player.updateInventory();
					}else if((item.getType().equals(Material.STRING)) && (name.contains("§7String Shot"))){
						event.setCancelled(true);
						player.updateInventory();
					}
				}
			}
		}catch(Exception e){}
	}
	
	public ItemStack getWeb(){
		ItemStack web = new ItemStack(Material.WEB);
		ItemMeta meta = web.getItemMeta();
		meta.setDisplayName("§7Web Shot");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to make web");
		loreList.add("§8Left click to remove web");
		loreList.add("§8Sneak-right click to make a super web");
		meta.setLore(loreList);
		web.setItemMeta(meta);
		return web;
	}
	
	public ItemStack getString(){
		ItemStack string = new ItemStack(Material.STRING);
		ItemMeta meta = string.getItemMeta();
		meta.setDisplayName("§7String Shot");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to shoot grapple web");
		loreList.add("§8Left click to fire bullet web");
		meta.setLore(loreList);
		string.setItemMeta(meta);
		return string;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 220, 1), true);
	    	try{
	    		User userCooler = game.getPlayerList.get(plugin.getIndex(player));
	    	    if(userCooler.isJumping() == true){
			    	//User userDoneCooling = new User(userCooler.getPlayer, userCooler.getPreviousStuff, userCooler.getHero, userCooler.getLives, userCooler.getSneaking, false, userCooler.getCooling, userCooler.getString);
			    	//game2.getPlayerList.set(plugin.getIndex(userCooler.getPlayer), userDoneCooling);
	    	    	userCooler.setJumping(false);
	    	    }
	    	}catch(Exception e){}
		}
	}
	
    public ArrayList<Block> getVines(Player player)
    {
      if (this.vineMap.containsKey(player.getName())) {
        return this.vineMap.get(player.getName());
      }
      ArrayList<Block> temp = new ArrayList<Block>();
      return temp;
    }

    public void setVines(Player player, ArrayList<Block> vines)
    {
      this.vineMap.put(player.getName(), vines);
    }

    public void addVines(Player player, Block vine)
    {
      ArrayList<Block> updated = new ArrayList<Block>();
      updated = getVines(player);
      updated.add(vine);
      setVines(player, updated);
    }

    public BlockFace yawToFace(float yaw) {
      BlockFace[] axis = { BlockFace.SOUTH, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST };
      return axis[(java.lang.Math.round(yaw / 90.0F) & 0x3)];
    }
}
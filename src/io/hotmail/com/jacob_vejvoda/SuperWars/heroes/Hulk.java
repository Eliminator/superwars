package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Hulk extends Hero{
	
	public Hulk(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onHulkMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Hulk")) && (game.getStarted == true)){
				try{
					Location loc1 = player.getLocation();
					loc1.setY(loc1.getY() -1);
					if ((loc1.getBlock().getType().equals(Material.AIR) && (user.isJumping()) == false)) {
						//Set Player Jumping
						user.setJumping(true);
			    	}else if ((!loc1.getBlock().getType().equals(Material.AIR)) && ((user.isJumping()) == true)) {
			    		boolean cooling = user.isCooling();
						if ((user.isSneaking() == true) && (user.isCooling() == false)) {
							//Create Boom
							Location loc = player.getLocation();
							loc.getWorld().createExplosion(loc, 5);	
							//Damage Nearby Entities
							for(Entity e : player.getNearbyEntities(6, 6, 6)){
								if(e instanceof LivingEntity)
									((LivingEntity)e).damage((int)Math.round(2.0D * 12));
							}
							//Set Player Cooling
							cooling = true;
							startCoolTimer(player, "Smash Ability", 20, 0);
						}
						//Set Jumping False (Also Start Cooling Posibly)
						//User userCooling = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, false, cooling, user.getString);
				    	//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCooling);
						user.setJumping(false);
						user.setCooling(cooling);
			    	}
				    
				    Location skyhead = player.getEyeLocation();
				    skyhead.setY(skyhead.getY() +1);
				    if (skyhead.getBlock().getType().equals(Material.AIR) && (player.isFlying()) && (player.getAllowFlight() == false)) {
				    	player.kickPlayer("You have been kicked for flying.");
				    }
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		//System.out.println("Toggle Fly");//------------------------DEBUG
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Hulk")) && (game.getStarted == true)){
				event.setCancelled(true);
				//System.out.println("String " + user.getString);//------------------------DEBUG
				if(!user.isCooling("highJump")){
					//High Jump
					Vector v = (player.getLocation().getDirection().multiply(1));
					v = v.setY(1.8);
					player.setVelocity(v);
					//Jump Cool
					quickCool(player, "highJump", 5);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onHulkInteract(PlayerInteractEvent event) {
		try{
			if(event.getAction().equals(Action.PHYSICAL))
				return;
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				//System.out.println("H1");//------------------------DEBUG
				if((user.getHero().equals("Hulk")) && (game.getStarted == true) && (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) && (player.getItemInHand().getType().equals(Material.AIR))){
					//System.out.println("H3");//------------------------DEBUG
					if(!user.isCooling("highJump")){
						//Create Boom
						Location loc = event.getClickedBlock().getLocation();
						loc.getWorld().createExplosion(loc, 3);	
						//Start Cooling
						quickCool(player, "highJump", 5);
					}
				}else if((player.isSneaking()) && (user.getHero().equals("Hulk")) && (game.getStarted == true) && (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && (player.getItemInHand().getType().equals(Material.AIR))){
					//System.out.println("H5");//------------------------DEBUG
					int itemsAmount = 0;
					for(ItemStack iStack : player.getInventory())
						if(iStack != null)
							itemsAmount = itemsAmount + iStack.getAmount();
					//System.out.println("Player Items: " + itemsAmount);//------------------------DEBUG
					if(itemsAmount <= 32){
						Block b = event.getClickedBlock();
						if(!plugin.transBlocks.contains(b.getType())){
							//Get Item
							ItemStack i = new ItemStack(b.getType(), 1, b.getData());
							ItemMeta m = i.getItemMeta();
							m.setDisplayName("Throwable Block");
							i.setItemMeta(m);
							//Remove Block
							b.setType(Material.AIR);
							//Add Item To Player
							player.getInventory().addItem(i);
							player.updateInventory();
						}
					}else{
						player.sendMessage("§2SuperWars: You can't carry any more blocks!");
					}
				}else if((user.getHero().equals("Hulk")) && (game.getStarted == true) && (event.getAction().equals(Action.LEFT_CLICK_AIR))){
					if(player.getItemInHand().getItemMeta().getDisplayName().equals("Throwable Block")){
						//Throw Block
						FallingBlock block = event.getPlayer().getWorld().spawnFallingBlock(event.getPlayer().getEyeLocation(), player.getItemInHand().getType(), (byte) player.getItemInHand().getDurability());
						block.setVelocity((player.getLocation().getDirection().multiply(1.5)));
						entityCauseDamage(player, block, 4);
						//Remove Block
						int amount = player.getItemInHand().getAmount() - 1;
						if(amount <= 0){
							player.setItemInHand(null);
						}else{
							player.getItemInHand().setAmount(amount);
						}
					}
				}
			}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		if(event.getDamager() instanceof Player){
    			Player attacker = (Player) event.getDamager();
    			Entity victom = event.getEntity();
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("Hulk")) && (game.getStarted == true)){
						if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(3.5);
							victom.setVelocity((attacker.getLocation().getDirection().multiply(2)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onSneak(PlayerToggleSneakEvent event){
	    final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			boolean sneak;
		    if(event.getPlayer().isSneaking()){
		    	sneak = false;
		    }else
		    	sneak = true;
			Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Hulk")) && (game.getStarted == true)){
				user.setSneaking(sneak);
	    	}
	    }
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("Hulk")){
						if(user.getHero().equals("Hulk")){
							if(event.getCause().equals(DamageCause.FALL)){
					    	event.setDamage(event.getDamage()/8);
							}else if((event.getCause().equals(DamageCause.BLOCK_EXPLOSION)) || (event.getCause().equals(DamageCause.ENTITY_EXPLOSION))){
								event.setDamage(event.getDamage()/6);
							}
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
		}
	}
    
}
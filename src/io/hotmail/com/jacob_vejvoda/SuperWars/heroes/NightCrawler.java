package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class NightCrawler extends Hero{
	Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
	public static ArrayList<String> climbingPlayers = new ArrayList<String>();
	
	public NightCrawler(SuperWars instance) {
		super(instance);
	}
	
	@SuppressWarnings({ "unused" })
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if((plugin.getGame(player) != null) && (player.getItemInHand() != null)){
			final Game game = plugin.getGame(player);
			String hero = plugin.getHero(player);
			if((hero.equals("NightCrawler")) && (game.getStarted == true)){
				try{
					User user = game.getPlayerList.get(plugin.getIndex(player));
					event.setCancelled(true);
					player.updateInventory();
					if(user.isJumping() == false) {
						Location l = player.getTargetBlock((Set<Material>)null, 200).getLocation();
						boolean cool = false;
						if((player.getItemInHand().getType().equals(Material.ENDER_PEARL)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7Self Teleport Control"))){
							//Set Right or Left Click
				    		User userCool = null;
				    		if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				    			//TP Self Up
				    			tpUp(player, 5);
				    			user.setString("make");
				    			cool = true;
				    		}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))) {
				    			if(!l.getBlock().getType().equals(Material.AIR)){
					    			l.setY(l.getY()+2);
					    			//TP Self To Mouse
					    			tpSpot(player, l);
					    			user.setString("remove");
					    			cool = true;
				    			}
				    		}
						}else if((player.getItemInHand().getType().equals(Material.EYE_OF_ENDER)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7All Teleport Control"))){
							//Set Right or Left Click
				    		User userCool = null;
				    		if((player.isSneaking()) && (user.isCooling() == false)){
				    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				    				//Summon
						    		l.setY(l.getY()+2);
				    				summonC(l, player);
				    				//Cool
				    				user.setCooling(true);
				    				user.setString("nest");
									startCoolTimer(player, "Creature Summon", 30, 0);
				    			}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
					    			//TP All Up
					    			for(Entity e : player.getNearbyEntities(4, 4, 4)){
					    				tpUp(e, 15);
					    			}
					    			tpUp(player, 15);
				    				//Cool
				    				user.setCooling(true);
				    				user.setString("nest");
									startCoolTimer(player, "High Sky", 20, 0);
				    			}
				    		}else if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				    			//TP All Up
				    			for(Entity e : player.getNearbyEntities(3, 3, 3)){
				    				tpUp(e, 5);
				    			}
				    			tpUp(player, 5);
			    				user.setString("grapple");
			    				cool = true;
				    		}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))) {
				    			if(!l.getBlock().getType().equals(Material.AIR)){
					    			//TP All To Mouse
						    		l.setY(l.getY()+2);
					    			for(Entity e : player.getNearbyEntities(3, 3, 3)){
					    				tpSpot(e, l);
					    			}
					    			tpSpot(player, l);
				    				user.setString("bullet");
				    				cool = true;
				    			}
				    		}
						}
						if(cool == true){
			    			user.setJumping(true);
							//Cool
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
							    	try{
							    		Game game = plugin.getGame(player);
							    		User userCooler = game.getPlayerList.get(plugin.getIndex(player));
							    		userCooler.setJumping(false);
							    	}catch(Exception e){}
								}
							},(5 * 20));
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		final Player player = event.getPlayer();
		if((plugin.getGame(player) != null) && (player.getItemInHand() != null)){
			final Game game = plugin.getGame(player);
			String hero = plugin.getHero(player);
			if((hero.equals("NightCrawler")) && (game.getStarted == true)){
				try{
					if((player.getItemInHand().getType().equals(Material.ARROW)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7Spiked Tail"))){
						player.setItemInHand(null);
						Entity grabbed = event.getRightClicked();
						grab(player, grabbed, 2);
					}
				}catch(Exception e){}
			}
		}
	}
	
	public void grab(final Player p, final Entity e, double time){
    	try{
    		if(time < 0.1){
    			((LivingEntity)e).damage((int)Math.round(2.0D * 1));
    			if(e instanceof Player)
    				((Player)e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1), true);
    		}
    		if(time > 0){
				//Grab
				Location l = p.getLocation();
				l.setX(l.getX() - 1);
				e.teleport(l);
				final double newTime = time - 0.05;
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
				    	grab(p, e, newTime);
					}
				},(1));
    		}else{
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Game game = plugin.getGame(p);
						User user = game.getPlayerList.get(plugin.getIndex(p));
		    			plugin.giveItems(p, user.getHero(), true, -1);
					}
				},(20 * 3));
    		}
    	}catch(Exception ex){}
	}
	
	public void tpSpot(Entity e, Location l){
		if(e instanceof Player){
			Player p = (Player) e;
			//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, p.getEyeLocation(), 0, 0, 0, 0, 15);
			//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, p.getLocation(), 0, 0, 0, 0, 15);
			//ParticleEffects.sendToLocation(ParticleEffects.ENDER, p.getEyeLocation(), 0, 0, 0, 1, 15);
			//ParticleEffects.sendToLocation(ParticleEffects.ENDER, p.getLocation(), 0, 0, 0, 1, 15);
			plugin.displayParticles("LARGE_SMOKE", p.getEyeLocation(), 0, 15);
			plugin.displayParticles("LARGE_SMOKE", p.getLocation(), 0, 15);
			plugin.displayParticles("ENDER", p.getEyeLocation(), 1, 15);
			plugin.displayParticles("ENDER", p.getLocation(), 1, 15);
		}else{
			//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, e.getLocation(), 0, 0, 0, 0, 15);
			//ParticleEffects.sendToLocation(ParticleEffects.ENDER, e.getLocation(), 0, 0, 0, 1, 15);
			plugin.displayParticles("LARGE_SMOKE", e.getLocation(), 0, 15);
			plugin.displayParticles("ENDER", e.getLocation(), 1, 15);
		}
		e.getWorld().playSound(l, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 2F);
		e.teleport(l);
	}
	
	public void tpUp(Entity e, int min){
		if(e instanceof Player){
			Player p = (Player) e;
			plugin.displayParticles("LARGE_SMOKE", p.getEyeLocation(), 0, 15);
			plugin.displayParticles("LARGE_SMOKE", p.getLocation(), 0, 15);
			plugin.displayParticles("ENDER", p.getEyeLocation(), 1, 15);
			plugin.displayParticles("ENDER", p.getLocation(), 1, 15);
		}else{
			plugin.displayParticles("LARGE_SMOKE", e.getLocation(), 0, 15);
			plugin.displayParticles("ENDER", e.getLocation(), 1, 15);
		}
		Location l = e.getLocation();
		e.getWorld().playSound(l, Sound.ENTITY_ENDERMEN_TELEPORT, 1F, 2F);
		int max = min + 10;
		int hight = new Random().nextInt((max - min) + 1) + min;
		l.setY(l.getY() + hight);
		e.teleport(l);
	}
	
	public void summonC(Location l, Player p){
		//Make Effects
		//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, l, 0, 0, 0, 0, 8);
		//ParticleEffects.sendToLocation(ParticleEffects.BIG_EXPLODE, l, 0, 0, 0, 1, 1);
		//ParticleEffects.sendToLocation(ParticleEffects.ENDER, l, 0, 0, 0, 1, 15);
		plugin.displayParticles("LARGE_SMOKE", l, 0, 8);
		plugin.displayParticles("BIG_EXPLODE", l, 1, 1);
		plugin.displayParticles("ENDER", l, 1, 15);
		l.getWorld().playSound(l, Sound.ENTITY_WITHER_SPAWN, 1F, 2F);
		//Get Mobs
		ArrayList<EntityType> mobs = new ArrayList<EntityType>();
		mobs.add(EntityType.BLAZE);
		mobs.add(EntityType.GHAST);
		mobs.add(EntityType.MAGMA_CUBE);
		mobs.add(EntityType.PIG_ZOMBIE);
		mobs.add(EntityType.SKELETON);
		//Pick Random Mob
		int i = new Random().nextInt(mobs.size());
		EntityType et = mobs.get(i);
		Entity e = l.getWorld().spawnEntity(l, et);
		if(e.getType().equals(EntityType.SKELETON)){
			Skeleton s = (Skeleton) e;
			s.setSkeletonType(SkeletonType.WITHER);
		}
		mobRemoveTimer(p, e);
	}
	
	public void mobRemoveTimer(final Player p, final Entity e){
		try{
			if(plugin.getGame(p) != null){
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						mobRemoveTimer(p, e);
					}
				},(10 * 20));
			}else{
				if(e.isDead() == false)
					e.remove();
			}
		}catch(Exception ex){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		if(event.getDamager() instanceof Player){
    			Player attacker = (Player) event.getDamager();
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("NightCrawler")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.ARROW))
							event.setDamage(3.5);
    				}
				}
    		}
        }catch(Exception e){System.out.println("Night Damage: " + e);}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("NightCrawler"))){
				    	event.setDamage(event.getDamage()/2);
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getTP(){
		ItemStack web = new ItemStack(Material.ENDER_PEARL);
		ItemMeta meta = web.getItemMeta();
		meta.setDisplayName("§7Self Teleport Control");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to TP up");
		loreList.add("§8Left click to TP to where your looking");
		meta.setLore(loreList);
		web.setItemMeta(meta);
		return web;
	}
	
	public ItemStack getTP2(){
		ItemStack web = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta meta = web.getItemMeta();
		meta.setDisplayName("§7All Teleport Control");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to TP up");
		loreList.add("§8Left click to TP to where your looking");
		loreList.add("§8Sneak-right click to let a creature escape");
		loreList.add("§8Sneak-left click to TP high into the sky");
		meta.setLore(loreList);
		web.setItemMeta(meta);
		return web;
	}
	
	public ItemStack getTail(){
		ItemStack tail = new ItemStack(Material.ARROW);
		ItemMeta meta = tail.getItemMeta();
		meta.setDisplayName("§7Spiked Tail");
		tail.setItemMeta(meta);
		return tail;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(3 * plugin.getArmour())), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 220, 1), true);
		}
	}
}
package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Storm extends Hero{
	
	public Storm(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			try{
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("Storm")) && (game.getStarted == true)){
					String state = user.getString();
					//System.out.println("State = " + state);
					if(state.equals("on")){
						player.setVelocity(player.getLocation().getDirection().multiply(1));
					}else if(state.equals("hover")){
						player.setVelocity(new Vector(0,0,0));
					}
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Storm")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.NETHER_STAR)) && (name.contains("§fWeather"))){
						//Check If Raining
						if(player.getWorld().hasStorm() == true){
							//Weather
							if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
								if(user.isCooling("lightning") == false){
									//Strike Lightning
						    		Location strike = player.getTargetBlock((Set<Material>)null, 200).getLocation();
						    		player.getWorld().strikeLightning(strike);
						    		//Create Boom
									Double x = strike.getX();
									Double y = strike.getY();
									Double z = strike.getZ();
									strike.getWorld().createExplosion(x, y, z, 2, true, true);
									//Set Cool
									quickCool(player, "lightning", 3);
								}
							}else if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
								//Tornado
								if(user.isCooling() == false){
									spawnTornado(plugin, player.getTargetBlock((Set<Material>)null, 200).getLocation(), Material.WEB, (byte) 0, player.getLocation().getDirection(), 2, 50, 1000, true, true);
									//Set Cool
									user.setCooling(true);
						    		//Start Cooling Timer
						    		startCoolTimer(player, "Tornado", 30, 0);
								}
							}
						}
					}else if((item.getType().equals(Material.SNOW_BALL)) && (name.contains("§fWind"))){
						event.setCancelled(true);
						player.updateInventory();
						//Wind
						if(user.isCooling("wind") == false){
							if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
								//Freeze
								if (getTarget(player) != null) {
									Entity attacked = getTarget(player);
									if ((attacked instanceof LivingEntity)) {
										((LivingEntity)attacked).damage((int)Math.round(2.0D));
										((LivingEntity)attacked).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 110, 2), true);
										//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, attacked.getLocation(), 0, 0, 0, 1, 15);
										plugin.displayParticles("LARGE_SMOKE", attacked.getLocation(), 1, 15);
									}
								}
							}else if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
								//Blow
				    			Location eyeLoc = player.getEyeLocation();
				    			double px = eyeLoc.getX();
				    			double py = eyeLoc.getY();
				    			double pz = eyeLoc.getZ();
				    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
				    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
				    			double x = Math.sin(pitch) * Math.cos(yaw);
				    			double y = Math.sin(pitch) * Math.sin(yaw);
				    			double z = Math.cos(pitch);
				    			for (int j = 1 ; j <= 10 ; j++) {
									if (getTarget(player) != null) {
										Entity attacked = getTarget(player);
										if ((attacked instanceof LivingEntity)) {
											((LivingEntity)attacked).damage((int)Math.round(2.0D * 6));
											//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, attacked.getLocation(), 0, 0, 0, 1, 15);
											plugin.displayParticles("LARGE_SMOKE", attacked.getLocation(), 1, 15);
											//Push
											attacked.setVelocity((player.getLocation().getDirection().multiply(2.5)));
										}
									}
				    			}
				    			for (int i = 1 ; i <= 15 ; i++) {
				    				Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
					    			//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, loc, 0, 0, 0, 1, 5);
				    				plugin.displayParticles("LARGE_SMOKE", loc, 1, 5);
					    			if(loc.getBlock().getType() != Material.AIR)
					    				i = 50;
					    		}
							}
							//Set Cool
							quickCool(player, "wind", 8);
						}
					}else if((item.getType().equals(Material.BUCKET)) && (name.contains("§9Rain"))){
						//Rain On
						player.getWorld().setStorm(true);
						player.sendMessage("Calling rain clouds...");
						player.setItemInHand(getRain("on"));
						player.updateInventory();
					}else if((item.getType().equals(Material.WATER_BUCKET)) && (name.contains("§9Rain"))){
						//Rain Off
						player.getWorld().setStorm(false);
						player.sendMessage("Dispersing rain clouds...");
						player.setItemInHand(getRain("off"));
						event.setCancelled(true);
						player.updateInventory();
					}else if((item.getType().equals(Material.FEATHER)) && (name.contains("§7Flight"))){
						//Flight Toggle
						String newState = "off";
						if(name.contains("on")){
							player.setItemInHand(getFly("hover"));
							newState = "hover";
							player.setAllowFlight(true);
							player.setFlying(true);
						}else if(name.contains("off")){
							player.setItemInHand(getFly("on"));
							newState = "on";
							player.setAllowFlight(true);
						}else if(name.contains("hover")){
							player.setItemInHand(getFly("off"));
							newState = "off";
							player.setAllowFlight(false);
						}
						//Set Flight
						user.setString(newState);
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("Storm")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(1.5);
							eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.5)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = game.getPlayerList.get(plugin.getIndex(player));
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("Storm"))){
				    	event.setDamage(event.getDamage()/2);
					}
				}
			}catch(Exception e){}
		}
	}

	public ItemStack getWind(){
		ItemStack ball = new ItemStack(Material.SNOW_BALL);
		ItemMeta meta = ball.getItemMeta();
		meta.setDisplayName("§fWind");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right-click to blow players");
		loreList.add("§8Left-click to freeze players");
		meta.setLore(loreList);
		ball.setItemMeta(meta);
		return ball;
	}
	
	public ItemStack getWeather(){
		ItemStack star = new ItemStack(Material.NETHER_STAR);
		ItemMeta meta = star.getItemMeta();
		meta.setDisplayName("§fWeather");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§7Right-click to strike lightning");
		loreList.add("§7Left-click to create a tornado");
		loreList.add("§7It must be raining to do these");
		meta.setLore(loreList);
		star.setItemMeta(meta);
		return star;
	}
	
	public ItemStack getRain(String state){
		ItemStack bucket;
		if(state.equals("on"))
			bucket = new ItemStack(Material.WATER_BUCKET);
		else if(state.equals("off"))
			bucket = new ItemStack(Material.BUCKET);
		else
			return null;
		ItemMeta meta = bucket.getItemMeta();
		meta.setDisplayName("§9Rain - " + state);
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§1Right-click to toggle the rain");
		meta.setLore(loreList);
		bucket.setItemMeta(meta);
		return bucket;
	}
	
	public ItemStack getFly(String state){
		ItemStack feather = new ItemStack(Material.FEATHER);
		ItemMeta meta = feather.getItemMeta();
		if(state.equals("on"))
			meta.setDisplayName("§7Flight - on");
		else if(state.equals("hover"))
			meta.setDisplayName("§7Flight - hover");
		else
			meta.setDisplayName("§7Flight - off");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right-click to toggle flight type");
		meta.setLore(loreList);
		feather.setItemMeta(meta);
		return feather;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					//Stop Flying
					User user = game.getPlayerList.get(plugin.getIndex(player));
					user.setString("off");
		    		player.setAllowFlight(false);
		    		for(ItemStack it : player.getInventory()){
		    			if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().contains("§7Flight"))) {
		    				player.getInventory().remove(it);
		    				player.getInventory().addItem(getFly("off"));
			    		}
		    		}
				}
			}
		}catch(Exception e){}
	}

	/**
	* Spawns a tornado at the given location l.
	*
	* @param plugin
	*            - Plugin instance that spawns the tornado.
	* @param location
	*            - Location to spawn the tornado.
	* @param material
	*            - The base material for the tornado.
	* @param data
	*            - Data for the block.
	* @param direction
	*            - The direction the tornado should move in.
	* @param speed
	*            - How fast it moves in the given direction. Warning! A number greater than 0.3 makes it look weird.
	* @param amount_of_blocks
	*            - The max amount of blocks that can exist in the tornado.
	* @param time
	*            - The amount of ticks the tornado should be alive.
	* @param spew
	*            - Defines if the tornado should remove or throw out any block it picks up.
	* @param explode
	*            - This defines if the tornado should "explode" when it dies. Warning! Right now it only creates a huge mess.
	*/
	public static void spawnTornado(
	        final JavaPlugin plugin,
	        final Location  location,
	        final Material  material,
	        final byte      data,
	        final Vector    direction,
	        final double    speed,
	        final int        amount_of_blocks,
	        final long      time,
	        final boolean    spew,
	        final boolean    explode
	) {
	   
	    class VortexBlock {
	 
	        private Entity entity;
	       
	        public boolean removable = true;
	 
	        private float ticker_vertical = 0.0f;
	        private float ticker_horisontal = (float) (Math.random() * 2 * Math.PI);
	 
	        @SuppressWarnings("deprecation")
	        public VortexBlock(Location l, Material m, byte d) {
	 
	            if (l.getBlock().getType() != Material.AIR) {
	 
	                Block b = l.getBlock();
	                entity = l.getWorld().spawnFallingBlock(l, b.getType(), b.getData());
	 
	                if (b.getType() != Material.WATER)
	                    b.setType(Material.AIR);
	               
	                removable = !spew;
	            }
	            else {
	                entity = l.getWorld().spawnFallingBlock(l, m, d);
	                removable = !explode;
	            }
	           
	            addMetadata();
	        }
	       
	        public VortexBlock(Entity e) {
	            entity    = e;
	            removable = false;
	            addMetadata();
	        }
	       
	        private void addMetadata() {
	            entity.setMetadata("vortex", new FixedMetadataValue(plugin, "protected"));
	        }
	       
	        public void remove() {
	            if(removable) {
	                entity.remove();
	            }
	            entity.removeMetadata("vortex", plugin);
	        }
	 
	        @SuppressWarnings("deprecation")
	        public HashSet<VortexBlock> tick() {
	           
	            double radius    = Math.sin(verticalTicker()) * 2;
	            float  horisontal = horisontalTicker();
	           
	            Vector v = new Vector(radius * Math.cos(horisontal), 0.5D, radius * Math.sin(horisontal));
	           
	            HashSet<VortexBlock> new_blocks = new HashSet<VortexBlock>();
	           
	            // Pick up blocks
	            Block b = entity.getLocation().add(v.clone().normalize()).getBlock();
	            if(b.getType() != Material.AIR) {
	                new_blocks.add(new VortexBlock(b.getLocation(), b.getType(), b.getData()));
	            }
	           
	            // Pick up other entities
	            List<Entity> entities = entity.getNearbyEntities(1.0D, 1.0D, 1.0D);
	            for(Entity e : entities) {
	                if(!e.hasMetadata("vortex")) {
	                    new_blocks.add(new VortexBlock(e));
	                }
	            }
	           
	            setVelocity(v);
	           
	            return new_blocks;
	        }
	 
	        private void setVelocity(Vector v) {
	            entity.setVelocity(v);
	        }
	 
	        private float verticalTicker() {
	            if (ticker_vertical < 1.0f) {
	                ticker_vertical += 0.05f;
	            }
	            return ticker_vertical;
	        }
	 
	        private float horisontalTicker() {
//	                ticker_horisontal = (float) ((ticker_horisontal + 0.8f) % 2*Math.PI);
	            return (ticker_horisontal += 0.8f);
	        }
	    }
	   
	    // Modify the direction vector using the speed argument.
	    if (direction != null) {
	        direction.normalize().multiply(speed);
	    }
	   
	    // This set will contain every block created to make sure the metadata for each and everyone is removed.
	    final HashSet<VortexBlock> clear = new HashSet<VortexBlock>();
	   
	    final int id = new BukkitRunnable() {
	 
	        private ArrayDeque<VortexBlock> blocks = new ArrayDeque<VortexBlock>();
	 
	        public void run() {
	           
	            if (direction != null) {
	                location.add(direction);
	            }
	 
	            // Spawns 10 blocks at the time.
	            for (int i = 0; i < 10; i++) {
	                checkListSize();
	                VortexBlock vb = new VortexBlock(location, material, data);
	                blocks.add(vb);
	                clear.add(vb);
	            }
	           
	            // Make all blocks in the list spin, and pick up any blocks that get in the way.
	            ArrayDeque<VortexBlock> que = new ArrayDeque<VortexBlock>();
	 
	            for (VortexBlock vb : blocks) {
	                HashSet<VortexBlock> new_blocks = vb.tick();
	                for(VortexBlock temp : new_blocks) {
	                    que.add(temp);
	                }
	            }
	           
	            // Add the new blocks
	            for(VortexBlock vb : que) {
	                checkListSize();
	                blocks.add(vb);
	                clear.add(vb);
	            }
	        }
	       
	        // Removes the oldest block if the list goes over the limit.
	        private void checkListSize() {
	            while(blocks.size() >= amount_of_blocks) {
	                VortexBlock vb = blocks.getFirst();
	                vb.remove();
	                blocks.remove(vb);
	                clear.remove(vb);
	            }
	        }
	    }.runTaskTimer(plugin, 5L, 5L).getTaskId();
	 
	    // Stop the "tornado" after the given time.
	    new BukkitRunnable() {
	        public void run() {
	            for(VortexBlock vb : clear) {
	                vb.remove();
	            }
	            plugin.getServer().getScheduler().cancelTask(id);
	        }
	    }.runTaskLater(plugin, time);
	}
}


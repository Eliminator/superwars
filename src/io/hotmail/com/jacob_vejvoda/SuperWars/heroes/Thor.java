package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Thor extends Hero{
	
	public Thor(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onThorMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			try{
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("Thor")) && (game.getStarted == true)){
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_AXE)) && (name.equals("§7Hammer "))){
						player.setVelocity(player.getLocation().getDirection().multiply(1));
					}
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onThorInteract(PlayerInteractEvent event) {
		try{
			if(event.getAction().equals(Action.PHYSICAL))
				return;
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("Thor")) && (game.getStarted == true)){
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_AXE)) && (name.contains("§7Hammer"))){
						if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
							if(player.isSneaking()){
								if(name.equals("§7Hammer")){
									ItemMeta meta = item.getItemMeta();
									meta.setDisplayName("§7Hammer ");
									item.setItemMeta(meta);
									player.setAllowFlight(true);
								}else{
									ItemMeta meta = item.getItemMeta();
									meta.setDisplayName("§7Hammer");
									item.setItemMeta(meta);
									player.setAllowFlight(false);
								}
							}
						}else if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
							if(player.isSneaking()){
				    			if (user.isCooling() == false) {
				    				//Play Smoke Pillar
				    				Location b1 = player.getTargetBlock((Set<Material>)null, 200).getLocation();
						    		for(int i = 1 ; i <= 50 ; i++){
						    			//player.playEffect(b1, Effect.STEP_SOUND, 1);
						    			showSmoke(b1);
						    			b1.setY(b1.getY() +1);
						    		}
									//Set Cool
						    		user.setCooling(true);
						    		//Set Entity Velocity
						    		Location spawn = player.getTargetBlock((Set<Material>)null, 0).getLocation();
						    		Entity myEntity = player.getWorld().spawnEntity(spawn, EntityType.EXPERIENCE_ORB);
						    		double radious = 2.0D;
						    		for(Entity e : myEntity.getNearbyEntities(radious, radious, radious)){
						    		    e.setVelocity(new Vector(0,5.5,0));
						    		}	
						    		myEntity.remove();
						    		//Start Stop Cooling Timer
						    		startCoolTimer(player, "Wind", 30, 0);
				    			}
							}else{
				    			if (user.isCooling("strike") == false) {
									//Strike Lightning
						    		Location strike = player.getTargetBlock((Set<Material>)null, 200).getLocation();
						    		player.getWorld().strikeLightning(strike);
						    		//Create Boom
									Double x = strike.getX();
									Double y = strike.getY();
									Double z = strike.getZ();
									strike.getWorld().createExplosion(x, y, z, 2, true, true);
									//Set Cool
						    		quickCool(player, "strike", 5);
				    			}
							}
						}
					}
				}
			}
		}catch(Exception e){}
	}
	
	public void showSmoke(Location l){
		for(int i = 0; i < 8; i++)
			l.getWorld().playEffect(l, Effect.SMOKE, i);
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("Thor"))){
				    	event.setDamage(event.getDamage()/4);
					}
				}
			}catch(Exception e){}
		}
	}

	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("Thor")) && (game.getStarted == true)){
    					ItemStack aItem = attacker.getItemInHand();
						if((aItem.getType().equals(Material.IRON_AXE)) && (aItem.getItemMeta().getDisplayName().equals("§7Hammer"))){
							event.setDamage(3.5);
						}
					}
    			}
			}
        }catch(Exception e){}
	}
	
	public ItemStack getHammer(){
		ItemStack hammer = new ItemStack(Material.IRON_AXE);
		ItemMeta meta = hammer.getItemMeta();
		meta.setDisplayName("§7Hammer");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to strike lightning");
		loreList.add("§8Sneak and right click to cause powerful wind");
		loreList.add("§8Sneak and left click to toggle the hammers pull");
		meta.setLore(loreList);
		hammer.setItemMeta(meta);
		hammer.addUnsafeEnchantment(Enchantment.KNOCKBACK, 3);
		return hammer;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 2), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 220, 1), true);
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_AXE)) && (name.equals("§7Hammer "))){
						//Stop Flying
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName("§7Hammer");
						item.setItemMeta(meta);
						player.setAllowFlight(false);
					}
				}
			}
		}catch(Exception e){}
	}
    
}
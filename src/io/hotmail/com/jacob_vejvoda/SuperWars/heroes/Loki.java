package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.Disguise;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Loki extends Hero{
	
	public Loki(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		if(plugin.getGame(p) != null){
			final Game game = plugin.getGame(p);
			final User user = game.getPlayerList.get(plugin.getIndex(p));
			if((user.getHero().equals("Loki")) && (game.getStarted == true)){
				try{
					ItemStack it = p.getItemInHand();
					if((it.getType().equals(Material.BLAZE_ROD)) && (it.getItemMeta().getDisplayName().equals("§6Sceptre"))) {
						if((e.getAction().equals(Action.RIGHT_CLICK_AIR)) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
							if(p.isSneaking()){
								//TP
								Location l = p.getTargetBlock((Set<Material>)null, 250).getLocation();
								if(!l.getBlock().getType().equals(Material.AIR)){
									l.setY(l.getY()+1);
									if(l.getBlock().getType().equals(Material.AIR))
										p.teleport(l);
								}
							}else if(!user.isCooling("energy")){
								//Shoot Energy
				    			Location eyeLoc = p.getEyeLocation();
				    			double px = eyeLoc.getX();
				    			double py = eyeLoc.getY();
				    			double pz = eyeLoc.getZ();
				    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
				    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
				    			double x = Math.sin(pitch) * Math.cos(yaw);
				    			double y = Math.sin(pitch) * Math.sin(yaw);
				    			double z = Math.cos(pitch);
				    			//Effect
				    			for (int i = 1 ; i <= 50 ; i++) {
					    			Location loc = new Location(p.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
					    			//ParticleEffects.sendToLocation(ParticleEffects.ENCHANT_CRITS, loc, 0, 0, 0, 0, 10);
					    			plugin.displayParticles("ENCHANT_CRITS", loc, 0, 10);
					    			if(loc.getBlock().getType() != Material.AIR)
					    				i = 50;
				    			}
				    			//Damage
								if (getTarget(p) != null) {
									Entity attacked = getTarget(p);
									if ((attacked instanceof LivingEntity))
										((LivingEntity)attacked).damage((int)Math.round(2.0D * 10));
								}
				    			//Set Cooling
								quickCool(p, "energy", 4);
							}
						}else if((e.getAction().equals(Action.LEFT_CLICK_AIR)) || (e.getAction().equals(Action.LEFT_CLICK_BLOCK))){
							if((p.isSneaking()) && (!user.isCooling("invis"))){
								//Invis
		    					if(plugin.getConfig().getBoolean("useHeroDisguises") == false){
				    				p.getInventory().setHelmet(null);
				    				p.getInventory().setChestplate(null);
				    				p.getInventory().setLeggings(null);
				    				p.getInventory().setBoots(null);
		    					}
			    				//Make Player Invisible
			    				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (20*10), 1), true);
				    			//Set Cooling
								quickCool(p, "invis", 15);
								Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
								    	 try{
											plugin.giveItems(p, user.getHero(), true, -1);
								    	 }catch(Exception x){}
									 }
								}, (20*10));
							}else{
								//Toss
								for(Entity ent : p.getNearbyEntities(5, 5, 5))
									try{
										if(ent instanceof LivingEntity){
											Vector v = p.getLocation().getDirection().normalize().multiply(1);
											v.setY(1);
											ent.setVelocity(v);
											//displayParticles(e.getLocation(), ParticleEffects.TILECRACK.toString(), 5, b.getTypeId());
											//e.getWorld().playEffect(e.getLocation(), Effect.STEP_SOUND,  b.getTypeId());
										}
									}catch(Exception x){}
							}
						}
					}else if((!user.isCooling()) && (it.getType().equals(Material.GOLD_NUGGET)) && (it.getItemMeta().getDisplayName().equals("§6Shape Shifting")) && (plugin.getConfig().getBoolean("useHeroDisguises"))){
						if((e.getAction().equals(Action.RIGHT_CLICK_AIR)) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
							//Shift To Target
							if((getTarget(p) != null) && (getTarget(p) instanceof LivingEntity)){
								LivingEntity target = (LivingEntity) (getTarget(p));
								Disguise dis;
								if(DisguiseAPI.getDisguise(target) != null){
									dis = DisguiseAPI.getDisguise(target);
								}else
									dis = new MobDisguise(DisguiseType.getType(target));
								DisguiseAPI.disguiseToAll(p, dis);
								//Cool
								user.setCooling(true);
								startCoolTimer(p, "Shape Shifting", 40, 0);
								//Undis
								final String hero = user.getHero();
								Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
								    	 try{
								    		 plugin.disguise(p, hero);
								    	 }catch(Exception x){}
									 }
								}, (20*20));
							}
						}else if((e.getAction().equals(Action.LEFT_CLICK_AIR)) || (e.getAction().equals(Action.LEFT_CLICK_BLOCK))){
							//Spawn fake Selfs
							ArrayList<Wolf> pzl = new ArrayList<Wolf>();
							String hero = user.getHero();
							for(int i = 0; i < 3; i++){
								Wolf w = (Wolf) p.getWorld().spawnEntity(p.getLocation(), EntityType.WOLF);
								pzl.add(w);
								w.setMaxHealth(20);
								w.setHealth(20);
								w.setAdult();
								w.setOwner(p);
								plugin.disguise(w, hero);
							}
							//Cool
							user.setCooling(true);
							startCoolTimer(p, "Shape Shifting", 40, 0);
							//Remove
							final ArrayList<Wolf> fpzl = pzl;
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
							    	 try{
							    		for(Entity e : fpzl)
							    			e.remove();
							    	 }catch(Exception x){}
								 }
							}, (20*20));
						}
					}
				}catch(Exception x){}
			}
		}
	}
	
	public ItemStack getStaff(){
		ItemStack stack = new ItemStack(Material.BLAZE_ROD);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName("§6Sceptre");
		meta.setLore(new ArrayList<String>(Arrays.asList("Right-click to shoot energy", "Left-click to toss", "Sneak right-click to TP", "Sneak left-click to go invisable")));
		stack.setItemMeta(meta);
		return stack;
	}
	
	public ItemStack getShift(){
		ItemStack stack = new ItemStack(Material.GOLD_NUGGET);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName("§6Shape Shifting");
		meta.setLore(new ArrayList<String>(Arrays.asList("Right-click to shift to target", "Left-click spawn 3 fake yous")));
		stack.setItemMeta(meta);
		return stack;
	}

	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 2), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
			}
		}catch(Exception e){}
	}
}
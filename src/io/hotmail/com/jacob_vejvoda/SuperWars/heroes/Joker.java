package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import org.bukkit.Bukkit;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Joker extends Hero{
	
	public Joker(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Joker")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_HOE)) && (name.contains("§7Gun"))){
						name = name.replace("§7", "");
						int ammo = Integer.parseInt(name.replaceAll("[\\D]", ""));
						//System.out.println("Ammo: " + ammo);//------------------------DEBUG
						if ((!user.isCooling("gun")) && ammo >= 1) {
							//Set Cool
							quickTickCool(player, "gun", 4);
				    		//Make Gun Smoke
							Location loc = player.getLocation();
							//Location fLoc = player.getEyeLocation();
							//loc.getWorld().playEffect(fLoc, Effect.SMOKE, numDir);
							//Make Gun Sound
							player.playSound(loc, Sound.ENTITY_ARROW_SHOOT, 2F, 1F);
							player.playSound(loc, Sound.ENTITY_BLAZE_HURT, 2F, 50F);
							player.playSound(loc, Sound.UI_BUTTON_CLICK, 1F, 0F);
							//Fire Gun Bullet
							Snowball ball = player.getWorld().spawn(player.getEyeLocation(), Snowball.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(8));
							//Take Gun Ammo
							ItemMeta m = item.getItemMeta();
							m.setDisplayName("§7Gun - <<" + (ammo-1) + ">>");
							item.setItemMeta(m);
						}else if ((!user.isCooling("gun")) && ammo <= 0) {
							//System.out.println("JK reload");//------------------------DEBUG
							//Make Gun Sound
							Location loc = player.getLocation();
							player.playSound(loc, Sound.BLOCK_WOODEN_DOOR_OPEN, 1F, 2F);
							player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 2F);
							//Set Cooling
							quickCool(player, "gun", 3);
							//Reload
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 3F);
										player.playSound(player.getLocation(), Sound.BLOCK_NOTE_SNARE, 1F, 2F);
							    		for(ItemStack item2 : player.getInventory()){
							    			if((item2.getItemMeta() != null) && (item2.getItemMeta().getDisplayName() != null) && (item2.getItemMeta().getDisplayName().equals("§7Gun - <<0>>"))){
												ItemMeta m2 = item2.getItemMeta();
												m2.setDisplayName("§7Gun - <<25>>");
												item2.setItemMeta(m2);
							    			}
							    		}
							    	}catch(Exception e){}
								 }
							}, (3 * 20));
						}
					}else if((item.getType().equals(Material.TNT)) && (name.contains("§cExplosive"))){
						event.setCancelled(true);
						if(user.isCooling() == false){
							//Set Explosive
							//Block b = event.getClickedBlock();
							Entity e = player.getEyeLocation().getWorld().spawnEntity(player.getEyeLocation(), EntityType.PRIMED_TNT);
							e.setVelocity(player.getLocation().getDirection().normalize().multiply(1));
							String uuid = e.getUniqueId().toString();
							//Take
							player.setItemInHand(null);
							//Start Cool
							user.setCooling(true);
							user.setString(uuid);
				    		startCoolTimer(player, "Explosive", 15, 0);
				    		//Give Back
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		player.getInventory().addItem(getExplosive());
							    	}catch(Exception e){}
								 }
							}, (15 * 20));
						}
						player.updateInventory();
					}else if((item.getType().equals(Material.YELLOW_FLOWER)) && (name.contains("§eAcid Spraying Flower")) && (!user.isCooling("acid"))){
						if((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR))){
			    			//Set Cooling
							quickCool(player, "acid", 1);
			    			//Shoot Acid
			    			Location eyeLoc = player.getEyeLocation();
			    			double px = eyeLoc.getX();
			    			double py = eyeLoc.getY();
			    			double pz = eyeLoc.getZ();
			    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
			    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
			    			double x = Math.sin(pitch) * Math.cos(yaw);
			    			double y = Math.sin(pitch) * Math.sin(yaw);
			    			double z = Math.cos(pitch);
			    			for (int j = 1 ; j <= 10 ; j++) {
								if(getTarget(player) != null){
									Entity attacked = getTarget(player);
									if((attacked instanceof LivingEntity) && (player.getNearbyEntities(5, 5, 5).contains(attacked))) {
										//((LivingEntity)attacked).damage((int)Math.round(2.0D * 2));
						    			//ParticleEffects.sendToLocation(ParticleEffects.HAPPY, attacked.getLocation(), 0, 0, 0, 5, 1);
										plugin.displayParticles("HAPPY", attacked.getLocation(), 5, 1);
										((LivingEntity)attacked).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 160, 1), true);
									}
								}
			    			}
			    			for (int i = 1 ; i <= 5 ; i++) {
				    			Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
				    			//ParticleEffects.sendToLocation(ParticleEffects.HAPPY, loc, 0, 0, 0, 5, 1);
				    			plugin.displayParticles("HAPPY", loc, 5, 1);
				    			if(loc.getBlock().getType() != Material.AIR)
				    				i = 50;
				    		}
						}
					}else if((item.getType().equals(Material.PAPER)) && (name.contains("§7Razor-sharp Playing Card"))){
						if((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR))){
							//Throw Card
							final Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(2));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Shield");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Card
							int amount = player.getItemInHand().getAmount() - 1;
							if(amount <= 0){
								player.setItemInHand(null);
							}else{
								player.getItemInHand().setAmount(amount);
							}
							//Damage
							//System.out.println("Target = " + getTarget(player));
							if (getTarget(player) != null) {
								Entity attacked = getTarget(player);
								if ((attacked instanceof LivingEntity)) {
									((LivingEntity)attacked).damage((int)Math.round(2.0D * 10));
								}
							}
							//Remove Card
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		dropped.remove();
							    	}catch(Exception e){}
								 }
							}, (20));
						}
					}else if((item.getType().equals(Material.ENDER_PEARL)) && (name.contains("§9Venom Gas Grenade"))){
						event.setCancelled(true);
						if((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR))){
							//Throw Grenade
							Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(1));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Grenade");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Item
							int amount = player.getItemInHand().getAmount() - 1;
							if(amount <= 0){
								player.setItemInHand(null);
							}else{
								player.getItemInHand().setAmount(amount);
							}
							//Time Boom
							explode(5, dropped);
						}
						player.updateInventory();
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onExplosionPrime(ExplosionPrimeEvent e) {
		if(e.getEntity() instanceof TNTPrimed){
			final TNTPrimed tnt = (TNTPrimed) e.getEntity();
			for(Game g : plugin.gameList)
				for(final User u : g.getPlayerList)
					if(u.getString().equals(tnt.getUniqueId().toString())){
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								try{
									Location l = tnt.getLocation();
									tnt.getWorld().createExplosion(l.getX(), l.getY(), l.getZ(), 5, true, true);
									//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, l, 0, 0, 0, 1, 100);
									//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, l, 0, 0, 0, (float)0.2, 100);
									plugin.displayParticles("LARGE_SMOKE", l, 1, 100);
									plugin.displayParticles("LARGE_SMOKE", l, 0.2, 100);
								}catch(Exception e1){e1.printStackTrace();}
							}
						},(tnt.getFuseTicks()));
					}
		}	
	}
	
	public void explode(int seconds, final Item gren){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					for(Entity e : gren.getNearbyEntities(6, 6, 6)){
						if (e instanceof LivingEntity) {
							((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (8 * 20), 1), true);
							((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (20 * 20), 1), true);
						}
					}
					//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, gren.getLocation(), 0, 0, 0, (float)0.4, 500);
					//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, gren.getLocation(), 0, 0, 0, (float)0.2, 250);
					plugin.displayParticles("CLOUD", gren.getLocation(), 0.4, 500);
					plugin.displayParticles("CLOUD", gren.getLocation(), 0.2, 250);
					gren.remove();
				}catch(Exception e1){e1.printStackTrace();}
			}
		}, (20 * seconds));
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("Joker")) && (game.getStarted == true))
    					event.setDamage(1);
    			}
    		}
        }catch(Exception e){}
	}
	
	public ItemStack getGun(){
		ItemStack gun = new ItemStack(Material.IRON_HOE);
		ItemMeta meta = gun.getItemMeta();
		meta.setDisplayName("§7Gun - <<25>>");
		gun.setItemMeta(meta);
		return gun;
	}
	
	public ItemStack getFlower(){
		ItemStack flower = new ItemStack(Material.YELLOW_FLOWER);
		ItemMeta meta = flower.getItemMeta();
		meta.setDisplayName("§eAcid Spraying Flower");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§6Left click to spray acid");
		meta.setLore(loreList);
		flower.setItemMeta(meta);
		return flower;
	}
	
	public ItemStack getCards(int amount){
		ItemStack card = new ItemStack(Material.PAPER, amount);
		ItemMeta meta = card.getItemMeta();
		meta.setDisplayName("§7Razor-sharp Playing Card");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to throw a card");
		loreList.add("§8You have a limited amount");
		meta.setLore(loreList);
		card.setItemMeta(meta);
		return card;
	}
	
	public ItemStack getGrenade(){
		ItemStack gren = new ItemStack(Material.ENDER_PEARL, 6);
		ItemMeta meta = gren.getItemMeta();
		meta.setDisplayName("§9Venom Gas Grenade");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§1Right click to throw a grenade");
		loreList.add("§1You have a limited amount");
		meta.setLore(loreList);
		gren.setItemMeta(meta);
		return gren;
	}
	
	public ItemStack getExplosive(){
		ItemStack gren = new ItemStack(Material.TNT);
		ItemMeta meta = gren.getItemMeta();
		meta.setDisplayName("§cExplosive");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§4Right click to set an explosive");
		meta.setLore(loreList);
		gren.setItemMeta(meta);
		return gren;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
		}
	}
    
}
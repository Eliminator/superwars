package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class Hero implements Listener{
	
	public static SuperWars plugin;
	@SuppressWarnings("static-access")
	public Hero(SuperWars instance) {
		this.plugin = instance;
	}
	
	public static Entity getCart(Player p){
		for(Entity e : p.getNearbyEntities(3, 3, 3))
			if(e.getPassenger() != null && e.getPassenger().equals(p))
				return e;
		return null;
	}
	
    public int rand(int min, int max) {
    	int r = min + (int) (Math.random() * ((1 + max) - min));
        return r;
    }
    
    public double rand(double mind, double maxd) {
    	int min = (int)(mind*10.0);
    	int max = (int)(maxd*10.0);
    	int r = rand(min, max);
    	double rd = r/10.0;
    	return rd;
    }
	
	private static List<Chunk> getNearbyChunks(Location l, int range) {
        List<Chunk> chunkList = new ArrayList<Chunk>();
        World world = l.getWorld();
        int chunks = range / 16 + 1;
 
        Chunk chunk;
        for (int x = l.getChunk().getX() - chunks; x < l.getChunk().getX() + chunks; x++) {
            for (int z = l.getChunk().getZ() - chunks; z < l.getChunk().getZ() + chunks; z++) {
                chunk = world.getChunkAt(x, z);
                if (chunk != null && chunk.isLoaded()) {
                   chunkList.add(chunk);
                }
            }
        }
 
        return chunkList;
    }
 
    private static List<Entity> getEntitiesInNearbyChunks(Location l, int range, List<EntityType> entityTypes) {
        List<Entity> entities = new ArrayList<Entity>();
 
        for(Chunk chunk : getNearbyChunks(l, range)) {
            if (entityTypes == null /*&& entityTypes.size() > 0*/) {
                entities.addAll(Arrays.asList(chunk.getEntities()));
            } else {
                for (Entity e : chunk.getEntities()) {
                    if (entityTypes.contains(e.getType())) {
                        entities.add(e);
                    }
                }
            }
        }
 
        return entities;
    }
 
    public static List<Entity> getNearbyEntities(Location l, float range, List<EntityType> entityTypes) {
        List<Entity> entities = new ArrayList<Entity>();
        for (Entity e : getEntitiesInNearbyChunks(l, (int) range, entityTypes)) {
            if (e.getLocation().distance(l) <= range) {
                entities.add(e);
            }
        }
 
        return entities;
    }
	
	public static Entity getTarget(final Player player) {
		 
        BlockIterator iterator = new BlockIterator(player.getWorld(), player
                .getLocation().toVector(), player.getEyeLocation()
                .getDirection(), 0, 100);
        Entity target = null;
        while (iterator.hasNext()) {
            Block item = iterator.next();
            for (Entity entity : player.getNearbyEntities(100, 100, 100)) {
            	if((entity instanceof LivingEntity) && (!(entity.getType().equals(EntityType.BAT)))){
	                int acc = 2;
	                for (int x = -acc; x < acc; x++)
	                    for (int z = -acc; z < acc; z++)
	                        for (int y = -acc; y < acc; y++)
	                            if (entity.getLocation().getBlock()
	                                    .getRelative(x, y, z).equals(item)) {
	                                return target = entity;
	                            }
            	}
            }
        }
        return target;
    }
	
	public ArrayList<Player> getNearPlayers(Location l, int r){
    	Entity e = l.getWorld().spawnEntity(l, EntityType.EXPERIENCE_ORB);
    	ArrayList<Player> pList = new ArrayList<Player>();
		for(Entity ent : e.getNearbyEntities(r, r, r))
			if(ent instanceof Player)
				pList.add((Player)ent);
    	e.remove();
    	return pList;
	}
	
	public static ArrayList<Player> getNearPlayers(Entity e, int r, boolean addPlayer){
    	ArrayList<Player> pList = new ArrayList<Player>();
		for(Entity ent : e.getNearbyEntities(r, r, r))
			if(ent instanceof Player)
				pList.add((Player)ent);
		if((addPlayer) || (e instanceof Player))
			pList.add((Player)e);
    	return pList;
	}
	
	public boolean nearWall(Location l){
		Location u = l.clone();
		u.setY(u.getY() - 1);
		//System.out.println("NW: " + u.getBlock().getType());//------------------------DEBUG
		if(u.getBlock().getType().equals(Material.AIR))
			for(int x = l.getBlockX()-1;x <= l.getBlockX()+1;x++)
				for(int z = l.getBlockZ()-1;z <= l.getBlockZ()+1;z++){
					Location newLoc = new Location(l.getWorld(), x, l.getY(), z);
					//System.out.println("Block: " + newLoc.getBlock().getType());//------------------------DEBUG
					if(!newLoc.getBlock().getType().equals(Material.AIR))
						return true;
				}
		return false;
	}
	
    public static String getDirection(Player player) {
        double rot = (player.getLocation().getYaw() - 90) % 360;
        if (rot < 0) {
            rot += 360.0;
        }
        return getDirection(rot);
    }

	public void setAir(final Block b, int time){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				b.setType(Material.AIR);
			}
		}, (time * 20));
	}
	
	public void setNearToAir(Block b, Material m){
		if(b.getType().equals(m)){
			b.setType(Material.AIR);
			return;
		}
		Location l = b.getLocation();
		for(double x = l.getX()-1; x <l.getX()+1; x++)
			for(double y = l.getY()-1; y <l.getY()+1; y++)
				for(double z = l.getZ()-1; z <l.getZ()+1; z++){
					//System.out.println(new Location(l.getWorld(), x, y, z).getBlock().getType() + " == " + (new Location(l.getWorld(), x, y, z).getBlock().getType().equals(m)));
					if(new Location(l.getWorld(), x, y, z).getBlock().getType().equals(m)){
						new Location(l.getWorld(), x, y, z).getBlock().setType(Material.AIR);
						return;
					}
				}
	}
	
	public void entityCauseDamage(final Entity source, final FallingBlock e, final int damage){
		if(!e.isOnGround()){
			for(Entity de : e.getNearbyEntities(1, 1, 1)){
				if ((de instanceof LivingEntity) && (!de.equals(source))) {
					((LivingEntity)de).damage((int)Math.round(2.0D * damage));
				}
			}
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
			    	 entityCauseDamage(source, e, damage);
				 }
			}, (2));
		}
	}
	
	public static List<Block> getSphere(Block block1, int radius){
        List<Block> blocks = new LinkedList<Block>();
        double xi = block1.getLocation().getX() + 0.5;
        double yi = block1.getLocation().getY() + 0.5;
        double zi = block1.getLocation().getZ() + 0.5;

        for(int v1 = 0; v1 <= 90; v1++){
            double y = (Math.sin((Math.PI / 180) * v1)) * radius;
            double r = (Math.cos((Math.PI / 180) * v1)) * radius;

            if(v1 == 90){
                r = 0;
            }
            for(int v2 = 0; v2 <= 90; v2++){
                double x = (Math.sin((Math.PI / 180) * v2)) * r;
                double z = (Math.cos((Math.PI / 180) * v2)) * r;

                if(v2 == 90){
                    z = 0;
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi + y), (int) (zi + z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi + y), (int) (zi + z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi + y), (int) (zi + z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi + y), (int) (zi + z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi - y), (int) (zi + z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi - y), (int) (zi + z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi + y), (int) (zi - z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi + y), (int) (zi - z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi - y), (int) (zi - z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi - y), (int) (zi - z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi - y), (int) (zi - z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi + x), (int) (yi - y), (int) (zi - z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi + y), (int) (zi - z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi + y), (int) (zi - z)));
                }
                if(!blocks.contains(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi - y), (int) (zi + z)))){
                    blocks.add(block1.getWorld().getBlockAt((int) (xi - x), (int) (yi - y), (int) (zi + z)));
                }
            }
        }
        return blocks;
    }
	
	public static List<Location> circle (Player player, Location loc, Integer r, Integer h, Boolean hollow, Boolean sphere, int plus_y) {
        List<Location> circleblocks = new ArrayList<Location>();
        int cx = loc.getBlockX();
        int cy = loc.getBlockY();
        int cz = loc.getBlockZ();
        for (int x = cx - r; x <= cx +r; x++)
            for (int z = cz - r; z <= cz +r; z++)
                for (int y = (sphere ? cy - r : cy); y < (sphere ? cy + r : cy + h); y++) {
                    double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere ? (cy - y) * (cy - y) : 0);
                    if (dist < r*r && !(hollow && dist < (r-1)*(r-1))) {
                        Location l = new Location(loc.getWorld(), x, y + plus_y, z);
                        	circleblocks.add(l);
                        }
                    }
     
        return circleblocks;
    }
    
    private static String getDirection(double rot) {
        if (0 <= rot && rot < 22.5) {
            return "North";
        } else if (22.5 <= rot && rot < 67.5) {
            return "Northeast";
        } else if (67.5 <= rot && rot < 112.5) {
            return "East";
        } else if (112.5 <= rot && rot < 157.5) {
            return "Southeast";
        } else if (157.5 <= rot && rot < 202.5) {
            return "South";
        } else if (202.5 <= rot && rot < 247.5) {
            return "Southwest";
        } else if (247.5 <= rot && rot < 292.5) {
            return "West";
        } else if (292.5 <= rot && rot < 337.5) {
            return "Northwest";
        } else if (337.5 <= rot && rot < 360.0) {
            return "North";
        } else {
            return null;
        }
    }

	
	public void hookPull(Entity e, Location loc){
	    Location entityLoc = e.getLocation();

	    entityLoc.setY(entityLoc.getY() + 0.5D);
	    e.teleport(entityLoc);

	    double g = -0.08D;
	    double d = loc.distance(entityLoc);
	    double t = d;
	    double v_x = (1.0D + 0.07000000000000001D * t) * (loc.getX() - entityLoc.getX()) / t;
	    double v_y = (1.0D + 0.03D * t) * (loc.getY() - entityLoc.getY()) / t - 0.5D * g * t;
	    double v_z = (1.0D + 0.07000000000000001D * t) * (loc.getZ() - entityLoc.getZ()) / t;

	    Vector v = e.getVelocity();
	    v.setX(v_x);
	    v.setY(v_y);
	    v.setZ(v_z);
	    e.setVelocity(v);

	    //addNoFall(e, 100);
	}
	
	public void quickCool(Player p, final String s, int time){
		Game g = plugin.getGame(p);
		final User u = g.getPlayerList.get(plugin.getIndex(p));
		//Cool
		u.setCooling(s);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		     public void run() {
		    	 try{
		    		 u.stopCooling(s);
		    	 }catch(Exception x2){}
			 }
		}, (20*time));
	}
	
	public void quickTickCool(Player p, final String s, int time){
		Game g = plugin.getGame(p);
		final User u = g.getPlayerList.get(plugin.getIndex(p));
		//Cool
		u.setCooling(s);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
		     public void run() {
		    	 try{
		    		 u.stopCooling(s);
		    	 }catch(Exception x2){}
			 }
		}, (time));
	}
    
	public void startCoolTimer(final Player player, final String ability, final int startSeconds, int passedSeconds){
		if(passedSeconds <= 0)
			if(plugin.langFile.getString("usePower") != null)
				player.getPlayer().sendMessage(plugin.langFile.getString("usePower").replace("&", "§").replace("<player>", player.getDisplayName()).replace("<power>", ability));
		if(plugin.getGame(player) == null)
			return;
		if (plugin.getConfig().getBoolean("enableBossBar") == true){
			//Set Boss Bar Name
			String tittle = ability + " Ability";
            //float percent = 100.0F;
            //--------------
            BossBar bar;
    		if(!plugin.bossBars.containsKey(player)){
    			BarColor bc = BarColor.valueOf(plugin.getConfig().getString("bossBarSettings.defaultColor"));
    			BarStyle bs = BarStyle.valueOf(plugin.getConfig().getString("bossBarSettings.defaultStyle"));
    			bar = Bukkit.createBossBar(tittle, bc, bs, BarFlag.CREATE_FOG);
    			bar.setVisible(true);
    			plugin.bossBars.put(player, bar);
    			bar.addPlayer(player);
    		}else
    			bar = plugin.bossBars.get(player);
            //--------------
			//Set Boss Bar Level
        	float time = (float) startSeconds-passedSeconds;
        	float maxTime = (float) startSeconds;
        	float setTime = (time * 100.0f) / maxTime;
        	bar.setProgress(setTime/100.0f);
		}
		if(passedSeconds != startSeconds){
			final int newPassedSeconds = passedSeconds + 1;
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					startCoolTimer(player, ability, startSeconds, newPassedSeconds);
				}
			},(20));
		}else{
	    	try{
	    		Game game2 = plugin.getGame(player);
	    		User uc = game2.getPlayerList.get(plugin.getIndex(player));
	    		uc.setCooling(false);
		    	if(plugin.langFile.getString("powerAvailable") != null)
					player.getPlayer().sendMessage(plugin.langFile.getString("powerAvailable").replace("&", "§").replace("<player>", player.getDisplayName()).replace("<power>", ability));
				if (plugin.getConfig().getBoolean("enableBossBar") == true){
	            	plugin.clearBossBar(player);
	            }
	    	}catch(Exception e){}
		}
	}
}

class distanceBlock {
	
	public double distance;
	public Location location;
	
	distanceBlock (double d, Location l) {
		
		distance = d;
		location = l;

	}
	
	public String toString () {
		
		return "Distance: " + distance + " Location: " + location;
		
	}
	
}

class ArrowHomingTask extends BukkitRunnable{
  //private static final double MaxRotationAngle = 0.12D;
  //private static final double TargetSpeed = 1.4D;
	Arrow arrow;
  LivingEntity target;

  public ArrowHomingTask(Arrow arrow, LivingEntity target, Plugin plugin)
  {
    this.arrow = arrow;
    this.target = target;
    runTaskTimer(plugin, 1L, 1L);
  }

  public void run()
  {
	  try{
	    double speed = this.arrow.getVelocity().length();
	    if ((this.arrow.isOnGround()) || (this.arrow.isDead()) || (this.target.isDead())) {
	      cancel();
	      return;
	    }
	
	    Vector toTarget = this.target.getLocation().clone().add(new Vector(0.0D, 0.5D, 0.0D)).subtract(this.arrow.getLocation()).toVector();
	
	    Vector dirVelocity = this.arrow.getVelocity().clone().normalize();
	    Vector dirToTarget = toTarget.clone().normalize();
	    double angle = dirVelocity.angle(dirToTarget);
	
	    double newSpeed = 0.9D * speed + 0.14D;
	
	//    if (((this.target instanceof Player)) && (this.arrow.getLocation().distance(this.target.getLocation()) < 8.0D)) {
	//      Player player = (Player)this.target;
	//      if (player.isBlocking())
	//        newSpeed = speed * 0.6D;
	//    }
	    Vector newVelocity;
	    //Vector newVelocity;
	    if (angle < 0.12D) {
	      newVelocity = dirVelocity.clone().multiply(newSpeed);
	    } else {
	      Vector newDir = dirVelocity.clone().multiply((angle - 0.12D) / angle).add(dirToTarget.clone().multiply(0.12D / angle));
	      newDir.normalize();
	      newVelocity = newDir.clone().multiply(newSpeed);
	    }
	
	    this.arrow.setVelocity(newVelocity.add(new Vector(0.0D, 0.03D, 0.0D)));
	    //this.arrow.getWorld().playEffect(this.arrow.getLocation(), Effect.SMOKE, 0);
	  }catch(Exception e){}
  }
}

class HomingTask extends BukkitRunnable{
  //private static final double MaxRotationAngle = 0.12D;
  //private static final double TargetSpeed = 1.4D;
  Snowball arrow;
  LivingEntity target;

  public HomingTask(Snowball arrow, LivingEntity target, Plugin plugin)
  {
    this.arrow = arrow;
    this.target = target;
    runTaskTimer(plugin, 1L, 1L);
  }

  public void run()
  {
	  try{
	    double speed = this.arrow.getVelocity().length();
	    if ((this.arrow.isOnGround()) || (this.arrow.isDead()) || (this.target.isDead())) {
	      cancel();
	      return;
	    }
	
	    Vector toTarget = this.target.getLocation().clone().add(new Vector(0.0D, 0.5D, 0.0D)).subtract(this.arrow.getLocation()).toVector();
	
	    Vector dirVelocity = this.arrow.getVelocity().clone().normalize();
	    Vector dirToTarget = toTarget.clone().normalize();
	    double angle = dirVelocity.angle(dirToTarget);
	
	    double newSpeed = 0.9D * speed + 0.14D;
	
	//    if (((this.target instanceof Player)) && (this.arrow.getLocation().distance(this.target.getLocation()) < 8.0D)) {
	//      Player player = (Player)this.target;
	//      if (player.isBlocking())
	//        newSpeed = speed * 0.6D;
	//    }
	    Vector newVelocity;
	    //Vector newVelocity;
	    if (angle < 0.12D) {
	      newVelocity = dirVelocity.clone().multiply(newSpeed);
	    } else {
	      Vector newDir = dirVelocity.clone().multiply((angle - 0.12D) / angle).add(dirToTarget.clone().multiply(0.12D / angle));
	      newDir.normalize();
	      newVelocity = newDir.clone().multiply(newSpeed);
	    }
	
	    this.arrow.setVelocity(newVelocity.add(new Vector(0.0D, 0.03D, 0.0D)));
	    //this.arrow.getWorld().playEffect(this.arrow.getLocation(), Effect.SMOKE, 0);
	  }catch(Exception e){}
  }
}
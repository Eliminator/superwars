package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("deprecation")
public class CaptainAmerica extends Hero{	
	
	public CaptainAmerica(SuperWars instance) {
		super(instance);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void onCaptianMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			String hero = plugin.getHero(player);
			if((hero.equals("CaptainAmerica")) && (game.getStarted == true)){
				try{
					Location locF = player.getEyeLocation().toVector().add(player.getLocation().getDirection().multiply(1)).toLocation(player.getWorld());
					Location locB = player.getLocation().toVector().add(player.getLocation().getDirection().multiply(1)).toLocation(player.getWorld());	    		
					if ((player.isSprinting()) && (!plugin.transBlocks.contains(locF.getBlock().getTypeId())) && (!plugin.transBlocks.contains(locB.getBlock().getTypeId()))) {
			        	//System.out.println("POUND1");
						User userSprinter = game.getPlayerList.get(plugin.getIndex(player));
						if (userSprinter.isCooling("pound") == false) {
							player.setVelocity(player.getLocation().getDirection().multiply(2));
							player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 10), true);
							locF.getWorld().createExplosion(locF, 2);
							locB.getWorld().createExplosion(locB, 2);
				    		//System.out.println("POUND2");
							quickCool(player, "pound", 5);
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onCaptianInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("CaptainAmerica")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_HOE)) && (name.contains("§9Gun"))){
						name = name.replace("§9", "");
						int ammo = Integer.parseInt(name.replaceAll("[\\D]", ""));
						//System.out.println("Ammo: " + ammo);//------------------------DEBUG
						if ((user.isCooling("gun") == false) && ammo >= 1) {
							//Set Cool
				    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, true, user.getJumping, user.getCooling, user.getString);
				    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
							quickCool(player, "gun", 1);
				    		//Make Gun Smoke
							Location loc = player.getLocation();
							Location fLoc = player.getEyeLocation();
							String direction = getDirection(player);
							int numDir = 0;
							if(direction.equals("Southeast")){
								//numDir = 0;
								numDir = 2;
							}else if(direction.equals("South")){
								//numDir = 1;
								numDir = 5;
							}else if(direction.equals("Southwest")){
								//numDir = 2;
								numDir = 8;
							}else if(direction.equals("East")){
								//numDir = 3;
								numDir = 1;
							}else if(direction.equals("West")){
								//numDir = 5;
								numDir = 7;
							}else if(direction.equals("Northeast")){
								//numDir = 6;
								numDir = 0;
							}else if(direction.equals("North")){
								//numDir = 7;
								numDir = 3;
							}else if(direction.equals("Northwest")){
								//numDir = 8;
								numDir = 6;
							}
							loc.getWorld().playEffect(fLoc, Effect.SMOKE, numDir);
							//Make Gun Sound
							player.playSound(loc, Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 2F, 1F);
							player.playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 2F, 50F);
							player.playSound(loc, Sound.ENTITY_ITEM_BREAK, 1F, 0F);
							//Fire Gun Bullet
							Snowball ball = player.getWorld().spawn(player.getEyeLocation(), Snowball.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(8));
							//Take Gun Ammo
							ItemMeta m = item.getItemMeta();
							m.setDisplayName("§9Gun - <<" + (ammo-1) + ">>");
							item.setItemMeta(m);
						}else if ((user.isCooling("gun") == false) && ammo <= 0) {
							//Make Gun Sound
							Location loc = player.getLocation();
							player.playSound(loc, Sound.BLOCK_WOODEN_DOOR_OPEN, 1F, 2F);
							player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 2F);
							//Set Cooling
							user.setCooling("gun");
							//Reload
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 3F);
										player.playSound(player.getLocation(), Sound.BLOCK_NOTE_SNARE, 1F, 2F);
							    		User user2 = game.getPlayerList.get(plugin.getIndex(player));
							    		user2.stopCooling("gun");
							    		for(ItemStack item2 : player.getInventory()){
							    			if((item2.getItemMeta() != null) && (item2.getItemMeta().getDisplayName() != null) && (item2.getItemMeta().getDisplayName().equals("§9Gun - <<0>>"))){
												ItemMeta m2 = item2.getItemMeta();
												m2.setDisplayName("§9Gun - <<8>>");
												item2.setItemMeta(m2);
							    			}
							    		}
							    	}catch(Exception e){}
								 }
							}, (3 * 20));
						}
					}else if((item.getType().equals(Material.IRON_SWORD)) && (name.contains("§9Captain America's Shield"))){
						if((user.isCooling() == false) && (event.getAction().equals(Action.LEFT_CLICK_AIR)) && (player.isSneaking())){
							//Start Cool
				    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
				    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
							user.setCooling(true);
				    		startCoolTimer(player, "Shield Throw", 20, 0);
							//Throw Item
							final Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(2));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Shield");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Shield
							player.setItemInHand(null);
							//Damage
							//System.out.println("Target = " + getTarget(player));
							if (getTarget(player) != null) {
								Entity attacked = getTarget(player);
								if ((attacked instanceof LivingEntity)) {
									((LivingEntity)attacked).damage((int)Math.round(2.0D * 10));
								}
							}
							//Give Shield Back
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		Location ex = dropped.getLocation();
							    		dropped.remove();
							    		ex.getWorld().createExplosion(ex, 2);
										player.playSound(player.getLocation(), Sound.BLOCK_CLOTH_BREAK, 1F, 2F);
										player.getInventory().addItem(getShield());
							    	}catch(Exception e){}
								 }
							}, (1 * 20));
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("CaptainAmerica")) && (game.getStarted == true))
    					event.setDamage(6);
    			}
    		}
    		if(eAttacker instanceof Player){
    			//System.out.println("PU1");//------------------------DEBUG
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				//System.out.println("PU2");//------------------------DEBUG
    				if((aHero.equals("CaptainAmerica")) && (game.getStarted == true)){
						ItemStack aItem = attacker.getItemInHand();
						//System.out.println("PU3");//------------------------DEBUG
						if((aItem.getType().equals(Material.IRON_SWORD)) && (aItem.getItemMeta().getDisplayName().equals("§9Captain America's Shield"))){
							//System.out.println("PU4");//------------------------DEBUG
							event.setDamage(3.5);
							//Vector v = eVictim.getVelocity();
							eVictim.setVelocity((attacker.getLocation().getDirection().multiply(1.5)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = game.getPlayerList.get(plugin.getIndex(player));
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("CaptainAmerica"))){
				    	event.setDamage(event.getDamage()/4);
					}
    				if((user.getHero().equals("CaptainAmerica")) && (game.getStarted == true)){
						ItemStack vItem = player.getItemInHand();
						if((vItem.getType().equals(Material.IRON_SWORD)) && (vItem.getItemMeta().getDisplayName().equals("§9Captain America's Shield"))){
							if(player.isBlocking()){
								//If attacker in front
								event.setDamage(0);
								event.setCancelled(true);
							}
						}
					}
				}
			}catch(Exception e){}
		}
	}

    public static String getDirection(Player player) {
        double rot = (player.getLocation().getYaw() - 90) % 360;
        if (rot < 0) {
            rot += 360.0;
        }
        return getDirection(rot);
    }

    private static String getDirection(double rot) {
        if (0 <= rot && rot < 22.5) {
            return "North";
        } else if (22.5 <= rot && rot < 67.5) {
            return "Northeast";
        } else if (67.5 <= rot && rot < 112.5) {
            return "East";
        } else if (112.5 <= rot && rot < 157.5) {
            return "Southeast";
        } else if (157.5 <= rot && rot < 202.5) {
            return "South";
        } else if (202.5 <= rot && rot < 247.5) {
            return "Southwest";
        } else if (247.5 <= rot && rot < 292.5) {
            return "West";
        } else if (292.5 <= rot && rot < 337.5) {
            return "Northwest";
        } else if (337.5 <= rot && rot < 360.0) {
            return "North";
        } else {
            return null;
        }
    }
	
	public ItemStack getGun(){
		ItemStack gun = new ItemStack(Material.IRON_HOE);
		ItemMeta meta = gun.getItemMeta();
		meta.setDisplayName("§9Gun - <<8>>");
		gun.setItemMeta(meta);
		return gun;
	}
	
	public ItemStack getShield(){
		ItemStack shield = new ItemStack(Material.IRON_SWORD);
		ItemMeta meta = shield.getItemMeta();
		meta.setDisplayName("§9Captain America's Shield");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Sneak-left click to throw shield");
		meta.setLore(loreList);
		shield.setItemMeta(meta);
		return shield;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
		}
	}
    
}
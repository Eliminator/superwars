package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("deprecation")
public class AquaMan extends Hero{
	
	public AquaMan(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("AquaMan")) && (game.getStarted == true)){
				Block b = player.getLocation().getBlock();
				if(user.isJumping()){
					if((b.getType().equals(Material.WATER)) || (b.getType().equals(Material.STATIONARY_WATER))){
						//player.setVelocity(player.getVelocity().multiply(1.5));
						player.setVelocity(player.getLocation().getDirection().multiply(0.6));
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		//System.out.println("Aq1");//------------------------DEBUG
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("AquaMan")) && (game.getStarted == true)){
				try{
					ItemStack it = player.getItemInHand();
					//System.out.println("Aq2 " + it.getType());//------------------------DEBUG
					//System.out.println(it.getItemMeta().getDisplayName() +  " == name");//------------------------DEBUG
		    		if((it.getType().equals(Material.COOKED_FISH)) && (it.getItemMeta().getDisplayName().equals("§bUse to start swimming"))) {
		    			//System.out.println("kookd1");//------------------------DEBUG
		    			event.setCancelled(true);
	    				ItemStack flyTog = new ItemStack(Material.RAW_FISH, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				//System.out.println("kookd2");//------------------------DEBUG
	    				meta.setDisplayName("§bUse to stop swimming");
	    				flyTog.setItemMeta(meta);
	    				//System.out.println("kookd3");//------------------------DEBUG
	    				player.setItemInHand(null);
	    				player.setItemInHand(flyTog);
	    				//System.out.println("kookd4");//------------------------DEBUG
			    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero(), user.getLives, user.getSneaking, true, user.getCooling, user.getString);
			    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
	    				user.setJumping(true);
			    		//System.out.println("kookd5");//------------------------DEBUG
			    		player.updateInventory();
		    		}else if((it.getType().equals(Material.RAW_FISH)) && (it.getItemMeta().getDisplayName().equals("§bUse to stop swimming"))) {
		    			//System.out.println("rwa");//------------------------DEBUG
		    			event.setCancelled(true);
	    				ItemStack flyTog = new ItemStack(Material.COOKED_FISH, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§bUse to start swimming");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
			    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero(), user.getLives, user.getSneaking, false, user.getCooling, user.getString);
			    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
	    				user.setJumping(false);
			    		player.updateInventory();
		    		}else if((it.getType().equals(Material.INK_SACK)) && (it.getItemMeta().getDisplayName().contains("§bWater"))) {
		    			//System.out.println(player.isSneaking() + " Aq3: " + event.getAction());//------------------------DEBUG
		    			if ((player.isSneaking() == true) && (event.getAction().equals(Action.LEFT_CLICK_AIR)) || (player.isSneaking() == true) && (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
		    				//System.out.println("Squid Attack");//------------------------DEBUG
		    				//Squid Attack
		    				if((!user.isCooling("squid")) && (getTarget(player) != null)) {
								Entity attacked = getTarget(player);
								//System.out.println("player in " + attacked.getLocation().getBlock().getType());//------------------------DEBUG
								if((attacked.getLocation().getBlock().getType().equals(Material.STATIONARY_WATER)) || (attacked.getLocation().getBlock().getType().equals(Material.WATER))){
									boolean tookWater = false;
									for(ItemStack w : player.getInventory()){
										if(w != null){
											if((tookWater == false) && (w.getType().equals(Material.WATER))){
												tookWater = true;
												int a = w.getAmount()-5;
												if(a <= 0){
													player.getInventory().remove(w);
												}else{
													w.setAmount(a);
												}
											}
										}
									}
									if(tookWater == true){
										final Entity squid = player.getWorld().spawnEntity(attacked.getLocation(), EntityType.SQUID);
										attacked.setPassenger(squid);
										((LivingEntity) squid).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 1000, 1), true);
										((LivingEntity) attacked).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 100, 1), true);
						    			//Set Cooling
										quickCool(player, "squid", 2);
									}
								}else
									player.sendMessage("§2SuperWars: §cYour target must be in water!");
		    				}
		    			}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
		    				if(user.isCooling("spray") == false){
			    				//System.out.println("Spray");//------------------------DEBUG
			    				//Spray
								//Remove Water
								boolean tookWater = false;
								for(ItemStack w : player.getInventory()){
									if(w != null){
										if((tookWater == false) && (w.getType().equals(Material.WATER))){
											tookWater = true;
											int a = w.getAmount()-1;
											if(a <= 0){
												player.getInventory().remove(w);
											}else{
												w.setAmount(a);
											}
										}
									}
								}
								//System.out.println("took water = " + tookWater);//------------------------DEBUG
								if(tookWater == true){
									player.updateInventory();
					    			//Set Cooling
									quickCool(player, "spray", 2);
					    			//Shoot Water
					    			Location eyeLoc = player.getEyeLocation();
					    			double px = eyeLoc.getX();
					    			double py = eyeLoc.getY();
					    			double pz = eyeLoc.getZ();
					    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
					    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
					    			double x = Math.sin(pitch) * Math.cos(yaw);
					    			double y = Math.sin(pitch) * Math.sin(yaw);
					    			double z = Math.cos(pitch);
					    			for (int j = 1 ; j <= 10 ; j++) {
										if (getTarget(player) != null) {
											Entity attacked = getTarget(player);
											if ((attacked instanceof LivingEntity)) {
												((LivingEntity)attacked).damage((int)Math.round(2.0D * 6));
												plugin.displayParticles("WATER_SPLASH", attacked.getLocation(), 1, 15);
											}
										}
					    			}
					    			for (int i = 1 ; i <= 15 ; i++) {
						    			Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
						    			//ParticleEffects.sendToLocation(ParticleEffects.WATER_SPLASH, loc, 0, 0, 0, 1, 5);
						    			plugin.displayParticles("WATER_SPLASH", loc, 1, 5);
						    			//player.playEffect(loc, Effect.STEP_SOUND, 57);
						    			if(loc.getBlock().getType() != Material.AIR)
						    				i = 15;
						    		}
					    			//Create Water
					    			Location water = player.getTargetBlock((Set<Material>)null, 200).getLocation();
					    			if(!water.getBlock().getType().equals(Material.AIR)){
						    			water.setY(water.getY()+1);
						    			water.getBlock().setType(Material.WATER);
									}
								}
		    				}
		    			}else if((player.isSneaking() == true) && (event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && (player.isSneaking() == true)){
		    				if(user.isCooling() == false){
			    				//System.out.println("Spout");//------------------------DEBUG
			    				//Water Spout
			    				event.setCancelled(true);
				    			//Set Cooling
			    				//System.out.println("Spout cooling");//------------------------DEBUG
					    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
					    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
			    				user.setCooling(true);
			    				//Create Spout
					    		//System.out.println("Spout2");//------------------------DEBUG
					    		Block clickedlBlock = player.getTargetBlock((Set<Material>)null, 200);
					    		clickedlBlock.setType(Material.STATIONARY_WATER);
					    		//System.out.println("Spout3");//------------------------DEBUG
					    		int min = 3;
					    		int max = 12;
					    		int randomNum = new Random().nextInt((max - min) + 1) + min;
					    		for(int i = 0; i <= randomNum; i++){
					    			Location l = clickedlBlock.getLocation();
					    			l.setY(l.getY()+i);
					    			l.getBlock().setType(Material.WATER);
					    		}
					    		//Start Timer
					    		//System.out.println("Spout4");//------------------------DEBUG
					    		startCoolTimer(player, "Water Spout", 15, 0);
		    				}
		    			}else if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
		    				//System.out.println("Absorb");//------------------------DEBUG
		    				//Water Absorb
		    				event.setCancelled(true);
		    				int radius = 1;
		    				double xTmp = player.getLocation().getX();
		    				double yTmp = player.getLocation().getY();
		    				double zTmp = player.getLocation().getZ();
		    				final int finalX = (int) Math.round(xTmp);
		    				final int finalY = (int) Math.round(yTmp);
		    				final int finalZ = (int) Math.round(zTmp);
		                    for (int x = finalX-(radius); x <= finalX+radius; x ++){
		                    	for (int y = finalY-(radius); y <= finalY+radius; y ++) {
		                    		for (int z = finalZ-(radius); z <= finalZ+radius; z ++) {
		                    			Location loc = new Location(player.getWorld(), x, y, z);
		                    			Block block = loc.getBlock();
		                    			if((block.getType().equals(Material.STATIONARY_WATER)) || (block.getType().equals(Material.WATER))){
		                    				int hasWater = 0;
		                    				block.setType(Material.AIR);
		                    				if(block.getData() == 0){
			                    				for(ItemStack i : player.getInventory()){
			                    					if((i != null) && (i.getType().equals(Material.WATER))){
			                    						hasWater = hasWater + i.getAmount();
			                    					}
			                    				}
		                    				}
		                    				if(hasWater < 64){
		                    					player.getInventory().addItem(new ItemStack(Material.WATER, 1));
		                    				}
		                    			}
		                    		}
		                    	}
		                    }
		                    player.updateInventory();
		    			}
		    		}
				}catch(Exception e){System.out.println("Error " + e);}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("AquaMan")) && (game.getStarted == true))
    					event.setDamage(4);
    					//System.out.println("Iron Damage!");//------------------------DEBUG
    			}
    		}
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("IronMan")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
							//eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.25)));
						}else if((attacker.getItemInHand().getType().equals(Material.IRON_SPADE)) && (attacker.getItemInHand().getItemMeta().getDisplayName().equals("§4Trident"))){
    						event.setDamage(3.5);
    					}
    				}
				}
    		}
        }catch(Exception e){System.out.println(e);}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("AquaMan")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/2);
						}
						if((event.getCause() == DamageCause.BLOCK_EXPLOSION) || (event.getCause() == DamageCause.ENTITY_EXPLOSION)){
					    	event.setDamage(event.getDamage()/2);
						}
						if(event.getCause() == DamageCause.DROWNING){
					    	event.setDamage(0);
					    	player.setRemainingAir(20);
						}
					}
				}
			}catch(Exception e){System.out.println(e);}
		}
	}
	
	public static ItemStack getTrident(){
		ItemStack unibeam = new ItemStack(Material.IRON_SPADE);
		ItemMeta meta = unibeam.getItemMeta();
		meta.setDisplayName("§4Trident");
		unibeam.setItemMeta(meta);
		return unibeam;
	}
	
	public static ItemStack getWater(){
		ItemStack water = new ItemStack(Material.INK_SACK, 1, (short) 4);
		ItemMeta meta = water.getItemMeta();
		meta.setDisplayName("§bWater Control");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§3Left click to spray water");
		loreList.add("§3Right click to absorb water");
		loreList.add("§3Sneak-left click to make squids attack");
		loreList.add("§3Sneak-right click to form a column of water");
		meta.setLore(loreList);
		water.setItemMeta(meta);
		return water;
	}
	
	public static ItemStack getFish(){
		ItemStack fish = new ItemStack(Material.COOKED_FISH);
		ItemMeta meta = fish.getItemMeta();
		meta.setDisplayName("§bUse to start swimming");
		fish.setItemMeta(meta);
		return fish;
	}
	
	public static void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
			if(player.getFoodLevel() < 20)
				player.setFoodLevel(player.getFoodLevel()+1);
			Damageable d = player;
			if(d.getHealth() < 6)
					player.setHealth(d.getHealth()+1);
		}
	}
    
}
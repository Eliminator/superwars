package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SuperMan extends Hero{
	
	public SuperMan(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("SuperMan")) && (game.getStarted == true)){
				if(user.isJumping() == true){
					player.setVelocity(player.getLocation().getDirection().multiply(1));
					//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, player.getLocation(), 0, 0, 0, 0, 2);
				}
			}
		}
	}
	
	@SuppressWarnings({ "deprecation", "static-access" })
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("SuperMan")) && (game.getStarted == true)){
				try{
					ItemStack it = player.getItemInHand();
		    		if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying"))) {
	    				ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Use to start flying");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(false);
	    				player.setAllowFlight(false);
		    		}else if((it.getType().equals(Material.ANVIL)) && (it.getItemMeta().getDisplayName().equals("§4Use to start flying"))) {
		    			event.setCancelled(true);
		    			ItemStack flyTog = new ItemStack(Material.FEATHER, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Use to stop flying");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(true);
	    				player.setAllowFlight(true);
		    		}else if((it.getType().equals(Material.EYE_OF_ENDER)) && (it.getItemMeta().getDisplayName().equals("§4Vision Control"))) {
		    			//Vision Control
		    			//Right click to use x-ray vision
		    			//Left click to use laser vision
		    			event.setCancelled(true);
		    			if((!user.isCooling("beam")) && ((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR)))){
			    			//Set Cooling
		    				quickCool(player, "beam", 6);
			    			//Shoot Beam
			    			Location eyeLoc = player.getEyeLocation();
			    			double px = eyeLoc.getX();
			    			double py = eyeLoc.getY();
			    			double pz = eyeLoc.getZ();
			    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
			    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
			    			double x = Math.sin(pitch) * Math.cos(yaw);
			    			double y = Math.sin(pitch) * Math.sin(yaw);
			    			double z = Math.cos(pitch);
			    			for (int j = 1 ; j <= 10 ; j++) {
								if (getTarget(player) != null) {
									Entity attacked = getTarget(player);
									if ((attacked instanceof LivingEntity)) {
										((LivingEntity)attacked).damage((int)Math.round(2.0D * 4));
									}
								}
				    			for (int i = 1 ; i <= 50 ; i++) {
									Location loc = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
									//player.playEffect(loc, Effect.STEP_SOUND, 57);
									//ParticleEffects.sendToLocation(ParticleEffects.MAGIC_CRITICAL_HIT, loc, 0, 0, 0, 0, 10);
									//Spawn Red
//									final Item item = loc.getWorld().dropItemNaturally(loc, new ItemStack(Material.INK_SACK, 1, (short) 1)); 
//									ItemMeta im = item.getItemStack().getItemMeta();
//									im.setDisplayName("§7Thrown Grenade");
//									item.getItemStack().setItemMeta(im);
//									//Remove Red
//									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
//										public void run() {
//											try{
//												item.remove();
//											}catch(Exception e){}
//										}
//									}, (5));
									plugin.displayParticles("REDSTONE_DUST", loc, 0, 1);
				    			}
			    			}
			    			//Create Boom
				    		Location boom = player.getTargetBlock((Set<Material>)null, 200).getLocation();
				    		boom.getWorld().createExplosion(boom, 1);
		    			}else if((user.isCooling("xray") == false) && ((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR)))){
			    			//Set Cooling
				    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, true, user.getJumping, user.getCooling, user.getString);
				    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
				    		//X-ray
				    		List<Block> blocks = plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 1);
				    		blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 2));
				    		blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 3));
				    		blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 4));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 5));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 6));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 7));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 8));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 9));
				    		//blocks.addAll(plugin.SMClass.getSphere(player.getTargetBlock((Set<Material>)null, 100), 10));
				    		//Create Glass
							for(final Block bl : blocks){
								if(!bl.getType().equals(Material.AIR)){
									player.sendBlockChange(bl.getLocation(), Material.GLASS, (byte) 0);
									//Fix Look
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									     public void run() {
									    	try{
									    		player.sendBlockChange(bl.getLocation(), bl.getType(), (byte) bl.getData());
									    	}catch(Exception e){}
										 }
									}, (5 * 20));
								}
							}
							//Cool Timer
							quickCool(player, "xray", 12);
		    			}
		    		}else if((it.getType().equals(Material.INK_SACK)) && (it.getDurability() == 7) && (it.getItemMeta().getDisplayName().equals("§4Super Breath"))) {
						if (user.isCooling() == false) {
							//Super Breath
							Location loc = player.getEyeLocation();
							//Set Cool
							user.setCooling(true);
							//Make Breath Sound
							player.playSound(loc, Sound.ENTITY_BAT_TAKEOFF, 2F, 1F);
							player.playSound(loc, Sound.ENTITY_BLAZE_AMBIENT, 2F, 50F);
							player.playSound(loc, Sound.BLOCK_LAVA_EXTINGUISH, 1F, 0F);
							//Fire Breath
			    			Location eyeLoc = player.getEyeLocation();
			    			double px = eyeLoc.getX();
			    			double py = eyeLoc.getY();
			    			double pz = eyeLoc.getZ();
			    			double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
			    			double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
			    			double x = Math.sin(pitch) * Math.cos(yaw);
			    			double y = Math.sin(pitch) * Math.sin(yaw);
			    			double z = Math.cos(pitch);
			    			for (int j = 1 ; j <= 10 ; j++) {
								if(getTarget(player) != null){
									Entity attacked = getTarget(player);
									if ((attacked instanceof LivingEntity)) {
										((LivingEntity)attacked).damage((int)Math.round(2.0D * 4));
										((LivingEntity)attacked).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (5 * 20), 1), true);
										((LivingEntity)attacked).setVelocity(player.getLocation().getDirection().multiply(5));
									}
								}
			    			}
			    			//Smoke
			    			for (int i = 1 ; i <= 50 ; i++) {
			    				Location loc2 = new Location(player.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
				    			//player.playEffect(loc, Effect.STEP_SOUND, 133);
			    				if(loc2.getBlock().getType() != Material.AIR)
			    					i = 50;
			    				//Block Toss
//			    				if(i == 3){
//			    					List<Block> blocks = plugin.SMClass.getSphere(loc2.getBlock(), 2);
//			    					for(Block b : blocks)
//			    						if(b.getType() != Material.AIR){
//			    							FallingBlock block = event.getPlayer().getWorld().spawnFallingBlock(event.getPlayer().getEyeLocation(), b.getType(), (byte) b.getData());
//			    							block.setVelocity((player.getLocation().getDirection().multiply(1.5)));
//			    							b.setType(Material.AIR);
//			    							plugin.GLClass.entityCauseDamage(player, block, 4);
//			    						}
//			    				}
				    			//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, loc2, 0, 0, 0, 0, 2);
			    				plugin.displayParticles("CLOUD", loc2, 0, 2);
				    		}
							startCoolTimer(player, "Super Breath", 20, 0);
						}
		    		}else if((it.getType().equals(Material.SUGAR)) && (it.getItemMeta().getDisplayName().equals("§4Super Speed"))) {
		    			//Super Speed
		    			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (5 * 20), 10), true);
		    			player.setItemInHand(null);
						//Cool Timer
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								try{
					    			//Stop Cooling
									if(plugin.getGame(player) != null)
										player.getInventory().addItem(getSpeed());
								}catch(Exception e){}
							}
						}, (10 * 20));
		    		}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("IronMan")) && (game.getStarted == true))
    					event.setDamage(4);
    					//System.out.println("Iron Damage!");//------------------------DEBUG
    			}
    		}
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("SuperMan")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
							eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.25)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("SuperMan")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/2);
						}
						if((event.getCause() == DamageCause.BLOCK_EXPLOSION) || (event.getCause() == DamageCause.ENTITY_EXPLOSION)){
					    	event.setDamage(event.getDamage()/2);
						}
						if(event.getCause() == DamageCause.FIRE_TICK){
					    	event.setDamage(event.getDamage()/2);
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getVision(){
		ItemStack unibeam = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta meta = unibeam.getItemMeta();
		meta.setDisplayName("§4Vision Control");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to use x-ray vision");
		loreList.add("§8Left click to use laser vision");
		meta.setLore(loreList);
		unibeam.setItemMeta(meta);
		return unibeam;
	}
	
	public ItemStack getBreath(){
		ItemStack pulsars = new ItemStack(Material.INK_SACK, 1 , (short) 7);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§4Super Breath");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getSpeed(){
		ItemStack pulsars = new ItemStack(Material.SUGAR);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§4Super Speed");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getFlight(){
		ItemStack fly = new ItemStack(Material.ANVIL);
		ItemMeta meta = fly.getItemMeta();
		meta.setDisplayName("§4Use to start flying");
		fly.setItemMeta(meta);
		return fly;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(3 * plugin.getArmour())), true);
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					//Stop Flying
					User user = game.getPlayerList.get(plugin.getIndex(player));
					user.setJumping(false);
		    		player.setAllowFlight(false);
		    		for(ItemStack it : player.getInventory()){
		    			if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Use to stop flying"))) {
		    				ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
		    				ItemMeta meta = flyTog.getItemMeta();
		    				meta.setDisplayName("§4Use to start flying");
		    				flyTog.setItemMeta(meta);
		    				player.getInventory().remove(it);
		    				player.getInventory().addItem(flyTog);
			    		}
		    		}
				}
			}
		}catch(Exception e){}
	}
    
}
package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class HumanTorch extends Hero{
	
	public HumanTorch(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("HumanTorch")) && (game.getStarted == true)){
				if(user.isJumping() == true){
					player.setVelocity(player.getLocation().getDirection().multiply(1));
					//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, player.getLocation(), 0, 0, 0, 0, 5);
					//ParticleEffects.sendToLocation(ParticleEffects.FLAME, player.getLocation(), 0, 0, 0, 0, 25);
					plugin.displayParticles("LARGE_SMOKE", player.getLocation(), 0, 5);
					plugin.displayParticles("FLAME", player.getLocation(), 0, 25);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("HumanTorch")) && (game.getStarted == true)){
				try{
					event.setCancelled(true);
					ItemStack it = player.getItemInHand();
		    		if((it.getType().equals(Material.BLAZE_POWDER)) && (it.getItemMeta().getDisplayName().equals("§cFire Control")) && (user.isSneaking() == false)) {
		    			boolean shot = false;
		    			boolean type = false;
		    			if(player.isSneaking()){
		    				if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
		    					if(user.isJumping() == true){
		    						//Stop Flying
		    						user.setJumping(false);
						    		plugin.giveItems(user.getPlayer(), user.getHero(), true, 1);
						    		player.updateInventory();
						    		player.setAllowFlight(false);
		    					}else{
		    						//Set Flying
		    						user.setJumping(true);
							 		//Create Armour
						    		if(plugin.getConfig().getBoolean("useHeroDisguises") == false){
								 		ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
								 		ItemStack ches = new ItemStack(Material.LEATHER_CHESTPLATE);
								 		ItemStack pant = new ItemStack(Material.LEATHER_LEGGINGS);
								 		ItemStack boot = new ItemStack(Material.LEATHER_BOOTS);
										//Dye Armour
										Color fireRed = Color.fromRGB(200,30,0);
										plugin.dye(helm, fireRed);
										plugin.dye(ches, fireRed);
										plugin.dye(pant, fireRed);
										plugin.dye(boot, fireRed);
										//Add Enchantments
										helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
										ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
										pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
										boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
										//Give Armour
										player.getInventory().setHelmet(helm);
										player.getInventory().setChestplate(ches);
										player.getInventory().setLeggings(pant);
										player.getInventory().setBoots(boot);
						    		}
									player.updateInventory();
									player.setAllowFlight(true);
		    					}
		    				}
		    			}
		    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
		                    type = true;
		    				shot = true;
		    			}else if((player.isSneaking() == false) && (event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
							type = false;
		    				shot = true;
		    			}
		    			if(shot == true){
		    				//Throw Fire
							final Item fireball = player.getWorld().dropItem(player.getEyeLocation(), new ItemStack(Material.INK_SACK, 1, (short) 14));
							fireball.setVelocity(player.getLocation().getDirection().normalize().multiply(2));
							//Change Item Name
							ItemMeta m = fireball.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Shield");
							fireball.getItemStack().setItemMeta(m);
							fireball.getItemStack().setAmount(1);
			    			//Set Cooling
							user.setSneaking(true);
			    			//Shoot Fire
				    		shootFire(fireball, type, 0);
			    			//Time Stop Cool
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									try{
										//Finish Player Cooling
							    		Game game2 = plugin.getGame(player);
							    		User userCooler = game2.getPlayerList.get(plugin.getIndex(player));
							    		userCooler.setSneaking(false);
									}catch(Exception e){}
								}
							}, (2 * 20));
		    			}
		    		}else if((it.getType().equals(Material.FIREBALL)) && (it.getItemMeta().getDisplayName().equals("§cFire Ball"))) {
		    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
		    				if(!user.isCooling("fireball")){
					    		player.launchProjectile(Fireball.class);
				    			//Set Cooling
					    		quickCool(player, "fireball", 2);
		    				}
		    			}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
		    				if((user.isCooling() == false) && (player.isSneaking())){
			    				//Set Cooling
		    					user.setJumping(false);
		    					user.setCooling(true);
					    		startCoolTimer(player, "SuperNova", 35, 0);
					    		//Do Effect
					    		Damageable d = player;
					    		doNovaEffect(player, 0, d.getHealth());
		    				}
		    			}
		    		}
				}catch(Exception e){}
			}
		}
	}
	
	public void shootFire(final Item s, final boolean type, double timePassed){
		if(timePassed >= 2){
			if(type == true){
				//ParticleEffects.sendToLocation(ParticleEffects.LARGE_EXPLODE, s.getLocation(), 0, 0, 0, 1, 2);
				plugin.displayParticles("LARGE_EXPLODE", s.getLocation(), 1, 2);
				//Fire Start
				Block clickedlBlock = s.getLocation().getBlock();
				if(clickedlBlock.getType().equals(Material.AIR)){
					clickedlBlock.setType(Material.FIRE);
				}
				//Ice Melt
				Location l = clickedlBlock.getLocation();
				l.setY(l.getY()+1);
				Block fire = l.getBlock();
				if(fire.getType().equals(Material.ICE)){
					fire.setType(Material.WATER);
				}
				//Water Evaporate
				int radius = 1;
				double xTmp = s.getLocation().getX();
				double yTmp = s.getLocation().getY();
				double zTmp = s.getLocation().getZ();
				final int finalX = (int) Math.round(xTmp);
				final int finalY = (int) Math.round(yTmp);
				final int finalZ = (int) Math.round(zTmp);
	            for (int x = finalX-(radius); x <= finalX+radius; x ++){
	            	for (int y = finalY-(radius); y <= finalY+radius; y ++) {
	            		for (int z = finalZ-(radius); z <= finalZ+radius; z ++) {
	            			Location loc = new Location(s.getWorld(), x, y, z);
	            			Block block = loc.getBlock();
	            			if((block.getType().equals(Material.STATIONARY_WATER)) || (block.getType().equals(Material.WATER))){
	            				block.setType(Material.AIR);
	            			}
	            		}
	            	}
	            }
			}else{
				//ParticleEffects.sendToLocation(ParticleEffects.LAVA_SPARK, s.getLocation(), 0, 0, 0, 2, 100);
				plugin.displayParticles("LAVA_SPARK", s.getLocation(), 2, 100);
				for(Entity attacked : s.getNearbyEntities(5, 5, 5)){
					if(attacked instanceof LivingEntity){
						attacked.setFireTicks(80);
						((LivingEntity)attacked).damage((int)Math.round(2.0D * 6));
					}
				}
			}
			s.remove();
		}else{
			//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, s.getLocation(), 0, 0, 0, 1, 5);
			//ParticleEffects.sendToLocation(ParticleEffects.FLAME, s.getLocation(), 0, 0, 0, 0, 10);
			plugin.displayParticles("LARGE_SMOKE", s.getLocation(), 0, 5);
			plugin.displayParticles("FLAME", s.getLocation(), 0, 10);
			//ParticleEffects.sendToLocation(ParticleEffects.DRIP_LAVA, s.getLocation(), 0, 0, 0, 0, 1);
			final double newTimePassed = timePassed + 0.05;
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					shootFire(s, type, newTimePassed);
				}
			}, (1));
		}
	}
	
	public void doNovaEffect(final Player p, double timePassed, final double h){
		//Game End Check
		if(plugin.getGame(p) == null)
			return;
		try{
			//Effects
			p.setHealth(h);
			int radius;
			double xTmp = p.getLocation().getX();
			double yTmp = p.getLocation().getY();
			double zTmp = p.getLocation().getZ();
			if(timePassed == 2){
				p.getLocation().getWorld().createExplosion(xTmp, yTmp, zTmp, 2, true, true);
			}
			if(timePassed <= 3){
				radius = 1;
			}else if (timePassed <= 4.5){
				radius = 2;
			}else{
				radius = 2;
				p.getLocation().getWorld().createExplosion(xTmp, yTmp, zTmp, 4, true, true);
				//ParticleEffects.sendToLocation(ParticleEffects.ENCHANTS, p.getLocation(), 0, 0, 0, 3, 50);
				plugin.displayParticles("ENCHANTS", p.getLocation(), 3, 50);
			}
			final int finalX = (int) Math.round(xTmp);
			final int finalY = (int) Math.round(yTmp);
			final int finalZ = (int) Math.round(zTmp);
            for (int x = finalX-(radius); x <= finalX+radius; x ++){
            	for (int y = finalY-(radius); y <= finalY+radius; y ++) {
            		for (int z = finalZ-(radius); z <= finalZ+radius; z ++) {
            			Location loc = new Location(p.getWorld(), x, y, z);
        				//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, loc, 0, 0, 0, 1, 5);
        				p.getWorld().playEffect(loc, Effect.MOBSPAWNER_FLAMES, 1, 256);
        				//ParticleEffects.sendToLocation(ParticleEffects.LAVA_SPARK, loc, 0, 0, 0, 1, 5);
    					plugin.displayParticles("LARGE_SMOKE", loc, 1, 5);
    					plugin.displayParticles("LAVA_SPARK", loc, 1, 5);
            		}
            	}
            }
            p.setVelocity(new Vector(0, 0.2, 0));
            //Pull
            for(Entity e : p.getNearbyEntities(7, 7, 7)){
				if(e instanceof LivingEntity){
					e.setFireTicks(6);
					((LivingEntity)e).damage((int)Math.round(1));
					Location loc1 = p.getEyeLocation();
					Location loc2 = e.getLocation();
					Vector v = new Vector(loc1.getX()-loc2.getX(), loc1.getY()-loc2.getY(), loc1.getZ()-loc2.getZ());
					v = v.setY(v.getY()+0.3);
			        e.setVelocity(v.multiply(0.2));
				}
            }
		}catch(Exception e){}
		//Timer
		final double newTimePassed = timePassed + 0.25;
		if(timePassed < 5){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					doNovaEffect(p, newTimePassed, h);
				}
			}, (5));
		}else{
    		plugin.giveItems(p, "HumanTorch", true, 1);
    		p.updateInventory();
    		p.setAllowFlight(false);
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				User user = game.getPlayerList.get(plugin.getIndex(attacker));
    				if((user.getHero().equals("HumanTorch")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
							eVictim.setFireTicks(60);
							//eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.25)));
    					}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = game.getPlayerList.get(plugin.getIndex(player));
					//Powers stop some damage
					if(user.getHero().equals("HumanTorch")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/4);
						}
						if((event.getCause() == DamageCause.BLOCK_EXPLOSION) || (event.getCause() == DamageCause.ENTITY_EXPLOSION)){
							event.setDamage(0);
							event.setCancelled(true);
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getFire(){
		ItemStack unibeam = new ItemStack(Material.BLAZE_POWDER);
		ItemMeta meta = unibeam.getItemMeta();
		meta.setDisplayName("§cFire Control");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§4Right click to shoot catching fire");
		loreList.add("§4Left click to shoot damaging fire");
		loreList.add("§4Sneak-Left click to toggle flight");
		meta.setLore(loreList);
		unibeam.setItemMeta(meta);
		return unibeam;
	}
	
	public ItemStack getCharge(){
		ItemStack pulsars = new ItemStack(Material.FIREBALL);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§cFire Ball");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§4Right click to throw firebals");
		loreList.add("§4Sneak-Left click to go SuperNova");
		meta.setLore(loreList);
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(3 * plugin.getArmour())), true);
				player.setFireTicks(220);
				showFire(player, 0);
				for(Entity e : player.getNearbyEntities(6, 6, 6)){
					//System.out.println("Found " + e);
					if(e instanceof LivingEntity){
						e.setFireTicks(60);
					}
				}
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					//Stop Flying
					User user = game.getPlayerList.get(plugin.getIndex(player));
		    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, false, user.getCooling, user.getString);
		    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
					user.setJumping(false);
				}
			}
		}catch(Exception e){}
	}
	
	public void showFire(final Player p, int time){
		try{
			Game game = plugin.getGame(p);
			if(game.getStarted == true){
				//Make Fire
				p.getWorld().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 1, 256);
				//ParticleEffects.sendToLocation(ParticleEffects.LAVA_SPARK, p.getLocation(), 0, 0, 0, 0, 1);
				plugin.displayParticles("LAVA_SPARK", p.getLocation(), 0, 1);
				//Damage In Water
				Location l = p.getLocation();
				if((l.getBlock().getType().equals(Material.WATER)) || (l.getBlock().getType().equals(Material.STATIONARY_WATER)))
					((LivingEntity)p).damage((int)Math.round(1));
				//Tick Time
				final int newTime = time + 1;
				if(time < 10){
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							try{
							showFire(p, newTime);
							}catch(Exception e){}
						}
					}, (20));
				}
			}
		}catch(Exception e){}
	}
    
}
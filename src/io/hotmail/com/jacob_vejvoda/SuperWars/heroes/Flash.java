package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

@SuppressWarnings("deprecation")
public class Flash extends Hero{
	HashMap<Entity, Location> locMap = new HashMap<Entity, Location>();
	
	public Flash(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onManMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if ((!user.getHero().equals("Flash")) && (game.getStarted == true)){
				//Time Stop
				for (PotionEffect effect : player.getActivePotionEffects())
					if(effect.getType().equals(PotionEffectType.SLOW_DIGGING)){
						//System.out.println("NM");
						event.setCancelled(true);
						return;
					}
			}else if ((user.getHero().equals("Flash")) && (game.getStarted == true)){
				if(user.isJumping()){
					//Speed
					Location l = player.getLocation(); l.setY(l.getY() - 1);
					if(!(l.getBlock().getType().equals(Material.AIR))){
						Vector v = player.getLocation().getDirection().multiply(2);
						//v.setY(0);
						player.setVelocity(v);
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		//System.out.println("INT");
		if(plugin.getGame(player) != null){
			//System.out.println("GG");
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Flash")) && (game.getStarted == true)){
				//System.out.println("SW");
				try{
					ItemStack it = player.getItemInHand();
		    		if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().equals("§4Super Speed - on"))) {
	    				ItemStack flyTog = new ItemStack(Material.ANVIL, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Super Speed - off");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(true);
	    				player.setAllowFlight(false);
		    		}else if((it.getType().equals(Material.ANVIL)) && (it.getItemMeta().getDisplayName().equals("§4Super Speed - off"))) {
		    			event.setCancelled(true);
		    			ItemStack flyTog = new ItemStack(Material.FEATHER, 1);
	    				ItemMeta meta = flyTog.getItemMeta();
	    				meta.setDisplayName("§4Super Speed - on");
	    				flyTog.setItemMeta(meta);
	    				player.setItemInHand(flyTog);
	    				user.setJumping(false);
	    				player.setAllowFlight(true);
		    		}else if((it.getType().equals(Material.GLOWSTONE_DUST)) && (it.getItemMeta().getDisplayName().equals("§eActions"))) {
		    			//Actions
		    			//Right-click to phase through a wall
		    			//Left-click to throw a super punch
		    			event.setCancelled(true);
		    			if((!user.isCooling("punch")) && ((event.getAction().equals(Action.LEFT_CLICK_BLOCK)) || (event.getAction().equals(Action.LEFT_CLICK_AIR)))){
		    				//Super Punch
				    		//Punch
				    		Vector v = player.getLocation().getDirection().multiply(2);
				    		if(v.getY() > 1)
				    			v.setY(1);
				    		player.setVelocity(v);
				    		//Particals
				    		for(int i = 0; i < 10; i++){
								Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
									public void run() {
										try{
											//ParticleEffects.sendToLocation(ParticleEffects.CRITICALS, player.getLocation(), 0, 0, 0, 1, 5);
											plugin.displayParticles("CRITICALS", player.getLocation(), 1, 5);
										}catch(Exception e){}
									}
								}, (1 * i));
				    		}
				    		//Damage
							if (getTarget(player) != null) {
								Entity attacked = getTarget(player);
								if ((attacked instanceof LivingEntity)) {
									((LivingEntity)attacked).damage((int)Math.round(2.0D * 4));
								}
							}
			    			//Set Cooling
		    				quickCool(player, "punch", 6);
		    			}if(((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR)))){
		    				//Phase
		    				Block feet = event.getClickedBlock().getRelative(event.getBlockFace().getOppositeFace());
		    				Location hl = feet.getLocation();
		    				hl.setY(hl.getY()+1);
		    				Block head = hl.getBlock();
		    				if((plugin.transBlocks.contains(head.getTypeId())) && (plugin.transBlocks.contains(feet.getTypeId()))){
		    					player.teleport(feet.getLocation());
		    				}
		    			}
		    		}else if((user.isCooling() == false) && (it.getType().equals(Material.WATCH)) && (it.getItemMeta().getDisplayName().equals("§4Stop Time"))) {
		    			//Stop Time
		    			for(User u : game.getPlayerList)
		    				if(!u.getHero().equals("Flash"))
		    					u.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 8*20, 9), true);
		    			//Mob Freeze
//		    			for(Entity e : player.getWorld().getEntities())
//		    				if(!(e instanceof Player))
//			    				if(plugin.locIsInGame(e.getLocation(), game))
//			    					locMap.put(e, e.getLocation());
		    			for(Entity e : player.getNearbyEntities(100, 100, 100))
		    				if(!(e instanceof Player))
		    					locMap.put(e, e.getLocation());
		    			mobFreeze(0, 8*20);
						//Start Cool
			    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
			    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
		    			user.setCooling(true);
			    		startCoolTimer(player, "Stop Time", 60, 0);
		    		}
				}catch(Exception e){}
			}else if ((!(user.getHero().equals("Flash"))) && (game.getStarted == true)){
				//Time Stop
				//System.out.println("U not F");
				for (PotionEffect effect : player.getActivePotionEffects())
					if(effect.getType().equals(PotionEffectType.SLOW_DIGGING)){
						//System.out.println("No Intera");
						event.setCancelled(true);
						return;
					}
			}
		}
	}
	
	public void mobFreeze(int time, final int orgTime){
		for(Entry<Entity, Location> hm : locMap.entrySet()){
			try{
				hm.getKey().teleport(hm.getValue());
			}catch(Exception e){}
		}
		//Time
		final int nt = time + 1;
		if(time < orgTime){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					try{
						mobFreeze(nt, orgTime);
					}catch(Exception e){}
				}
			}, (1));
		}else
			locMap.clear();
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onProjLaunch(ProjectileLaunchEvent e) {
		//System.out.println("Launch");
		if((e.getEntity() instanceof Fireball) || (e.getEntity() instanceof Arrow)){
			Projectile f = (Projectile) e.getEntity();
			try{
				if(locMap.containsKey(f.getShooter()))
					f.remove();
			}catch(Exception x){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("Flash")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.GLOWSTONE_DUST)){
							event.setDamage(2);
							eVictim.setVelocity((attacker.getLocation().getDirection().multiply(0.3)));
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("Flash")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/2);
						}
						if(event.getCause() == DamageCause.FIRE_TICK){
					    	event.setDamage(event.getDamage()/4);
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getSpeed(){
		ItemStack speed = new ItemStack(Material.ANVIL);
		ItemMeta meta = speed.getItemMeta();
		meta.setDisplayName("§4Super Speed - off");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Click to toggle");
		meta.setLore(loreList);
		speed.setItemMeta(meta);
		return speed;
	}
	
	public ItemStack getFist(){
		ItemStack fist = new ItemStack(Material.GLOWSTONE_DUST);
		ItemMeta meta = fist.getItemMeta();
		meta.setDisplayName("§eActions");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§6Right-click to phase through a wall");
		loreList.add("§6Left-click to throw a super punch");
		meta.setLore(loreList);
		fist.setItemMeta(meta);
		return fist;
	}
	
	public ItemStack getTime(){
		ItemStack time = new ItemStack(Material.WATCH);
		ItemMeta meta = time.getItemMeta();
		meta.setDisplayName("§4Stop Time");
		time.setItemMeta(meta);
		return time;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
			}
		}catch(Exception e){}
	}
    
}
package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Deadpool extends Hero{

	public Deadpool(SuperWars instance) {
		super(instance);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onCaptianInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Deadpool")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_HOE)) && (name.contains("§7Gun"))){
						name = name.replace("§7", "");
						int ammo = Integer.parseInt(name.replaceAll("[\\D]", ""));
						//System.out.println("Ammo: " + ammo);//------------------------DEBUG
						if ((user.isCooling("gun") == false) && ammo >= 1) {
							//Set Cool
							quickCool(player, "gun", 1);
				    		//Make Gun Smoke
							Location loc = player.getLocation();
							Location fLoc = player.getEyeLocation();
							String direction = getDirection(player);
							int numDir = 0;
							if(direction.equals("Southeast")){
								//numDir = 0;
								numDir = 2;
							}else if(direction.equals("South")){
								//numDir = 1;
								numDir = 5;
							}else if(direction.equals("Southwest")){
								//numDir = 2;
								numDir = 8;
							}else if(direction.equals("East")){
								//numDir = 3;
								numDir = 1;
							}else if(direction.equals("West")){
								//numDir = 5;
								numDir = 7;
							}else if(direction.equals("Northeast")){
								//numDir = 6;
								numDir = 0;
							}else if(direction.equals("North")){
								//numDir = 7;
								numDir = 3;
							}else if(direction.equals("Northwest")){
								//numDir = 8;
								numDir = 6;
							}
							loc.getWorld().playEffect(fLoc, Effect.SMOKE, numDir);
							//Make Gun Sound
							player.playSound(loc, Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 2F, 1F);
							player.playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 2F, 50F);
							player.playSound(loc, Sound.ENTITY_ITEM_BREAK, 1F, 0F);
							//Fire Gun Bullet
							Snowball ball = player.getWorld().spawn(player.getEyeLocation(), Snowball.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(8));
							//Take Gun Ammo
							ItemMeta m = item.getItemMeta();
							m.setDisplayName("§7Gun - <<" + (ammo-1) + ">>");
							item.setItemMeta(m);
						}else if ((user.isCooling("gun") == false) && ammo <= 0) {
							//Make Gun Sound
							Location loc = player.getLocation();
							player.playSound(loc, Sound.BLOCK_WOODEN_DOOR_OPEN, 1F, 2F);
							player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 2F);
							//Set Cooling
							user.setCooling("gun");
							//Reload
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							     public void run() {
							    	try{
							    		player.playSound(player.getLocation(), Sound.BLOCK_WOODEN_DOOR_CLOSE, 1F, 3F);
										player.playSound(player.getLocation(), Sound.BLOCK_NOTE_SNARE, 1F, 2F);
							    		User user2 = game.getPlayerList.get(plugin.getIndex(player));
							    		user2.stopCooling("gun");
							    		for(ItemStack item2 : player.getInventory()){
							    			if((item2.getItemMeta() != null) && (item2.getItemMeta().getDisplayName() != null) && (item2.getItemMeta().getDisplayName().equals("§7Gun - <<0>>"))){
												ItemMeta m2 = item2.getItemMeta();
												m2.setDisplayName("§7Gun - <<8>>");
												item2.setItemMeta(m2);
							    			}
							    		}
							    	}catch(Exception e){}
								 }
							}, (3 * 20));
						}
					}else if ((player.getItemInHand().getType().equals(Material.SLIME_BALL)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Frag Grenade"))){
						if((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))){
							//Throw Item
							Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(1));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Grenade");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Block
							int amount = player.getItemInHand().getAmount() - 1;
							if(amount <= 0){
								player.setItemInHand(null);
							}else{
								player.getItemInHand().setAmount(amount);
							}
							//Time Boom
							BatMan.explode(5, "frag", dropped);
						}
					}else if ((player.getItemInHand().getType().equals(Material.WATCH)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Teleportation Device"))){
						if(user.isCooling() == false){
							Location old = player.getLocation().clone();
							Location l = player.getTargetBlock((Set<Material>)null, 100).getLocation();
							l.setY(l.getY() + 1);
							player.teleport(l);
							//ParticleEffects.sendToLocation(ParticleEffects.EXPLODE, old, 0, 0, 0, 1, 1);
							plugin.displayParticles("EXPLODE", old, 1, 1);
							//Set Cool
				    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
				    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
							user.setCooling(true);
				    		startCoolTimer(player, "Teleport", 20, 0);
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Deadpool")) && (game.getStarted == true)){
				event.setCancelled(true);
				//Check Jump Timer and Near Wall
				//System.out.println("J1");//------------------------DEBUG
				if((user.isCooling("jump") == false) && (nearWall(user.getPlayer().getLocation()))){
					//System.out.println("J2");//------------------------DEBUG
					//High Jump
					Vector v = (player.getLocation().getDirection().multiply(1));
					v = v.setY(1.0);
					player.setVelocity(v);
					//Jump Cool
			    	quickCool(player, "jump", 5);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Snowball){
    			Snowball sb = (Snowball) eAttacker;
    			if(sb.getShooter() instanceof Player){
    				Player shooter = (Player) sb.getShooter();
    				String h = plugin.getHero(shooter);
    				Game game = plugin.getGame(shooter);
    				if((h.equals("Deadpool")) && (game.getStarted == true))
    					event.setDamage(6);
    			}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = game.getPlayerList.get(plugin.getIndex(player));
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("Deadpool"))){
				    	event.setDamage(event.getDamage()/4);
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getPistol(){
		ItemStack gun = new ItemStack(Material.IRON_HOE);
		ItemMeta meta = gun.getItemMeta();
		meta.setDisplayName("§7Gun - <<8>>");
		gun.setItemMeta(meta);
		return gun;
	}
	
	public ItemStack getSword(){
		ItemStack sword = new ItemStack(Material.IRON_SWORD);
		ItemMeta meta = sword.getItemMeta();
		meta.setDisplayName("§7Katana");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8For slicing enemies up!");
		meta.setLore(loreList);
		sword.setItemMeta(meta);
		sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		return sword;
	}
	
	public ItemStack getGrenades(int amount){
		ItemStack gren = new ItemStack(Material.SLIME_BALL);
		gren.setAmount(amount);
		ItemMeta meta = gren.getItemMeta();
		meta.setDisplayName("§7Frag Grenade");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to throw");
		meta.setLore(loreList);
		gren.setItemMeta(meta);
		return gren;
	}
	
	public ItemStack getTPDevice(){
		ItemStack tp = new ItemStack(Material.WATCH);
		ItemMeta meta = tp.getItemMeta();
		meta.setDisplayName("§7Teleportation Device");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right-click to teleport");
		meta.setLore(loreList);
		tp.setItemMeta(meta);
		return tp;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
			//Heal
			Damageable d = player;
			if(d.getHealth() < 20){
				try{
					player.setHealth(d.getHealth()+1);
				}catch(Exception e){}
			}
		}
	}
    
}
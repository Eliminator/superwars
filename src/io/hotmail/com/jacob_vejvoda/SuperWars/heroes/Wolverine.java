package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Wolverine extends Hero{
	
	public Wolverine(SuperWars instance) {
		super(instance);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Wolverine")) && (user.isSneaking() == false) && (game.getStarted == true)){
				Location f = player.getLocation();
				f.setY(f.getY()-1);
				if(plugin.transBlocks.contains(f.getBlock().getTypeId())){
					user.setSneaking(true);
					cling(player);
				}else
					player.setAllowFlight(false);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Wolverine")) && (game.getStarted == true)){
				event.setCancelled(true);
				if((user.isSneaking() == true) && (player.getAllowFlight() == true)){
					//Jump
					//System.out.println("Jump");//------------------------DEBUG
					Vector v = (player.getLocation().getDirection().multiply(1));
					v = v.setY(1.2);
					player.setVelocity(v);
				}else if((user.isSneaking() == false) && (player.getAllowFlight() == true)){
					Vector v = (player.getLocation().getDirection().multiply(1));
					player.setVelocity(v);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public void cling(final Player player){
		try{
			if(player.isSneaking()){
				boolean clinging = false;
				Location l = player.getLocation();
				int radius = 1;
				double xTmp = l.getX();
				double yTmp = l.getY();
				double zTmp = l.getZ();
				final int finalX = (int) Math.round(xTmp);
				final int finalY = (int) Math.round(yTmp);
				final int finalZ = (int) Math.round(zTmp);
                for (int x = finalX-(radius); x <= finalX+radius; x ++){
                	for (int y = finalY-(radius); y <= finalY+radius; y ++) {
                		for (int z = finalZ-(radius); z <= finalZ+radius; z ++) {
                			Location loc = new Location(l.getWorld(), x, y, z);
                			Block block = loc.getBlock();
                			if(!plugin.transBlocks.contains(block.getTypeId())){
                				clinging = true;
                			}
                		}
                	}
                }
                if(clinging == true){
                	if(player.getVelocity().getY() <= 0.4)
                		player.setVelocity(new Vector(0, 0, 0));
					player.setAllowFlight(true);
        			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
        				public void run() {
        					cling(player);
        				}
        			}, (1));
					return;
                }
			}
			player.setAllowFlight(true);
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			user.setSneaking(false);
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Wolverine")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_FENCE)) && (name.contains("§9Claws"))){
						//System.out.println("Intera");//------------------------DEBUG
						if((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && (player.isSneaking())){
							//System.out.println("Slice");//------------------------DEBUG
				    		//Make Cut
				    		Block top = event.getClickedBlock();
				    		Location b = top.getLocation();
				    		b.setY(b.getY()-1);
				    		Block bottom = b.getBlock();
				    		top.setType(Material.AIR);
				    		bottom.setType(Material.AIR);
				    		player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 1F, 2F);
						}else if((player.isSneaking()) && (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
			                if(user.isSneaking() == true){
								player.setVelocity(new Vector(0, 0.5, 0));
			                }
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event){
		try{
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("Wolverine")) && (game.getStarted == true)){
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.IRON_FENCE)) && (name.contains("§9Claws"))){
						event.setCancelled(true);
					}
				}
			}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		if(event.getDamager() instanceof Player){
    			Player attacker = (Player) event.getDamager();
    			Entity eVictim = event.getEntity();
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				User user = game.getPlayerList.get(plugin.getIndex(attacker));
    				if((user.getHero().equals("Wolverine")) && (game.getStarted == true)){
						ItemStack aItem = attacker.getItemInHand();
						if((aItem.getType().equals(Material.IRON_FENCE)) && (aItem.getItemMeta().getDisplayName().equals("§9Claws"))){
							if((eVictim instanceof Player) && (attacker.isSneaking()) && (user.isCooling() == false)){
								Player pVic = (Player) eVictim;
								pVic.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 360, 5), true);
								pVic.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 120, 2), true);
								pVic.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 2), true);
								//Set Cool
								user.setCooling(true);
					    		startCoolTimer(attacker, "Power Slice", 20, 0);
							}
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("Wolverine"))){
				    	event.setDamage(event.getDamage()/4);
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getClaws(){
		ItemStack claws = new ItemStack(Material.IRON_FENCE);
		ItemMeta meta = claws.getItemMeta();
		meta.setDisplayName("§9Claws");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Left click to attack");
		loreList.add("§8Sneak-right click to slice through blocks");
		loreList.add("§8Sneak-attack to do a power slice");
		meta.setLore(loreList);
		claws.setItemMeta(meta);
		claws.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);
		claws.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
		return claws;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 8), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 5), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 220, 1), true);
			//Heal
			Damageable d = player;
			if(d.getHealth() < 20){
				try{
					player.setHealth(d.getHealth()+1);
				}catch(Exception e){}
			}
		}
	}
    
}
package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

@SuppressWarnings("deprecation")
public class Hawkeye extends Hero{
	
	public Hawkeye(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onManInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("Hawkeye")) && (game.getStarted == true)){
				try{
					ItemStack i = player.getItemInHand();
		    		if(i.getType().equals(Material.ARROW)) {
		    			//System.out.println("Used");
    					if(i.getItemMeta().getDisplayName().contains("§7Fire")){
    						player.setItemInHand(getArrow2());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Explosive")){
    						player.setItemInHand(getArrow3());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Toxic")){
    						player.setItemInHand(getArrow5());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Paralisis")){
    						player.setItemInHand(getArrow6());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Grapple")){
    						player.setItemInHand(getArrow4());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Normal")){
    						player.setItemInHand(getArrow7());
    					}else if(i.getItemMeta().getDisplayName().contains("§7Lava")){
    						player.setItemInHand(getArrow1());
    					}
		    		}else if(i.getType().equals(Material.BOW)){
		    			event.setCancelled(true);
		    			if(hasArrow(player) == true){
		    				player.launchProjectile(Arrow.class);
		    				for(ItemStack it : player.getInventory()){
		    					if((it != null) && (it.getType().equals(Material.ARROW))){
		    						player.getInventory().remove(it);
		    					}
		    				}
		    			}
		    		}
				}catch(Exception e){}
			}
		}
	}
	
	public void addArrow(Player p){
		try{
			Game game = plugin.getGame(p);
			User user = game.getPlayerList.get(plugin.getIndex(p));
			//System.out.println("String: " + user.getString());
			if((user.getHero().equals("Hawkeye")) && (game.getStarted == true)){
				if(user.getString().equals("Fire")){
					//Fire
					p.getInventory().addItem(getArrow1());
				}else if(user.getString().equals("Explosive")){
					//Explode
					p.getInventory().addItem(getArrow2());
				}else if(user.getString().equals("Toxic")){
					//Toxin
					p.getInventory().addItem(getArrow4());
				}else if(user.getString().equals("Paralisis")){
					//Freeze
					p.getInventory().addItem(getArrow5());
				}else if(user.getString().equals("Grapple")){
					//Grapple
					p.getInventory().addItem(getArrow3());
				}else if(user.getString().equals("Normal")){
					//Normal
					p.getInventory().addItem(getArrow6());
				}else if (user.getString().equals("Lava")){
					//Lava
					p.getInventory().addItem(getArrow7());
				}
			}
		}catch(Exception e){}
		
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if((event.getEntity().getShooter() instanceof Player) && (event.getEntity() instanceof Arrow)){
			final Player player = (Player) event.getEntity().getShooter();
			Arrow a = (Arrow) event.getEntity();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("Hawkeye")) && (game.getStarted == true)){
					try{
						for(ItemStack i : player.getInventory()){
				    		if(i.getType().equals(Material.ARROW)){
		    					String type = "Normal";
		    					if(i.getItemMeta().getDisplayName().contains("§7Fire")){
		    						type = "Fire";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Explosive")){
		    						type = "Explosive";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Toxic")){
		    						type = "Toxic";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Paralisis")){
		    						type = "Paralisis";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Grapple")){
		    						type = "Grapple";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Lava")){
		    						type = "Lava";
		    					}else if(i.getItemMeta().getDisplayName().contains("§7Normal")){
		    						type = "Normal";
		    					}
		    					user.setString(type);
		    					if((player.isSneaking()) && (user.isCooling() == false)){
		    						//Power Shot
				    				//Make New Arrow
				    				ArrayList<Arrow> arrowList = new ArrayList<Arrow>();
									Location loc1 = player.getTargetBlock((Set<Material>)null, 200).getLocation();
									Location loc2 = player.getEyeLocation();
									int arrowSpeed = 1;
									loc2.setY(loc2.getBlockY()+2);
									loc2.setX(loc2.getBlockX()+0.5);
									loc2.setZ(loc2.getBlockZ()+0.5);
				    				Arrow a2 = a.getWorld().spawnArrow(loc2, new Vector(loc1.getX()-loc2.getX(), loc1.getY()-loc2.getY(), loc1.getZ()-loc2.getZ()), arrowSpeed, 12);
				    				a2.setShooter(player);
				    				loc2.setY(loc2.getBlockY()+2);
									loc2.setX(loc2.getBlockX()-1);
									loc2.setZ(loc2.getBlockZ()-1);
				    				Arrow a3 = a.getWorld().spawnArrow(loc2, new Vector(loc1.getX()-loc2.getX(), loc1.getY()-loc2.getY(), loc1.getZ()-loc2.getZ()), arrowSpeed, 12);
				    				a3.setShooter(player);
				    				arrowList.add(a);
				    				arrowList.add(a2);
				    				arrowList.add(a3);
				    				//arrowList.add(a3);
				    				//Set Arrows Lava
				    				for(Arrow ar : arrowList){
					    		      double minAngle = 6.283185307179586D;
					    		      Entity minEntity = null;
					    		      for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
					    		        if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity)) && (!entity.isDead())) {
					    		          Vector toTarget = entity.getLocation().toVector().clone().subtract(player.getLocation().toVector());
					    		          double angle = ar.getVelocity().angle(toTarget);
					    		          if (angle < minAngle) {
					    		            minAngle = angle;
					    		            minEntity = entity;
					    		          }
					    		        }
					    		      }
					    		      if (minEntity != null)
					    		        new ArrowHomingTask((Arrow)ar, (LivingEntity)minEntity, plugin);
				    				}
				    				//Cool
		    						//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, true, true, type);
		    						//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
				    				user.setCooling(true);
		    						startCoolTimer(player, "Triple Homing Shot", 25, 0);
		    					}else if(user.isJumping() == false){
		    						//Type Shot
		    						user.setJumping(true);
		    						if(!(type.equals("Grapple"))){
					    		      double minAngle = 6.283185307179586D;
					    		      Entity minEntity = null;
					    		      for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
					    		        if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity)) && (!entity.isDead())) {
					    		          Vector toTarget = entity.getLocation().toVector().clone().subtract(player.getLocation().toVector());
					    		          double angle = a.getVelocity().angle(toTarget);
					    		          if (angle < minAngle) {
					    		            minAngle = angle;
					    		            minEntity = entity;
					    		          }
					    		        }
					    		      }
					    		      if (minEntity != null)
					    		        new ArrowHomingTask((Arrow)a, (LivingEntity)minEntity, plugin);
		    						}
		    					}else{
		    						event.setCancelled(true);
		    					}
		    					return;
				    		}
						}
					}catch(Exception e){}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		Entity eVictim = event.getEntity();
    		if(eAttacker instanceof Arrow){
    			Arrow a = (Arrow) eAttacker;
    			if(a.getShooter() instanceof Player){
    				final Player shooter = (Player) a.getShooter();
    				Game game = plugin.getGame(shooter);
    				User user = game.getPlayerList.get(plugin.getIndex(shooter));
    				if((plugin.locIsInGame(eVictim.getLocation(), game) == true) && (plugin.locIsInGame(a.getLocation(), game) == true) && (game.getStarted == true) && ((user.getHero().equals("Hawkeye")) || (user.getHero().equals("IronMan")))){
    					boolean difArrow = false;
    					if(user.getString().equals("Fire")){
    						//Light On Fire
    						eVictim.setFireTicks(100);
    						difArrow = true;
    					}else if(user.getString().equals("Explosive")){
    						//Make Boom
    						a.getWorld().createExplosion(a.getLocation(), 3);
    						difArrow = true;
    					}else if(user.getString().equals("Toxic")){
    						//Add Toxin
    						((LivingEntity) eVictim).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 80, 1), true);
    						difArrow = true;
    					}else if(user.getString().equals("Paralisis")){
    						//Freeze
    						((LivingEntity) eVictim).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 5), true);
    						difArrow = true;
    					}else if(user.getString().equals("Grapple")){
    						//Pull Victim
    						hookPull(eVictim, shooter.getLocation());
    						difArrow = true;
    					}else if(user.getString().equals("Lava")){
    						//Lava
    						a.getLocation().getBlock().setType(Material.LAVA);
    					}else if(user.getString().equals("Normal")){
    						//Do More Damage
    						event.setDamage(3.5);
    					}
    					if(difArrow == true){
    						//Cool
    						event.setDamage(2);
				    		Game game2 = plugin.getGame(shooter);
				    		User userCooler = game2.getPlayerList.get(plugin.getIndex(shooter));
				    		userCooler.setJumping(true);
					    	//User userDoneCooling = new User(userCooler.getPlayer, userCooler.getPreviousStuff, userCooler.getHero, userCooler.getLives, userCooler.getSneaking, false, userCooler.getCooling, userCooler.getString);
					    	//game2.getPlayerList.set(plugin.getIndex(userCooler.getPlayer), userDoneCooling);
    					}
    					a.remove();
    				}
    			}
    		}
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("Hawkeye")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR)){
							event.setDamage(2);
						}
    				}
				}
    		}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onProjectileHitEvent(ProjectileHitEvent event){
		try{
    		if(event.getEntity() instanceof Arrow){
    			Arrow a = (Arrow) event.getEntity();
    			if(a.getShooter() instanceof Player){
    				final Player shooter = (Player) a.getShooter();
    				Game game = plugin.getGame(shooter);
    				User user = game.getPlayerList.get(plugin.getIndex(shooter));
    				if((plugin.locIsInGame(a.getLocation(), game) == true) && (game.getStarted == true) && ((user.getHero().equals("Hawkeye")) || (user.getHero().equals("IronMan")))){
    					if(user.getString().equals("Explosive")){
    						//Make Boom
    						a.getWorld().createExplosion(a.getLocation(), 4);
    					}else if(user.getString().equals("Grapple")){
    						//Pull Victim
    						hookPull(shooter, a.getLocation());
    					}else if(user.getString().equals("Lava")){
    						//Lava
    						a.getLocation().getBlock().setType(Material.LAVA);
    					}
    					if(user.getHero().equals("Hawkeye")){
				    		Game game2 = plugin.getGame(shooter);
				    		User userCooler = game2.getPlayerList.get(plugin.getIndex(shooter));
				    		userCooler.setJumping(true);
	    				}
				    	//User userDoneCooling = new User(userCooler.getPlayer, userCooler.getPreviousStuff, userCooler.getHero, userCooler.getLives, userCooler.getSneaking, false, userCooler.getCooling, userCooler.getString);
				    	//game2.getPlayerList.set(plugin.getIndex(userCooler.getPlayer), userDoneCooling);
				    	a.remove();
    				}
    			}
    		}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if(user.getHero().equals("Hawkeye")){
						if(event.getCause() == DamageCause.FALL){
					    	event.setDamage(event.getDamage()/2);
						}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getBow(){
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta meta = bow.getItemMeta();
		meta.setDisplayName("§7Bow");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§Sneak shoot for triple homing shot");
		meta.setLore(loreList);
		bow.setItemMeta(meta);
		return bow;
	}
	
	public ItemStack getArrow1(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Fire Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow2(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Explosive Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow3(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Grapple Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow4(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Toxic Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow5(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Paralisis Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow6(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Normal Arrow");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Has no cool-down");
		meta.setLore(loreList);
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public ItemStack getArrow7(){
		ItemStack pulsars = new ItemStack(Material.ARROW);
		ItemMeta meta = pulsars.getItemMeta();
		meta.setDisplayName("§7Lava Arrow");
		pulsars.setItemMeta(meta);
		return pulsars;
	}
	
	public void addPotionEffects(final Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			//Add Effects
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(3 * plugin.getArmour())), true);
			//System.out.println("Pot");
			arrowTask(player);
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					arrowTask(player);
				}
			},(20 * 5));
		}
	}
	
	public void arrowTask(Player player){
		//Give Arrow
		//System.out.println("AT: " + hasArrow(player));
		try{
			if(!hasArrow(player)){
				addArrow(player);
				player.updateInventory();
	    		Game game2 = plugin.getGame(player);
	    		User userCooler = game2.getPlayerList.get(plugin.getIndex(player));
	    		userCooler.setJumping(false);
			}
		}catch(Exception e){e.printStackTrace();}
	}
	
	public boolean hasArrow(Player p){
		try{
			for(ItemStack i : p.getInventory()){
				if(i != null)
					if(i.getType().equals(Material.ARROW))
						return true;
			}
		}catch(Exception e){}
		return false;
	}
    
}
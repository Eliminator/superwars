package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class ScarletWitch extends Hero{
	
	public ScarletWitch(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			try{
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("ScarletWitch")) && (game.getStarted == true)){
					String state = user.getString();
					//System.out.println("State = " + state);
					if(state.equals("on")){
						player.setVelocity(player.getLocation().getDirection().multiply(1));
					}else if(state.equals("hover")){
						player.setVelocity(new Vector(0,0,0));
					}
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("ScarletWitch")) && (game.getStarted == true)){
				try{
					ItemStack item = player.getItemInHand();
					String name = item.getItemMeta().getDisplayName();
					if((item.getType().equals(Material.REDSTONE_BLOCK)) && (name.contains("§cReality Bender"))){
						event.setCancelled(true);
						//Bend
						if(user.isCooling() == false){
							if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
								//Reality Warp
								launchFirework(player, 0);
								for(Entity e : player.getNearbyEntities(9, 9, 9))
									if(e instanceof LivingEntity){
										((LivingEntity)e).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 5, true));
										((LivingEntity)e).setVelocity(player.getLocation().toVector().subtract(player.getLocation().toVector()));
										((LivingEntity)e).damage(4);
									}
								
							}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))){
								//Chaos
								for(Entity e : player.getNearbyEntities(8, 8, 8))
									if(e instanceof Player){
										((Player)e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1, true));
										((Player)e).addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 40, 1, true));
										((Player)e).damage(6);
										launchFirework(((Player)e), 0);
										randomTP(((Player)e));
									}
							}
							//Start Cooling
				    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
				    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
							user.setCooling(true);
				    		startCoolTimer(player, "Reality Bend", 15, 0);
						}
					}else if((item.getType().equals(Material.INK_SACK)) && (name.contains("§cHex"))){
						event.setCancelled(true);
						if(user.isCooling("bolt") == false){
							if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
								//Create Bolt
								shootHex(player, false);
							}else if((event.getAction().equals(Action.LEFT_CLICK_AIR)) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)){
								//Shoot Orb
								shootHex(player, true);
							}
			    			//Set Cooling
							quickCool(player, "bolt", 3);
						}
					}else if((item.getType().equals(Material.FEATHER)) && (name.contains("§7Flight"))){
						//Flight Toggle
						String newState = "off";
						if(name.contains("on")){
							player.setItemInHand(getFly("hover"));
							newState = "hover";
							player.setAllowFlight(true);
							player.setFlying(true);
						}else if(name.contains("off")){
							player.setItemInHand(getFly("on"));
							newState = "on";
							player.setAllowFlight(true);
						}else if(name.contains("hover")){
							player.setItemInHand(getFly("off"));
							newState = "off";
							player.setAllowFlight(false);
						}
						//Set Flight
						user.setString(newState);
					}
				}catch(Exception e){}
			}
		}
	}
	
	public void randomTP(Player p){
		//Get Locations
		Location l = p.getLocation();
		ArrayList<Location> lList = new ArrayList<Location>();
		for(double x = l.getX()-10; x < l.getX() + 10; x++)
			for(double z = l.getZ()-10; z < l.getZ() + 10; z++){
				Location tmp = new Location(p.getWorld(), x, 1, z);
				if(tmp.getWorld().getHighestBlockAt(tmp).getType().equals(Material.AIR) == false){
					Location newLoc = new Location(p.getWorld(), x, tmp.getWorld().getHighestBlockYAt(tmp)+1, z);
					lList.add(newLoc);
				}
			}
		//TP
        int index = new Random().nextInt(lList.size());
        Location tp = lList.get(index);
        p.teleport(tp);
	}
	
	public void launchFirework(Player p, int speed) {
	    Firework fw = (Firework) p.getWorld().spawn(p.getEyeLocation(), Firework.class);
	    FireworkMeta meta = fw.getFireworkMeta();
	    meta.addEffect(FireworkEffect.builder().withColor(Color.RED).with(Type.BALL_LARGE).build());
	    fw.setFireworkMeta(meta);
	    fw.setVelocity(p.getLocation().getDirection().multiply(speed));
	    detonate(fw);
	}
	
	public void detonate(final Firework fw){
	    //Wait 1 Tick and Detontae
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					fw.detonate();
				}catch(Exception e){}
			}
		}, (2));
	}
	
	public void shootHex(Player player, boolean orb){
		final Item bolt = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
		bolt.setVelocity(player.getLocation().getDirection().normalize().multiply(1.8));
		//Change Item Name
		ItemMeta m = bolt.getItemStack().getItemMeta();
		m.setDisplayName("§7Thrown Shield");
		bolt.getItemStack().setItemMeta(m);
		bolt.getItemStack().setAmount(1);
		hexEffect(bolt, new ArrayList<UUID>(Arrays.asList(player.getUniqueId())), orb);
		//Remove Item
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					bolt.remove();
				}catch(Exception e){}
			}
		}, (40));
	}
	
	@SuppressWarnings({ "unchecked" })
	public void hexEffect(final Item i, ArrayList<UUID> dList, final boolean orb){
		if(i.isDead() == false){
			//Effect
			//ParticleEffects.sendToLocation(ParticleEffects.TILECRACK, i.getLocation(), 0, 0, 0, 0, 260);
			if(orb){
				i.getLocation().getWorld().playEffect(i.getLocation(), Effect.STEP_SOUND, 152);
			}else{
				//ParticleEffects.sendToLocation(ParticleEffects.REDSTONE_DUST, i.getLocation(), 0, 0, 0, 0, 3);
				plugin.displayParticles("REDSTONE_DUST", i.getLocation(), 0, 3);
			}
			//Damage
			ArrayList<UUID> tmp = (ArrayList<UUID>) dList.clone();
			for(Entity e : i.getNearbyEntities(1, 1, 1))
				if((e instanceof LivingEntity) && (!tmp.contains(e.getUniqueId()))){
					tmp.add(e.getUniqueId());
					if(orb){
						((LivingEntity)e).damage(1);
						((LivingEntity)e).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40, 5), true);
						((LivingEntity)e).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 100, 5), true);
					}else
						((LivingEntity)e).damage(3);
				}
			//Kill
			if(i.getLocation().toVector().getY() == 0)
				i.remove();
			//Tick
			final ArrayList<UUID> dList2 = (ArrayList<UUID>) tmp.clone();
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					try{
						hexEffect(i, dList2, orb);
					}catch(Exception e){}
				}
			}, (1));
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = game.getPlayerList.get(plugin.getIndex(player));
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("ScarletWitch"))){
				    	event.setDamage(event.getDamage()/2);
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getRealityBend(){
		ItemStack star = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta meta = star.getItemMeta();
		meta.setDisplayName("§cReality Bender");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§4Right-click cast a tempest of reality-warping");
		loreList.add("§4Left-click to cast a tempest of chaos");
		meta.setLore(loreList);
		star.setItemMeta(meta);
		return star;
	}
	
	@SuppressWarnings("deprecation")
	public ItemStack getHexBolt(){
		ItemStack bolt = new ItemStack(351, 1, (byte)1);
		ItemMeta meta = bolt.getItemMeta();
		meta.setDisplayName("§cHex");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§4Right-click to fire hex bolt");
		loreList.add("§4Left-click to fire hex orb");
		meta.setLore(loreList);
		bolt.setItemMeta(meta);
		return bolt;
	}
	
	public ItemStack getFly(String state){
		ItemStack feather = new ItemStack(Material.FEATHER);
		ItemMeta meta = feather.getItemMeta();
		if(state.equals("on"))
			meta.setDisplayName("§7Flight - on");
		else if(state.equals("hover"))
			meta.setDisplayName("§7Flight - hover");
		else
			meta.setDisplayName("§7Flight - off");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right-click to toggle flight type");
		meta.setLore(loreList);
		feather.setItemMeta(meta);
		return feather;
	}
	
	public void addPotionEffects(Player player){
		try{
			Game game = plugin.getGame(player);
			if(game.getStarted == true){
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 1), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(1 * plugin.getArmour())), true);
				//Check Fly
				Location l = player.getLocation();
				l.setY(l.getY() - 1);
				if(!l.getBlock().getType().equals(Material.AIR)){
					//Stop Flying
					User user = game.getPlayerList.get(plugin.getIndex(player));
					user.setString("off");
		    		player.setAllowFlight(false);
		    		for(ItemStack it : player.getInventory()){
		    			if((it.getType().equals(Material.FEATHER)) && (it.getItemMeta().getDisplayName().contains("§7Flight"))) {
		    				player.getInventory().remove(it);
		    				player.getInventory().addItem(getFly("off"));
			    		}
		    		}
				}
			}
		}catch(Exception e){}
	}
}


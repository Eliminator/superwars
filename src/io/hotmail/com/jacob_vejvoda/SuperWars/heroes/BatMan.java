package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

@SuppressWarnings("deprecation")
public class BatMan extends Hero{
		
	public BatMan(SuperWars instance) {
		super(instance);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void onManMove(PlayerMoveEvent event) {
		try{
			final Player player = event.getPlayer();
			if(plugin.getGame(player) != null){
				final Game game = plugin.getGame(player);
				User user = game.getPlayerList.get(plugin.getIndex(player));
				if((user.getHero().equals("BatMan")) && (game.getStarted == true)){
					Location l = player.getLocation();
					l.setY(l.getY() - 1);
					Block f = l.getBlock();
					if((user.isSneaking() == true) && (f.getType().equals(Material.AIR))){
						Vector v = (player.getLocation().getDirection().multiply(1));
						v = v.setY(-0.15);
						player.setVelocity(v);
					}else{
						for(ItemStack i : player.getInventory()){
							if((i != null) && (i.getItemMeta().getDisplayName().equals("§7Bat Wings [§aon§7]"))){
								//Set Off
					    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, false, user.getJumping, user.getCooling, user.getString);
					    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
								user.setSneaking(false);
					    		ItemMeta m = i.getItemMeta();
					    		m.setDisplayName("§7Bat Wings [§aoff§7]");
					    		i.setItemMeta(m);
					    		//Remove Glider
//					    		player.getInventory().setHelmet(null);
//						 		//Create Armour
//						 		ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
//								Color batBlack = Color.fromRGB(0,0,0);
//								plugin.dye(helm, batBlack);
//								helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
//								player.getInventory().setHelmet(helm);
								return;
							}
						}
					}
				}
			}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onBatInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("BatMan")) && (game.getStarted == true)){
				try{
					//System.out.println("in " + player.getItemInHand().getType() + " name: " + player.getItemInHand().getItemMeta().getDisplayName());//------------------------DEBUG
					if ((player.getItemInHand().getType().equals(Material.INK_SACK)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Batarang")) && (event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (user.isCooling("batarang") == false)){
						//Start Cooling
			    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, true, user.getCooling, user.getString);
			    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
						quickCool(player, "batarang", 2);
			    		//Cancel Event
						event.setCancelled(true);
						//Create Batarang
						final Item batarang = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
						batarang.setVelocity(player.getLocation().getDirection().normalize().multiply(3));
						//Change Item Name
						ItemMeta m = batarang.getItemStack().getItemMeta();
						m.setDisplayName("§7Thrown Shield");
						batarang.getItemStack().setItemMeta(m);
						batarang.getItemStack().setAmount(1);
						//Damage
						if (getTarget(player) != null) {
							Entity attacked = getTarget(player);
							//System.out.println("Bat Target = " + attacked.getType());
							if ((attacked instanceof LivingEntity)) {
								((LivingEntity)attacked).damage((int)Math.round(2.0D * 2));
							}
						}
						//Remove Batarang
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								batarang.remove();
							}
						},(2*20));
						//entityCauseDamage(player, bat, 3, bat.getLocation());
					}else if ((player.getItemInHand().getType().equals(Material.COAL)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Smoke Grenade"))){
						if((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))){
							//Throw Item
							Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(1));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Grenade");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Item
							int amount = player.getItemInHand().getAmount() - 1;
							if(amount <= 0){
								player.setItemInHand(null);
							}else{
								player.getItemInHand().setAmount(amount);
							}
							//Time Boom
							explode(5, "smoke", dropped);
						}
					}else if ((player.getItemInHand().getType().equals(Material.SLIME_BALL)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Frag Grenade"))){
						if((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))){
							//Throw Item
							Item dropped = player.getWorld().dropItem(player.getEyeLocation(), player.getItemInHand());
							dropped.setVelocity(player.getLocation().getDirection().normalize().multiply(1));
							//Change Item Name
							ItemMeta m = dropped.getItemStack().getItemMeta();
							m.setDisplayName("§7Thrown Grenade");
							dropped.getItemStack().setItemMeta(m);
							dropped.getItemStack().setAmount(1);
							//Remove Block
							int amount = player.getItemInHand().getAmount() - 1;
							if(amount <= 0){
								player.setItemInHand(null);
							}else{
								player.getItemInHand().setAmount(amount);
							}
							//Time Boom
							explode(5, "frag", dropped);
						}
					}else if((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))){
						if (player.getItemInHand().getType().equals(Material.FEATHER)){
							if(player.getItemInHand().getItemMeta().getDisplayName().equals("§7Bat Wings [§aoff§7]")){
								//Set On
					    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, true, user.getJumping, user.getCooling, user.getString);
					    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
								user.setSneaking(true);
					    		ItemMeta m = player.getItemInHand().getItemMeta();
					    		m.setDisplayName("§7Bat Wings [§aon§7]");
					    		player.getItemInHand().setItemMeta(m);
					    		//Remove Armour
					    		//player.getInventory().setHelmet(null);
						 		//Create Glider
					    		//ItemStack glider = new ItemStack(Material.HOPPER, 1);
					    		//player.getInventory().setHelmet(glider);
							}else if(player.getItemInHand().getItemMeta().getDisplayName().equals("§7Bat Wings [§aon§7]")){
								//Set Off
					    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, false, user.getJumping, user.getCooling, user.getString);
					    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
								user.setSneaking(false);
					    		ItemMeta m = player.getItemInHand().getItemMeta();
					    		m.setDisplayName("§7Bat Wings [§aoff§7]");
					    		player.getItemInHand().setItemMeta(m);
					    		//Remove Glider
					    		//player.getInventory().setHelmet(null);
						 		//Create Armour
						 		//ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
								//Color batBlack = Color.fromRGB(0,0,0);
								//plugin.dye(helm, batBlack);
								//helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
								//player.getInventory().setHelmet(helm);
							}
						}else if ((player.getItemInHand().getTypeId() == 2263) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Ultrasonic Transmitter"))){
							if(user.isCooling() == false){
								if((getTarget(player) != null) && (getTarget(player) instanceof LivingEntity)) {
									//Start Cooling
						    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, true, user.getString);
						    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
									user.setCooling(true);
						    		startCoolTimer(player, "Bat Call", 40, 0);
						    		//Cancel Event
									event.setCancelled(true);
									//Get Target
									final LivingEntity target = (LivingEntity) getTarget(player);
									final Entity bat1 = player.getWorld().spawnEntity(player.getLocation(), EntityType.BAT);
									entRemove(bat1, target);
									doSlowDamage(target, 10, 0);
									//Spawn 2 wave
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
										public void run() {
											try{
												if(plugin.getGame(player) != null){
													Entity bat2 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat2, target);
													Entity bat3 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat3, target);
													Entity bat4 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat4, target);
												}
											}catch(Exception e){}
										}
									}, (2 * 20));
									//Spawn 3rd wave
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
										public void run() {
											try{
												if(plugin.getGame(player) != null){
													Entity bat5 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat5, target);
													Entity bat6 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat6, target);
													Entity bat7 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat7, target);
													Entity bat8 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat8, target);
													Entity bat9 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat9, target);
													Entity bat10 = player.getWorld().spawnEntity(target.getLocation(), EntityType.BAT);
													entRemove(bat10, target);
												}
											}catch(Exception e){}
										}
									}, (5 * 20));
								}
							}
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	public void entRemove(final Entity e, final LivingEntity target){
		e.setVelocity(new Vector(0, 0.5, 0));
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					moveToward(e, target, 0.5, 60);
				}catch(Exception e){}
			}
		}, (1 * 20));
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					e.remove();
				}catch(Exception e){}
			}
		}, (15 * 20));
	}
	
	public void moveToward(final Entity entity, final LivingEntity p, final double speed, int time){
		if(time > 0){
			Location loc1 = p.getEyeLocation();
			Location loc2 = entity.getLocation();
			try{
				Vector v = new Vector(loc1.getX()-loc2.getX(), loc1.getY()-loc2.getY(), loc1.getZ()-loc2.getZ());
				v = v.setY(v.getY()+0.3);
		        entity.setVelocity(v.multiply(0.2));
		        final int newTime = time - 1;
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						try{
							moveToward(entity, p, speed, newTime);
						}catch(Exception e){}
					}
				}, (5));
			}catch(Exception e){}
		}
    }
	
	public void doSlowDamage(final Entity e, final int damage, int time){
		//Game End Check
		if(e instanceof Player)
			if(plugin.getGame((Player) e) == null)
				return;
		if(time <= 10){
			try{
				((LivingEntity)e).damage((int)Math.round(2.0D * 1));
			}catch(Exception ex){}
			final int newTime = time + 1;
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					doSlowDamage(e, damage, newTime);
				}
			}, (20));
		}
	}
	
	public static void explode(int seconds, final String type, final Item gren){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				try{
					ArrayList<Entity> damagedList = new ArrayList<Entity>();
					//System.out.println("Thrown " + type);//------------------------DEBUG
					if(type.equals("smoke")){
						//System.out.println("Flash");//------------------------DEBUG
						for(Entity e : gren.getNearbyEntities(6, 6, 6)){
							if (e instanceof LivingEntity) {
								((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 160, 1), true);
							}
						}
						//ItemStack tmpCol = new ItemStack(Material.LEATHER_HELMET, 1);
						//LeatherArmorMeta tmpCol2 = (LeatherArmorMeta)tmpCol.getItemMeta();
						//tmpCol2.setColor(Color.fromRGB(0, 0, 0));
						//Color col = tmpCol2.getColor();
						//FireworkEffect fe = (FireworkEffect.builder().withColor(col).with(Type.BALL_LARGE).build());
						//FireworkEffectPlayer fplayer = new FireworkEffectPlayer();
						//	fplayer.playFirework(gren.getWorld(), gren.getLocation(), fe);
						//ParticleEffects.sendToLocation(ParticleEffects.LARGE_SMOKE, gren.getLocation(), 0, 0, 0, 1, 100);
						plugin.displayParticles("LARGE_SMOKE", gren.getLocation(), 1, 100);
						gren.remove();
					}else if(type.equals("frag")){
						//System.out.println("Frag");//------------------------DEBUG
						for(Entity e : gren.getNearbyEntities(1, 1, 1)){
							if ((e instanceof LivingEntity) && (!damagedList.contains(e))) {
								((LivingEntity)e).damage((int)Math.round(2.0D * 12));
								damagedList.add(e);
							}
						}
						for(Entity e : gren.getNearbyEntities(3, 3, 3)){
							if ((e instanceof LivingEntity) && (!damagedList.contains(e))) {
								((LivingEntity)e).damage((int)Math.round(2.0D * 8));
								damagedList.add(e);
							}
						}
						for(Entity e : gren.getNearbyEntities(5, 5, 5)){
							if ((e instanceof LivingEntity) && (!damagedList.contains(e))) {
								((LivingEntity)e).damage((int)Math.round(2.0D * 4));
								damagedList.add(e);
							}
						}
						Double x = gren.getLocation().getX();
						Double y = gren.getLocation().getY();
						Double z = gren.getLocation().getZ();
						gren.getLocation().getWorld().createExplosion(x, y, z, 4, true, true);
						gren.remove();
					}
				}catch(Exception e1){e1.printStackTrace();}
			}
		}, (20 * seconds));
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			try{
				Item i = event.getItem();
				ItemStack s = i.getItemStack();
				if((s.getItemMeta().getDisplayName().equals("§7Thrown Grenade")) || (s.getItemMeta().getDisplayName().equals("§7Thrown Shield"))){
					event.setCancelled(true);
				}
			}catch(Exception e){}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onFish(PlayerFishEvent event) {
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("BatMan")) && (game.getStarted == true)){
				//System.out.println("1");//------------------------DEBUG
				try{
					if ((player.getItemInHand().getType().equals(Material.FISHING_ROD)) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§7Grapple Gun"))){
						//System.out.println("2");//------------------------DEBUG
						Fish hook = event.getHook();
						//System.out.println("X = " + hook.getVelocity().getX());//------------------------DEBUG
						//System.out.println("Y = " + hook.getVelocity().getY());//------------------------DEBUG
						//System.out.println("Z = " + hook.getVelocity().getZ());//------------------------DEBUG
						if((hook.getVelocity().getY() >= 0)){
							//System.out.println("cast");//------------------------DEBUG
							checkHook(hook);
						}else if((hook.getVelocity().getX() == 0) && (hook.getVelocity().getY() <= 0) && (hook.getVelocity().getZ() == 0)){
							//System.out.println("Pull");//------------------------DEBUG
							event.setCancelled(true);
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	public void checkHook(final Fish hook){
		if(!hook.isDead()){
			Block b = hook.getLocation().getBlock();
			//System.out.println("hook in " + b.getType());//------------------------DEBUG
			//System.out.println("hook speed " + hook.getVelocity().getY());//------------------------DEBUG
			if(hook.getVelocity().getY() <= 0){
				int radius = 1;
				double xTmp = hook.getLocation().getX();
				double yTmp = hook.getLocation().getY();
				double zTmp = hook.getLocation().getZ();
				final int finalX = (int) Math.round(xTmp);
				final int finalY = (int) Math.round(yTmp);
				final int finalZ = (int) Math.round(zTmp);
                for (int x = finalX-(radius); x <= finalX+radius; x ++){
                	for (int y = finalY-(radius); y <= finalY+radius; y ++) {
                		for (int z = finalZ-(radius); z <= finalZ+radius; z ++) {
                			Location loc = new Location(hook.getWorld(), x, y, z);
                			Block block = loc.getBlock();
                			if(!plugin.transBlocks.contains(block.getTypeId())){
            					hook.setVelocity(new Vector(0, 0, 0));
            					hookPull((Entity)hook.getShooter(), hook.getLocation());
            					return;
                			}
                		}
                	}
                }
			}
			if(b.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)){
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						checkHook(hook);
					}
				},(1));
			}else{
				hookPull((Entity)hook.getShooter(), hook.getLocation());
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onSneak(PlayerToggleSneakEvent event){
		final Player player = event.getPlayer();
		if(plugin.getGame(player) != null){
			final Game game = plugin.getGame(player);
			final User user = game.getPlayerList.get(plugin.getIndex(player));
			if((user.getHero().equals("BatMan")) && (game.getStarted == true)){
				try{
    				if (!user.isCooling("invis")) {
    					//Remove Armour
    					if(plugin.getConfig().getBoolean("useHeroDisguises") == false){
		    				player.getInventory().setHelmet(null);
		    				player.getInventory().setChestplate(null);
		    				player.getInventory().setLeggings(null);
		    				player.getInventory().setBoots(null);
    					}
	    				//Make Player Invisible
	    				player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 200, 1), true);
						//Set Cool
			    		//User userCool = new User(user.getPlayer, user.getPreviousStuff, user.getHero, user.getLives, user.getSneaking, user.getJumping, user.getCooling, "cooling");
			    		//game.getPlayerList.set(plugin.getIndex(user.getPlayer), userCool);
	    				quickCool(player, "invis", 15);
	    				//Time Invisibility
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						     public void run() {
						 		//Create Armour
						    	//player.getInventory().clear();
						    	plugin.giveItems(player, user.getHero(), true, -1);
//						    	if(plugin.getConfig().getBoolean("useHeroDisguises") == false){
//							 		ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
//							 		ItemStack ches = new ItemStack(Material.LEATHER_CHESTPLATE);
//							 		ItemStack pant = new ItemStack(Material.LEATHER_LEGGINGS);
//							 		ItemStack boot = new ItemStack(Material.LEATHER_BOOTS);
//									//Dye Armour
//									Color batBlack = Color.fromRGB(0,0,0);
//									plugin.dye(helm, batBlack);
//									plugin.dye(ches, batBlack);
//									plugin.dye(pant, batBlack);
//									plugin.dye(boot, batBlack);
//									//Add Enchantments
//									helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 8);
//									ches.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 8);
//									pant.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 8);
//									boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 8);
//									//Give Armour
//									player.getInventory().setHelmet(helm);
//									player.getInventory().setChestplate(ches);
//									player.getInventory().setLeggings(pant);
//									player.getInventory().setBoots(boot);
//						    	}
							 }
						}, (10 * 20));
    				}
				}catch(Exception e){}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEntityAttack(EntityDamageByEntityEvent event){
        try{
    		Entity eAttacker = event.getDamager();
    		if(eAttacker instanceof Player){
    			Player attacker = (Player) eAttacker;
    			if(plugin.getGame(attacker) != null){
    				Game game = plugin.getGame(attacker);
    				String aHero = plugin.getHero(attacker);
    				if((aHero.equals("BatMan")) && (game.getStarted == true)){
    					if(attacker.getItemInHand().getType().equals(Material.AIR))
							event.setDamage(3.5);
					}
    			}
			}
        }catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("BatMan"))){
				    	if(user.isSneaking() == true){
				    		event.setDamage(0);
				    	}else{
				    		event.setDamage(event.getDamage()/2);
				    	}
					}
				}
			}catch(Exception e){}
		}
	}
	
	public ItemStack getTransmiter(){
		ItemStack trans = new ItemStack(2263, 1);
		ItemMeta meta = trans.getItemMeta();
		meta.setDisplayName("§7Ultrasonic Transmitter");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to call bats");
		meta.setLore(loreList);
		trans.setItemMeta(meta);
		return trans;
	}
	
	public ItemStack getBatarang(){
		ItemStack rang = new ItemStack(Material.INK_SACK);
		ItemMeta meta = rang.getItemMeta();
		meta.setDisplayName("§7Batarang");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to throw");
		meta.setLore(loreList);
		rang.setItemMeta(meta);
		return rang;
	}
	
	public ItemStack getGG(){
		ItemStack gun = new ItemStack(Material.FISHING_ROD);
		ItemMeta meta = gun.getItemMeta();
		meta.setDisplayName("§7Grapple Gun");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to shoot");
		loreList.add("§8Right click again to pull yourself");
		meta.setLore(loreList);
		gun.setItemMeta(meta);
		return gun;
	}
	
	public ItemStack getGlider(){
		ItemStack gun = new ItemStack(Material.FEATHER);
		ItemMeta meta = gun.getItemMeta();
		meta.setDisplayName("§7Bat Wings [§aoff§7]");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to toggle");
		meta.setLore(loreList);
		gun.setItemMeta(meta);
		return gun;
	}
	
	public ItemStack getGrenade(int amount){
		ItemStack gren = new ItemStack(Material.SLIME_BALL);
		gren.setAmount(amount);
		ItemMeta meta = gren.getItemMeta();
		meta.setDisplayName("§7Frag Grenade");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to throw");
		meta.setLore(loreList);
		gren.setItemMeta(meta);
		return gren;
	}
	
	public ItemStack getFlashBang(int amount){
		ItemStack gren = new ItemStack(Material.COAL);
		gren.setAmount(amount);
		ItemMeta meta = gren.getItemMeta();
		meta.setDisplayName("§7Smoke Grenade");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to throw");
		meta.setLore(loreList);
		gren.setItemMeta(meta);
		return gren;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
		}
	}
}
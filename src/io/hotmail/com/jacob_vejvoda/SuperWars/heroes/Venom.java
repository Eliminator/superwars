package io.hotmail.com.jacob_vejvoda.SuperWars.heroes;

import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars.Game;
import io.hotmail.com.jacob_vejvoda.SuperWars.SuperWars;
import io.hotmail.com.jacob_vejvoda.SuperWars.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Venom extends Hero{
	Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
	public static ArrayList<String> climbingPlayers = new ArrayList<String>();
	
	public Venom(SuperWars instance) {
		super(instance);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onSpideyInteract(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.PHYSICAL))
			return;
		final Player player = event.getPlayer();
		if((plugin.getGame(player) != null) && (player.getItemInHand() != null) && (!event.getAction().equals(Action.LEFT_CLICK_BLOCK)) && (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
			final Game game = plugin.getGame(player);
			String hero = plugin.getHero(player);
			if((hero.equals("Venom")) && (game.getStarted == true)){
				try{
					User user = game.getPlayerList.get(plugin.getIndex(player));
					if(user.isJumping() == false) {
						boolean cool = false;
						boolean sence = false;
						if((player.getItemInHand().getType().equals(Material.INK_SACK)) && (player.getItemInHand().getItemMeta().getDisplayName().contains("§7Web Shot"))){
							//Set Right or Left Click
				    		//User userCool = null;
				    		if(player.isSneaking()){
				    			if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				    				//Grapple
				    				cool = true;
				    				user.setJumping(true);
				    				user.setString("grapple");
				    			}
				    		}else if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
				    			//Make Web
				    			cool = true;
			    				user.setJumping(true);
			    				user.setString("make");
				    		}else if(event.getAction().equals(Action.LEFT_CLICK_AIR)) {
				    			//Remove Web
				    			cool = true;
			    				user.setJumping(true);
			    				user.setString("remove");
				    		}
						}
						if(cool == true){
							//Make Gun Sound
							player.playSound(player.getLocation(), Sound.BLOCK_CLOTH_BREAK, 2F, 1F);
							//Fire Gun Bullet
							EnderPearl ball = player.getWorld().spawn(player.getEyeLocation(), EnderPearl.class);
							ball.setShooter(player);
							ball.setVelocity(player.getLocation().getDirection().multiply(3));
			    			//Start Homing
							if(sence == true){
				    		      double minAngle = 6.283185307179586D;
				    		      Entity minEntity = null;

				    		      for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
				    		        if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity)) && (!entity.isDead())) {
				    		          Vector toTarget = entity.getLocation().toVector().clone().subtract(player.getLocation().toVector());
				    		          double angle = ball.getVelocity().angle(toTarget);
				    		          if (angle < minAngle) {
				    		            minAngle = angle;
				    		            minEntity = entity;
				    		          }
				    		        }
				    		      }

				    		      if (minEntity != null)
				    		        new HomingTask((Snowball)ball, (LivingEntity)minEntity, plugin);
							}
						}
					}
				}catch(Exception e){}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent e) {
		try{
			final Player p = e.getPlayer();
			Game g = plugin.getGame(p);
			if(g.getStarted == true){
				Location l = p.getLocation();
				if((l.getBlock().getType().equals(Material.CARPET)) && (l.getBlock().getData() == (byte)15)){
					p.setVelocity(p.getVelocity().multiply(0.1).multiply(0.1));
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5, 2), true);
				}
			}
		}catch(Exception x){}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.NORMAL)
    public void onProjectileHitEvent(ProjectileHitEvent event){
		try{
			if(event.getEntity() instanceof EnderPearl){
				EnderPearl sb = (EnderPearl) event.getEntity();
				if(sb.getShooter() instanceof Player){
					final Player shooter = (Player) sb.getShooter();
					Game game = plugin.getGame(shooter);
					User user = game.getPlayerList.get(plugin.getIndex(shooter));
					if((user.getHero().equals("Venom")) && (game.getStarted == true)){
						Location l = event.getEntity().getLocation();
						final Block b = l.getBlock();
						Location under = l.clone();
						under.setY(under.getY()-1);
						if(user.getString().equals("make")){
							//Make Web
							if((plugin.transBlocks.contains(under.getBlock().getTypeId()) == false) && (b.getType().equals(Material.AIR))){
								b.setTypeIdAndData(171, (byte)15, true);
								setAir(b, 30);
							}
							//Blind
							for(Entity e : sb.getNearbyEntities(1, 1, 1))
								if(e instanceof LivingEntity)
									((LivingEntity)e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 1), true);
						}else if(user.getString().equals("remove")){
							//Remove Web
							//setAir(l.getBlock(), 3);
							setNearToAir(b, Material.CARPET);
						}else if(user.getString().equals("grapple")){
							//Grapple String
							try{
								hookPull((Entity)shooter, sb.getLocation());
							}catch(Exception e){}
						}
						user.setJumping(false);
						event.getEntity().remove();
					}
				}
			}
		}catch(Exception e){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerTP(PlayerTeleportEvent e) {
		try{
			Player player = e.getPlayer();
			if(plugin.getGame(player) != null){
				Game game = plugin.getGame(player);
				User user = null;
				for(User u : game.getPlayerList)
					if(u.getPlayer().equals(player))
						user = u;
				if((user.getHero().equals("Venom")) && (e.getCause().equals(TeleportCause.ENDER_PEARL)))
					e.setCancelled(true);
			}
		}catch(Exception x){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
		try{
			Player player = e.getPlayer();
			if(plugin.getGame(player) != null){
				Game game = plugin.getGame(player);
				User user = null;
				for(User u : game.getPlayerList)
					if(u.getPlayer().equals(player))
						user = u;
				//Poison Attack
				if(user.isCooling() == false)
					if((player.getItemInHand().getType().equals(Material.FLINT)) && (user.getHero().equals("Venom"))){
						((LivingEntity)e.getRightClicked()).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 60, 1), true);
						((LivingEntity)e.getRightClicked()).damage(2);
						//Cool
						user.setCooling(true);
						startCoolTimer(player, "Poison Fang", 15, 0);
					}
			}
		}catch(Exception x){}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onEntityDamage(final EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			try{
				Player player = (Player)event.getEntity();
				if(plugin.getGame(player) != null){
					Game game = plugin.getGame(player);
					User user = null;
					for(User u : game.getPlayerList)
						if(u.getPlayer().equals(player))
							user = u;
					//Powers stop some damage
					if((event.getCause() == DamageCause.FALL) && (user.getHero().equals("Venom"))){
				    	event.setDamage(event.getDamage()/2);
					}
				}
			}catch(Exception e){}
		}
	}

	public ItemStack getWeb(){
		ItemStack web = new ItemStack(Material.INK_SACK);
		ItemMeta meta = web.getItemMeta();
		meta.setDisplayName("§7Web Shot");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right click to make web");
		loreList.add("§8Left click to remove web");
		loreList.add("§8Sneak-right click to grapple");
		meta.setLore(loreList);
		web.setItemMeta(meta);
		return web;
	}
	
	public ItemStack getClaws(){
		ItemStack claws = new ItemStack(Material.FLINT);
		claws.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 5);
		ItemMeta meta = claws.getItemMeta();
		meta.setDisplayName("§7Claws");
		ArrayList<String> loreList = new ArrayList<String>();
		loreList.add("§8Right-click to bite with Poison Fangs");
		meta.setLore(loreList);
		claws.setItemMeta(meta);
		return claws;
	}
	
	public void addPotionEffects(Player player){
		Game game = plugin.getGame(player);
		if(game.getStarted == true){
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 220, 3), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 220, (int)(2 * plugin.getArmour())), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 220, 4), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 220, 2), true);
			player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20, 2), true);
	    	try{
	    		User userCooler = game.getPlayerList.get(plugin.getIndex(player));
	    	    if(userCooler.isJumping() == true){
	    	    	userCooler.setJumping(false);
	    	    }
	    	}catch(Exception e){}
		}
	}
}